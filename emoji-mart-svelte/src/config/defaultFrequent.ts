/**
 * From https://github.com/missive/emoji-mart/blob/d29728f7b4e295e46f9b64aa80335aa4a3c15b8e/packages/emoji-mart/src/helpers/frequently-used.ts#L4
 */
export const DefaultFrequent: Array<string> = [
	'+1',
	'grinning',
	'kissing_heart',
	'heart_eyes',
	'laughing',
	'stuck_out_tongue_winking_eye',
	'sweat_smile',
	'joy',
	'scream',
	'disappointed',
	'unamused',
	'weary',
	'sob',
	'sunglasses',
	'heart',
];
