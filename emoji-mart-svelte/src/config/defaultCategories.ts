export const DefaultCategoriesDic: Record<string, string> = {
	people: 'Smileys & People',
	nature: 'Animals & Nature',
	foods: 'Food & Drink',
	activity: 'Activity',
	places: 'Travel & Places',
	objects: 'Objects',
	symbols: 'Symbols',
	flags: 'Flags',
};

export function getDefaultCategories() {
	return ['frequent', 'custom', ...Object.keys(DefaultCategoriesDic)];
}

export function getDefaultCategory(id: string) {
	return DefaultCategoriesDic[id] as string;
}
