import type { Writable } from 'svelte/store';

import type { EmojiIndexed } from './emoji.types';

export interface ContextMap {
	scrolled_catergory: Writable<string>;
	scroll_to_category: Writable<string>;
	hovered_emoji: Writable<EmojiIndexed | null>;
	found_emojis: Writable<Array<EmojiIndexed> | null>;
}
