export interface EmojiSkin {
	native: string;
	unified: string;
	version: number;
	/** Coords in Atlas */
	x: number;
	y: number;
}

export interface Emoji {
	id: string;
	custom?: boolean;
	keywords: Array<string>;
	name: string;
	skins: Array<EmojiSkin>;
	version: number;
}

export interface EmojiSkinCustom {
	src: string;
	static_src: string;
}

export interface EmojiCustom {
	id: string;
	custom: true;
	keywords: Array<string>;
	name: string;
	skins: [EmojiSkinCustom];
	version: number;
}

export interface EmojiIndexedSkin extends EmojiSkin {
	shortcodes: string;
}

export interface EmojiIndexedSkinCustom extends EmojiSkinCustom {
	shortcodes: string;
}

// @ts-expect-error
export interface EmojiIndexed extends Emoji {
	search: string;
	emoticons: Array<string>;
	skins: Array<EmojiIndexedSkin | EmojiIndexedSkinCustom>;
}

export interface ClickedEmoji {
	id: string;
	keywords: Array<string>;
	name: string;
	skin: EmojiSkin | EmojiSkinCustom;
}

export interface EmojiData {
	id: string;
	name: string;
	keywords: Array<string>;
	skin: EmojiSkin;
}
