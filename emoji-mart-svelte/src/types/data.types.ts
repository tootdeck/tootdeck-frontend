import { type Writable } from 'svelte/store';

import type { Emoji, EmojiCustom, EmojiIndexed } from './emoji.types';

export interface DataCategory {
	id: string;
	name: string;
	collapsed: Writable<boolean>;
	emojis: Array<string>;
}

export interface DataCategoryCustom extends Omit<DataCategory, 'emojis'> {
	emojis: Array<EmojiCustom>;
}

/** Atlas size */
export interface DataSheet {
	cols: number;
	rows: number;
}

export interface Data {
	aliases: Record<string, string>;
	categories: Array<DataCategory>;
	emojis: Record<string, Emoji>;
	sheet: DataSheet;
}

/**
 * Linter isn't happy with `EmojiIndexed`
 * because it doesn't know if the property `skins`
 * is `EmojiIndexedSkin` or `EmojiIndexedSkinCustom`
 * it can ignored because the check can only be done at runtime
 * (and it's checked at runtime)
 */
// @ts-expect-error
export interface DataIndexed extends Data {
	customCategories: Array<DataCategory>;
	emojis: Record<string, EmojiIndexed>;
	emoticons: Record<string, string>;
	natives: Record<string, string>;
}

export type ChunkedData = { id: number; chunk: Array<string> };
export type ChunkedCategory = {
	id: string;
	name: string;
	emojis: Array<ChunkedData>;
};
