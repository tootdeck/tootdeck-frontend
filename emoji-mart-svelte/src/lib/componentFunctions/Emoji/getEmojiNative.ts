import { getEmojiSkin } from './getEmojiSkin';

import type { EmojiIndexed } from '../../../types/emoji.types';

export function getEmojiNative(emoji: EmojiIndexed, skin_tone: number) {
	return getEmojiSkin(emoji, skin_tone).native;
}
