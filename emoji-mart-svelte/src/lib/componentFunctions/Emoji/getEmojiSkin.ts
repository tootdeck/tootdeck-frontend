import type { EmojiIndexed, EmojiSkin } from '../../../types/emoji.types';

export function getEmojiSkin(emoji: EmojiIndexed, skin_tone: number): EmojiSkin {
	return (emoji.skins[skin_tone] ?? emoji.skins[0]) as any;
}
