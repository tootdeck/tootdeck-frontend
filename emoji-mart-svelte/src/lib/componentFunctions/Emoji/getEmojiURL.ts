import type { EmojiSkin } from '../../../types/emoji.types';
import type { EmojiStyle } from '../../../types/picker.types';

export function getEmojiURL(skin: EmojiSkin, style: EmojiStyle) {
	return `https://cdn.jsdelivr.net/npm/emoji-datasource-${style}@15.1.2/img/${style}/64/${skin.unified}.png`;
}
