import type { IconStyle } from '../../types/picker.types';

interface DualStyleIcon {
	outline: string;
	solid: string;
}

export function getIconFromCategory(
	dic: Record<string, string | DualStyleIcon>,
	style: IconStyle,
	id: string
): string {
	if (['custom', 'loupe', 'delete'].includes(id)) {
		return dic[id] as string;
	}

	return (dic[id] as DualStyleIcon)[style] as string;
}
