import { SkinToneDic, type SkinTone } from '../../types/picker.types';

export function getSkinToneIndex(skin_tone: number): SkinTone {
	return Object.entries(SkinToneDic).find(([_, value]) => value === skin_tone)![0] as SkinTone;
}
