import { getTheme } from './getTheme';

import { contrastRatio } from '../utils/contrastRatio';

import type { PickerProps } from '../../types/picker.types';

export function adjustTextContrast(Props: PickerProps) {
	const light = '222427';
	const dark = 'dededd';
	const ratio = contrastRatio(
		getTheme(Props) === 'light' ? light : dark,
		Props.accentColor.slice(1, 7)
	);

	return '#' + (ratio <= 0.5 ? light : dark);
}
