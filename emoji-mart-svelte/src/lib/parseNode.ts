/**
 * From https://www.npmjs.com/package/twemoji
 */
import { getEmojiURL } from './componentFunctions/Emoji/getEmojiURL';
// import { getBackgroundPosition } from './componentFunctions/Atlas/getEmojiPositionInAtlas';

import type { EmojiData } from '../types/emoji.types';
import type { EmojiStyle } from '../types/picker.types';
// import type { VersionSupported } from '../types/picker.types';

// RegExp based on emoji's official Unicode standards
// http://www.unicode.org/Public/UNIDATA/EmojiSources.txt
import regex from 'emoji-regex';
const re = regex();

// avoid runtime RegExp creation for not so smart,
// not JIT based, and old browsers / engines
const UFE0Fg = /\uFE0F/g;

// nodes with type 1 which should **not** be parsed
const shouldntBeParsed = /^(?:iframe|noframes|noscript|script|select|style|textarea)$/;

// 1x1px transparent png
const base64_px =
	'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAsSAAALEgHS3X78AAAAAnRSTlMA/1uRIrUAAAAKSURBVAgdY/gPAAEBAQA2X2eAAAAAAElFTkSuQmCC';

/**r
 * Shortcut to create text nodes
 * @param   string  text used to create DOM text node
 * @return  Node  a DOM node with that text
 */
function createText(text: string, clean: boolean): Text {
	return document.createTextNode(clean ? text.replace(UFE0Fg, '') : text);
}

/**
 * Given a generic DOM nodeType 1, walk through all children
 * and store every nodeType 3 (#text) found in the tree.
 * @param   Element a DOM Element with probably some text in it
 * @param   Array the list of previously discovered text nodes
 * @return  Array same list with new discovered nodes, if any
 */
function grabAllTextNodes(node: HTMLElement, allText: Array<HTMLElement>): Array<HTMLElement> {
	const childNodes = node.childNodes;

	let length = childNodes.length;
	while (length--) {
		const subnode: HTMLElement = childNodes[length] as any;
		const nodeType: number = subnode.nodeType;

		// parse emoji only in text nodes
		if (nodeType === 3) {
			// collect them to process emoji later
			allText.push(subnode);
		}

		// ignore all nodes that are not type 1, that are svg, or that
		// should not be parsed as script, style, and others
		else if (
			nodeType === 1 &&
			!('ownerSVGElement' in subnode) &&
			!shouldntBeParsed.test(subnode.nodeName.toLowerCase())
		) {
			grabAllTextNodes(subnode, allText);
		}
	}

	return allText;
}

/**
 * Workaroud to remove duplicate inserted emoji during Svelte lifecycle
 */
function isAlreadyParsed(node: HTMLElement) {
	const parent = node.parentElement;
	if (!parent) {
		return false;
	}

	const parent_children = Array.from(parent.childNodes) as Array<Element>;
	const found_img = parent_children.find(
		(e) =>
			e.tagName === 'IMG' &&
			e.classList.contains('emoji') &&
			e.getAttribute('src') === base64_px
	);

	return !!found_img;
}

/**
 * DOM version of the same logic / parser:
 *  emojify all found sub-text nodes placing images node instead.
 * @param   node   generic DOM node with some text in some child node
 * @param   atlas  options  containing atlas info
 * @return  node same generic node with emoji in place, if any.
 */
export function parseNode(
	node: HTMLElement,
	// atlas: {
	// 	Data: Data;
	// 	atlasURL: string;
	// 	backgroundSize: string;
	// },

	style: EmojiStyle,

	getEmojiDataFromNative: (native: string) => EmojiData | undefined
): HTMLElement {
	// const { Data, atlasURL, backgroundSize } = atlas;

	const allText = grabAllTextNodes(node, []);

	let length = allText.length;

	while (length--) {
		const fragment: DocumentFragment = document.createDocumentFragment();
		const subnode: HTMLElement = allText[length];
		const text: string = subnode.nodeValue ?? '';

		let modified: boolean = false;
		let i: number = 0;
		let match: RegExpExecArray | null;

		const already_parsed = isAlreadyParsed(subnode);

		while ((match = re.exec(text))) {
			const index: number = match.index;

			if (index !== i) {
				fragment.appendChild(createText(text.slice(i, index), true));
			}

			const rawText: string = match[0];
			i = index + rawText.length;

			if (already_parsed) {
				modified = true;
				continue;
			}

			const found = getEmojiDataFromNative(rawText);
			if (found) {
				const img = document.createElement('img');

				/**
				 * Atlas method
				 */
				// const backgroundPosition = getBackgroundPosition(Data, found.skin);
				/**
				 * Fill `src` with an 1x1px transparent png file to hide the broken image link
				 * Sorry about this one..
				 */
				// img.setAttribute('src', base64_px);
				// img.style.backgroundImage = `url(${atlasURL})`;
				// img.style.backgroundSize = backgroundSize;
				// img.style.backgroundPosition = backgroundPosition;

				img.setAttribute('src', getEmojiURL(found.skin, style));
				img.classList.add('emoji');
				img.setAttribute('alt', rawText);

				modified = true;
				fragment.appendChild(img);
			} else {
				fragment.appendChild(createText(rawText, false));
			}
		}

		// is there actually anything to replace in here ?
		if (modified) {
			// any text left to be added ?
			if (i < text.length) {
				fragment.appendChild(createText(text.slice(i), true));
			}

			// replace the text node only, leave intact
			// anything else surrounding such text
			subnode.parentNode?.replaceChild(fragment, subnode);
		}
	}

	return node;
}
