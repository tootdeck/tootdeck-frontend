export function save(key: string, value: any) {
	window.localStorage.setItem(key, JSON.stringify(value));
}

export function load<T>(key: string): T | undefined {
	try {
		const raw = window.localStorage.getItem(key);
		return raw ? JSON.parse(raw) : undefined;
	} catch (e) {
		console.error(e);
		return undefined;
	}
}
