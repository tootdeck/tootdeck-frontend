export function createDictionary<T>(array: Array<string>, default_value: T): Record<string, T> {
	const dictionary: Record<string, T> = {};

	for (const name of array) {
		dictionary[name] = default_value;
	}

	return dictionary;
}

export function reCreateDictionary<T>(entries: Array<[string, T]>): Record<string, T> {
	const dictionary: Record<string, T> = {};

	for (const [key, value] of entries) {
		dictionary[key] = value;
	}

	return dictionary;
}
