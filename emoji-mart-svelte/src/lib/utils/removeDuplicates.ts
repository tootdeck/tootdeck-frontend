export function removeDuplicates(array: Array<string | number>) {
	return Array.from(new Set(array));
}
