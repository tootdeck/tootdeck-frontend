export function capitalize(s: string) {
	const first = s.charAt(0).toUpperCase();
	const remain = s.substring(1);
	return first + remain;
}
