import { getContext, setContext } from 'svelte';

import type { ContextMap } from '../../types/context.type';

export function typedSetContext<K extends keyof ContextMap>(key: K, value: ContextMap[K]) {
	setContext(key, value);
}

export function typedGetContext<K extends keyof ContextMap>(key: K): ContextMap[K] {
	return getContext(key);
}
