import type { Writable } from 'svelte/store';

import type { TootDeckAccount } from '../stores/Account/Accounts';

import type { ParsedAccount } from './contentParsed';

export interface ProfileProps {
	account: Writable<TootDeckAccount>;
	status_account: Writable<ParsedAccount | (() => Promise<ParsedAccount | null>)>;
}

export enum ActiveTab {
	Posts = 0,
	Replies = 1,
	Pinned = 2,
	Media = 3,
}

export const ActiveTabs: Record<string, ActiveTab> = {
	Posts: ActiveTab.Posts,
	Replies: ActiveTab.Replies,
	Pinned: ActiveTab.Pinned,
	Media: ActiveTab.Media,
};
