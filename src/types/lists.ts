import type { Entity } from './mastodonEntities';

export interface ListAccount extends Entity.List {
	accounts: Array<Entity.Account>;
}

export interface ListAccountLength extends Entity.List {
	length: number;
	in_list: boolean;
	animate?: string;
}
