export namespace MisskeyEntity {
	export interface Account {
		id: string;
		name: string;
		username: string;
		host: string | null;
		avatarUrl: string;
		avatarBlurhash: string;
		avatarDecorations: Array<Decoration>;
		isBot: boolean;
		isCat: boolean;
		emojis: { [key: string]: string };
		onlineStatus: 'active';
		badgeRoles: Array<BadgeRole>;
		url: null;
		uri: null;
		movedTo: null;
		alsoKnownAs: null;
		createdAt: string;
		updatedAt: string;
		lastFetchedAt: null;
		bannerUrl: string;
		bannerBlurhash: string;
		isLocked: boolean;
		isSilenced: boolean;
		isLimited: boolean;
		isSuspended: boolean;
		description: string;
		location: string;
		birthday: string;
		lang: string;
		fields: Array<Field>;
		verifiedLinks: Array<string>;
		followersCount: number;
		followingCount: number;
		notesCount: number;
		pinnedNoteIds: Array<string>;
		pinnedNotes: Array<Note>;
		pinnedPageId: null;
		pinnedPage: null;
		publicReactions: boolean;
		followersVisibility: Visibility;
		followingVisibility: Visibility;
		twoFactorEnabled: boolean;
		usePasswordLessLogin: boolean;
		securityKeys: boolean;
		roles: Array<Role>;
		memo: null;
	}

	export interface BadgeRole {
		name: string;
		iconUrl: string;
		displayOrder: number;
	}

	export interface Decoration {
		id: string;
		url: string;
	}

	export interface Field {
		name: string;
		value: string;
	}

	export interface File {
		id: string;
		createdAt: string;
		name: string;
		type: string;
		md5: string;
		size: number;
		isSensitive: boolean;
		blurhash: string;
		properties: {
			width: number;
			height: number;
		};
		url: string;
		thumbnailUrl: string;
		comment: null;
		folderId: null;
		folder: null;
		userId: string;
		user: null;
	}

	export interface Note {
		id: string;
		createdAt: string;
		userId: string;
		user: PartialAccount;
		text: string;
		cw: null;
		visibility: Visibility;
		localOnly: boolean;
		reactionAcceptance: null;
		renoteCount: number;
		repliesCount: number;
		reactionCount: number;
		reactions: { [key: string]: number };
		reactionEmojis: { [key: string]: string };
		tags: Array<string>;
		fileIds: Array<string>;
		files: Array<File>;
		replyId: null;
		renoteId: null;
		clippedCount: number;
	}

	export interface PartialAccount {
		id: string;
		name: string;
		username: string;
		host: string | null;
		avatarUrl: string;
		avatarBlurhash: string;
		avatarDecorations: [];
		isBot: boolean;
		isCat: boolean;
		emojis: { [key: string]: string };
		onlineStatus: boolean;
		badgeRoles: [];
	}

	export interface Role {
		id: string;
		name: string;
		color: string;
		iconUrl: string;
		description: string;
		isModerator: boolean;
		isAdministrator: boolean;
		displayOrder: number;
	}

	export enum Visibility {
		Public = 'public',
		Private = 'private',
	}
}
