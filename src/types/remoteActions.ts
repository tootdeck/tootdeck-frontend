export enum RemoteActions {
	Follow = 0,
	Favourite = 1,
	Bookmark = 2,
	Reaction = 3,
}
