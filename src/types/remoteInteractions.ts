import type { EmojiReaction } from './contentParsed';

export interface RemoteInteractions {
	id: string;
	replies_count: number;
	reblogs_count: number;
	favourites_count: number;
	emoji_reactions: Array<EmojiReaction>;
	application: string | null;
}
