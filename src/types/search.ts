import type { Readable, Writable } from 'svelte/store';

import type { TootDeckAccount } from '../stores/Account/Accounts';

import type { Composer } from './composer';
import type { ParsedAccount, ParsedStatus } from './contentParsed';
import type { Entity } from './mastodonEntities';

export interface SearchHistory {
	toggle: {
		type: boolean;
		has: boolean;
		is: boolean;
		language: boolean;
		from: boolean;
		before: boolean;
		during: boolean;
		after: boolean;
		in: boolean;
	};
	values: {
		type: {
			accounts: boolean;
			hashtags: boolean;
			statuses: boolean;
		};
		has: {
			media: boolean;
			poll: boolean;
			embed: boolean;
		};
		is: {
			reply: boolean;
			sensitive: boolean;
		};
		language: string;
		from: {
			id: string;
			acct: string;
		};
		me: boolean;
		before: string;
		during: string;
		after: string;
		in: {
			all: boolean;
			library: boolean;
		};
		query: string;
	};
}

export interface Search {
	composer: Composer; // for `From.svelte`
	account: Writable<TootDeckAccount>;
	props: {
		searched: Writable<boolean>;
		search_result: Writable<{
			accounts: Array<ParsedAccount>;
			hashtags: Array<Entity.Tag>;
			statuses: Array<ParsedStatus>;
		}>;
		view: Writable<{
			simple: boolean;
			advanced: boolean;
		}>;
	};
	toggle: Writable<{
		type: boolean;
		has: boolean;
		is: boolean;
		language: boolean;
		from: boolean;
		before: boolean;
		during: boolean;
		after: boolean;
		in: boolean;
	}>;
	values: {
		type: Writable<{
			accounts: boolean;
			hashtags: boolean;
			statuses: boolean;
		}>;
		has: Writable<{
			media: boolean;
			poll: boolean;
			embed: boolean;
		}>;
		is: Writable<{
			reply: boolean;
			sensitive: boolean;
		}>;
		language: Writable<string>;
		from: Writable<{
			id: string;
			acct: string;
		}>;
		me: Writable<boolean>;
		before: Writable<string>;
		during: Writable<string>;
		after: Writable<string>;
		in: Writable<{
			all: boolean;
			library: boolean;
		}>;
		query: Writable<string>;
	};
	show: {
		language_selector: Writable<boolean>;
		tag_selector: Writable<boolean>;
	};
	output: Readable<string>;
}
