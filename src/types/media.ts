export interface IterableMedia {
	id: number;
	uid?: string; // id on instance
	src: string;
	name: string;
	type: string;
	alt: { text: string };
}

export interface MediaFile extends File {
	description: {
		text: string;
	};
	// On instance
	uid?: string; // id
	url?: string; // url
	media_type?: string; // type
	old_description?: string; // description
}
