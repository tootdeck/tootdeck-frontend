export interface AutoDetect {
	auto: boolean;
	disable: boolean;
	enable: boolean;
}
