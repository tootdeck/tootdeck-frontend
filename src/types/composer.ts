import type { Writable } from 'svelte/store';
import type { Readable } from 'svelte/motion';

import type { TootDeckAccount } from '../stores/Account/Accounts';

import type { ReplyAccount } from '../lib/ComponentFunctions/getReplyingAccounts';
import type { StatusOptionalPoll } from '../lib/APIs/MirrorAPI/types';

import type { ParsedStatus } from './contentParsed';
import type { IterableMedia, MediaFile } from './media';
import type { Entity } from './mastodonEntities';

export interface ComposerData {
	replying_to_status: ParsedStatus | undefined;
	replying_to_accounts: Array<ReplyAccount>;
	content_warning: string;
	sensitive: boolean;
	poll: StatusOptionalPoll;
	language: string;
	visibility: string;
	text: string;
	media: Array<IterableMedia>;

	// -------------------
	account: TootDeckAccount;
	edition: boolean;
	redraft: boolean;
	quote: boolean;
	lite: boolean;
	added_media: Array<File>;
}

export interface ComposerSendingInfo {
	id: number;
	uid: string; // id on the instance
	size: number;
	sent_size: number;
	processed: boolean;
}

export enum ComposerSuggestionTarget {
	Textarea = 'textarea',
	Input = 'input',
}

export interface Composer {
	replying_to_status: Writable<ParsedStatus | undefined>;
	replying_to_accounts: Writable<Array<ReplyAccount>>;
	content_warning: Writable<string>;
	sensitive: Writable<boolean>;
	poll: Writable<StatusOptionalPoll>;
	language: Writable<string>;
	visibility: Writable<Entity.Visibility>;
	text: Writable<string>;
	media: Writable<Array<MediaFile>>;

	// -------------------
	uuid: string;
	account: Writable<TootDeckAccount>;
	/**
	 * [`Handle`, `TootDeckAccount`, `toggle`]
	 */
	enabled_accounts: Writable<Array<[string, TootDeckAccount, boolean]>>;
	remaining_char: Readable<number>;
	edition: Writable<boolean>;
	redraft: Writable<boolean>;
	quote: Writable<boolean>;
	lite: boolean;
	invalid: Readable<boolean>;
	publish_button_text: Writable<string>;
	sending: Writable<boolean>;
	loading: Writable<boolean>;
	picked_emoji: Writable<string>;
	failed_remote_fetch: Writable<boolean>;
	added_media: Writable<Array<File>>;
	sending_media: Writable<Array<ComposerSendingInfo>>;
	suggestion_target: Writable<ComposerSuggestionTarget>;
	toggle: {
		poll: Writable<boolean>;
		content_warning: Writable<boolean>;
		clear: Readable<boolean>;
	};

	// -------------------
	selector: {
		tag: Writable<boolean>;
		emoji: Writable<boolean>;
		visibility: Writable<boolean>;
		hashtag: Writable<boolean>;
	};
	search: {
		tag: Writable<string>;
		emoji: Writable<string>;
		hashtag: Writable<string>;
	};
	found: {
		tag: Writable<string>;
		emoji: Writable<string>;
		hashtag: Writable<string>;
	};
}
