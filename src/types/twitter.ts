export namespace Twitter {
	export enum MediaType {
		Image = 'image',
		Gif = 'gif',
		Video = 'video',
	}

	export interface MediaSize {
		height: number;
		width: number;
	}

	export interface Media {
		altText: string | null;
		duration_millis: number;
		size: Twitter.MediaSize;
		thumbnail_url: string;
		type: Twitter.MediaType;
		url: string;
	}

	export interface Response {
		allSameType: boolean;
		combinedMediaUrl: string | null;
		communityNote: string | null;
		conversationID: string;
		date: string;
		date_epoch: number;
		hasMedia: boolean;
		hashtags: Array<string>;
		likes: number;
		mediaURLs: Array<string>;
		media_extended: Array<Twitter.Media>;
		pollData: string | null;
		possibly_sensitive: boolean;
		qrt: string | null;
		qrtURL: string | null;
		replies: number;
		retweets: number;
		text: string;
		tweetID: string;
		tweetURL: string;
		user_name: string;
		user_profile_image_url: string;
		user_screen_name: string;
		/**
		 * {
		 *	"allSameType": true,
		 *	"combinedMediaUrl": null,
		 *	"communityNote": null,
		 *	"conversationID": "1792184009755390314",
		 *	"date": "Sun May 19 13:22:19 +0000 2024",
		 *	"date_epoch": 1716124939,
		 *	"hasMedia": true,
		 *	"hashtags": [
		 *	  "persona3"
		 *	],
		 *	"likes": 1300,
		 *	"mediaURLs": [
		 *	  "https://pbs.twimg.com/media/GN8d2ByacAADfbK.jpg"
		 *	],
		 *	"media_extended": [
		 *	  {
		 *		"altText": null,
		 *		"size": {
		 *		  "height": 2048,
		 *		  "width": 2048
		 *		},
		 *		"thumbnail_url": "https://pbs.twimg.com/media/GN8d2ByacAADfbK.jpg",
		 *		"type": "image",
		 *		"url": "https://pbs.twimg.com/media/GN8d2ByacAADfbK.jpg"
		 *	  }
		 *	],
		 *	"pollData": null,
		 *	"possibly_sensitive": false,
		 *	"qrt": null,
		 *	"qrtURL": null,
		 *	"replies": 13,
		 *	"retweets": 361,
		 *	"text": "Absolution\n#persona3 https://t.co/hLtspz2HgE",
		 *	"tweetID": "1792184009755390314",
		 *	"tweetURL": "https://twitter.com/KickThat_Can/status/1792184009755390314",
		 *	"user_name": "🌕",
		 *	"user_profile_image_url": "https://pbs.twimg.com/profile_images/1748698804688437249/tjSIeVa__normal.jpg",
		 *	"user_screen_name": "KickThat_Can"
		 *  }
		 */
	}
}
