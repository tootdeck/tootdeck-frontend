import type { Diff } from '@sanity/diff-match-patch';

import type { ParsedNode } from '../lib/ContentParser/flattenHTML';

import type { Entity } from './mastodonEntities';

type AccountOverride = 'display_name' | 'note' | 'fields';
type StatusOverride = 'account' | 'content' | 'reblog' | 'emoji_reactions';
type NotificationOverride = 'account' | 'content' | 'status' | 'type';

export interface ParsedField {
	name: string;
	value: Array<ParsedNode>;
	verified_at: string | null;
}

export interface ParsedAccount extends Omit<Entity.Account, AccountOverride> {
	display_name: Array<ParsedNode> | null;
	note: Array<ParsedNode>;
	fields: Array<ParsedField>;
	tootdeck: {
		plain_content: string;
		/** Increase when there is a visual diff */
		update: number;
		uuid: string;
	};
}

export interface LinkedToStatus {
	id: string;
	quote: boolean;
}

export interface EmojiReaction {
	uuid: string;
	me: boolean;
	count: number;
	name: string;
	url: string | undefined;
	fallback: string | undefined;
	static_url: string | undefined;
}

export interface Attachment extends Entity.Attachment {
	diff?: Array<Diff>;
}

export interface ParsedStatus extends Omit<Entity.Status, StatusOverride> {
	account: ParsedAccount;
	content: Array<ParsedNode> | null;
	reblog: ParsedStatus | null;
	plain_content: string;
	emoji_reactions: Array<EmojiReaction>;
	media_attachments: Array<Attachment>;
	uri: 'TootDeck:btn:previous' | string;
	tootdeck: {
		height: number;
		in_notification_id: Array<string>;
		in_status: Array<LinkedToStatus>;
		show_cw: boolean;
		media_attachments_show: boolean | null;
		quote_urls: Array<string>;
		/** Increase when a new version is parsed */
		update: number;
		uuid: string;
		is_twitter_embed: boolean;
		embed: {
			twitter: Array<string>;
			youtube: Array<string>;
			dailymotion: Array<string>;
		};
	};
}

export interface ParsedNotification extends Omit<Entity.Notification, NotificationOverride> {
	accounts: Array<ParsedAccount>;
	notify: boolean;
	status: ParsedStatus | null;
	type: Entity.NotificationType | 'TootDeck:btn:previous';
	uuid: string;
}

export interface ParsedAccountStatus extends Omit<Entity.Status, 'account' | 'reblog'> {
	account: ParsedAccount;
	reblog: null | ParsedAccountStatus;
}

export interface ParsedAccountNotification extends Omit<Entity.Notification, 'account' | 'status'> {
	account: ParsedAccount;
	status: null | ParsedAccountStatus;
}

/**
 * parseAnchor
 */
export enum ClickEventOverride {
	None = 0,
	Both = 1,
	StopPropagation = 2,
	PreventDefault = 3,
}

export interface AnchorCallbackData {
	event: string;
	detail: any;
}

export interface AnchorCallback {
	clickOverride: ClickEventOverride;
	data?: AnchorCallbackData;
}

export interface ParsedAnchor {
	attribs: object;
	cb?: () => AnchorCallback;
}

/**
 * Diff
 */
export type ParsedDiff = [number, Array<ParsedNode>];
