import './app.css';
import App from './App.svelte';

import { EventsManager } from './lib/Events/EventsManager';
import { ContentManager } from './lib/Managers/ContentManager';
import { AppInit } from './AppInit';

Error.stackTraceLimit = Infinity;

AppInit;
EventsManager;
ContentManager;

const app = new App({
	target: document.getElementById('app') as Element,
});

export default app;
