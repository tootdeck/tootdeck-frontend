import { writable, type Writable } from 'svelte/store';

import { init as initAccounts, initAccountsOptions } from './Account/Accounts';
import { init as initOptions } from './Options';
import { init as initColumns } from '../lib/Columns/ColumnFactory';
import { init as initLayout } from './Columns/Layout';
import { init as initOptionalFeatures } from './OptionalFeatures';
import { initializedState, loadingUpdateState } from './loading';
import { GlobalWindow, GlobalWindowState } from './Window';
import { notificationModal, NotificationModalType } from './NotificationsModal';

import { init as initRelationship } from '../lib/Cache/Relationships';
import { errorModal } from '../lib/errorModal';
import { Sessions } from '../lib/Sessions';
import { TootDeckConfig, type ConfigResponse } from '../lib/TootDeckConfig';
import { SoundsLibrary } from '../lib/SoundsLibrary';

import { SettingsState } from '../types/settings';

export let stInit: Writable<boolean> = writable(false);
export let stRelationshipBuildDate: Writable<number> = writable(
	Number.parseInt(window.localStorage.getItem('relationships_build_date') ?? '0')
);

stRelationshipBuildDate.subscribe((v) => {
	const key = 'relationships_build_date';
	const storage = window.localStorage.getItem(key);
	if (storage === null || Number.parseInt(storage) !== v) {
		window.localStorage.setItem(key, v.toString());
	}
});

function initSoundsLibrary(config: ConfigResponse) {
	loadingUpdateState('initializing sounds library');

	SoundsLibrary.import(config.value!.sounds_library ?? []);
}

export async function initConfig(dashboard: boolean) {
	const config = (await TootDeckConfig.import()) as ConfigResponse;

	initOptions(config);
	initAccountsOptions(config);
	initSoundsLibrary(config);

	if (!dashboard) {
		if (!initColumns()) {
			return false;
		}

		await initLayout(config);
	}

	return true;
}

async function checkSessions() {
	const diff = await Sessions.init(true);
	if (!diff) {
		return;
	}

	notificationModal.create(
		NotificationModalType.Info,
		"A new session has been created when you were away, if it's not you click here to revoke the session and change your instance account password.",
		false,
		() =>
			GlobalWindow.create({
				state: GlobalWindowState.Setting,
				special: false,
				category: SettingsState.Sessions,
			})
	);
}

export async function initStores(dashboard: boolean) {
	await initOptionalFeatures();

	const account = await initAccounts();
	if (!account) {
		errorModal('Unable to initialize accounts', undefined, true);
		return false;
	}

	const config = await initConfig(dashboard);
	if (!config) {
		errorModal('Unable to initialize app with user configuration', undefined, true);
		return false;
	}

	initializedState(true);

	if (!dashboard) {
		initRelationship();
		checkSessions();
	}

	return true;
}
