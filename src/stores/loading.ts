import { get, writable, type Writable } from 'svelte/store';

export let stLoadingStatus: Writable<Array<string>> = writable([]);
export let stInitialized: Writable<boolean> = writable(false);

export function initializedState(value: boolean) {
	stInitialized.update(() => value);
}

export function loadingUpdateState(state: string) {
	if (get(stInitialized)) {
		return;
	}

	stLoadingStatus.update((v) => {
		v.push(state);
		return v;
	});
}
