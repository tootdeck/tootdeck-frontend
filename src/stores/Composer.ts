import { writable, type Writable } from 'svelte/store';

class ComposerDragNDropInner {
	static readonly instance = new ComposerDragNDropInner();

	private open_composer: Array<number> = [];
	private id: number = 1;

	private updateID() {
		stComposerOpenID.set(this.open_composer.at(-1) ?? 0);
	}

	open() {
		this.open_composer.push(++this.id);
		this.updateID();
		return this.id;
	}

	close(id: number) {
		this.open_composer = this.open_composer.filter((x) => x !== id);
		this.updateID();
	}
}

export const ComposerDragNDrop = ComposerDragNDropInner.instance;

export let stComposerOpenID: Writable<number> = writable(0);
