import { get, writable, type Writable } from 'svelte/store';

import { stLayout } from '../Columns/Layout';
import { notificationModal, NotificationModalType } from '../NotificationsModal';
import { getEmojis } from '../Emojis';
import { loadingUpdateState } from '../loading';
import { stOptionsReadOnly } from '../Options';
import { stSearchHistoryUpdate } from '../Search';

import { CacheStorage, type DataFormat } from '../../lib/Cache/CacheStorage';
import { TootDeckAPI } from '../../lib/APIs/TootDeckAPI/TootDeckAPI';
import { MirrorAPI } from '../../lib/APIs/MirrorAPI/MirrorAPI';
import { errorModal } from '../../lib/errorModal';
import type {
	AccountResponse,
	AuthGetResponse,
} from '../../lib/APIs/TootDeckAPI/Methods/Auth/types';
import { AccountsMananger } from '../../lib/Managers/AccountsManager';
import { TootDeckConfig, type ConfigResponse } from '../../lib/TootDeckConfig';
import { filterMutate } from '../../lib/Utils/filter/filterMutate';
import {
	filterDuplicate,
	filterDuplicateStringOnly,
} from '../../lib/Utils/filter/filterDuplicates';
import { stringifyHandle } from '../../lib/Utils/handle/stringifyHandle';

import type { ParsedAccount } from '../../types/contentParsed';
import type { InstanceHandle } from '../../types/handle';
import type { SearchHistory } from '../../types/search';
import type { Entity } from '../../types/mastodonEntities';

/**
 * Types
 */
export interface TootDeckAccountOptions {
	publishConfirmationStep: boolean;
	defaultAccount: boolean;
	moderator: boolean; // Override instance response
}

export interface EntityRelationship extends Entity.Relationship {
	acct: string;
}

export interface TootDeckAccount {
	mirrorAPI: MirrorAPI;
	instance: Entity.Instance;
	entity: ParsedAccount;
	relationship: CacheStorage<EntityRelationship>;
	options: Writable<TootDeckAccountOptions>;
	searchHistory: CacheStorage<SearchHistory>;
}

/**
 * Stores
 */
export let stAccounts: Writable<Map<string, TootDeckAccount>> = writable(new Map());
export let stAccountDefault: Writable<string> = writable();
export let stAccountLogin: Writable<TootDeckAccount> = writable();
export let stAccountsSameInstance: Writable<boolean> = writable(false);
export let stRefreshAccount: Writable<number> = writable(1);
export let stMe: Writable<Array<AccountResponse>> = writable();

/**
 * Functions
 */
//#region

export class Account {
	static get(handle: string | InstanceHandle): TootDeckAccount;
	static get(handle: string | InstanceHandle, throwing: false): TootDeckAccount | undefined;
	static get(handle: string | InstanceHandle, throwing: true): TootDeckAccount;
	static get(handle: string | InstanceHandle, throwing: boolean = true): TootDeckAccount {
		let handle_string = typeof handle === 'string' ? handle : stringifyHandle(handle);

		const account = get(stAccounts).get(handle_string);
		if (!account && throwing) {
			errorModal(
				`Fatal error: ${handle} isn't in accounts`,
				['accounts: ', get(stAccounts).values()],
				true
			);
			throw new Error();
		}

		return account!;
	}

	static remove(handle: InstanceHandle) {
		get(stLayout).forEach((column) => {
			const column_account = column.params.account;
			if (
				column_account.username === handle.username &&
				column_account.domain === handle.domain
			) {
				column.destroy();
			}
		});

		stAccounts.update((v) => {
			v.delete(handle.username + '@' + handle.domain);
			return v;
		});
	}

	static reorder(new_order: Array<TootDeckAccount>) {
		const new_map = new Map<string, TootDeckAccount>(
			new_order.map((acc) => [acc.mirrorAPI.handle_string, acc])
		);
		stAccounts.set(new_map);
	}

	static getLoginAccount() {
		return get(stAccountLogin);
	}

	static isLoginAccount(account: TootDeckAccount) {
		return get(stAccountLogin).mirrorAPI.handle_string === account.mirrorAPI.handle_string;
	}
}

export function accountsSameInstance() {
	const map_array = Array.from(get(stAccounts));
	const domains = map_array.map(([_, value]) => value.mirrorAPI.domain);
	const no_duplicate = filterDuplicateStringOnly(domains);

	stAccountsSameInstance.update((v) => no_duplicate.length === 1);
}

//#endregion

/**
 * Init
 */
//#region

// Create MirrorAPI instance
function createMirrors(me: AuthGetResponse) {
	const mirrors: Array<MirrorAPI> = [];

	// Main account
	mirrors.push(
		new MirrorAPI({ username: me.main.username, domain: me.main.domain }, me.main.type)
	);

	// Secondary accounts
	for (const account of me.secondary) {
		mirrors.push(
			new MirrorAPI({ username: account.username, domain: account.domain }, account.type)
		);
	}

	return mirrors;
}

// Get instances infos
async function getInstancesInfos(mirrors: Array<MirrorAPI>) {
	// Remove instance duplicates
	const unique_instance = filterDuplicate(mirrors, 'domain');

	// Get instance infos
	const instance_entities: Array<{
		domain: string;
		data: Entity.Instance | null;
	}> = (
		await Promise.all(
			unique_instance.map((instance) =>
				instance.Instance.get().then((r) => ({
					domain: instance.domain,
					data: r,
				}))
			)
		)
	).filter((x) => x.data);

	if (!instance_entities.length) {
		errorModal(`Unable to get instances informations`, undefined, true);
		return null;
	}

	if (mirrors.length !== instance_entities.length) {
		const filter = mirrors
			.filter((mirror) => !instance_entities.find((x) => x.domain === mirror.domain))
			.map((mirror) => mirror.handle_string);
		filterMutate(mirrors, (mirror) => {
			const domain = mirror.domain;
			if (!instance_entities.find((x) => x.domain === domain)) {
				errorModal(`${domain} timed out`, undefined, true);
				return false;
			}

			return true;
		});

		if (filter.length) {
			errorModal(
				`${filter.join(', ')} ${
					filter.length === 1 ? 'is' : 'are'
				} missing, the app is in an unknown state. To preserve your configuration, nothing will be saved until the issue is resolved.`,
				undefined,
				false
			);
			stOptionsReadOnly.set(true);
		}
	}

	const instances = new Map<String, Entity.Instance>();
	const emojis_promises = instance_entities.map((entity) => {
		const promise = getEmojis(unique_instance.find((x) => x.domain === entity.domain)!);

		const instance = entity.data!;

		instance.support_emoji_reactions = !!(
			instance.configuration.reactions ??
			instance.configuration.features?.find((x) => x.includes('emoji_reactions')) ??
			false
		);
		instances.set(entity.domain, instance);

		return promise;
	});

	await Promise.all(emojis_promises);

	return instances;
}

// Refresh instance token
export async function refreshInstanceToken(handle: string) {
	const redirect_url = await TootDeckAPI.OAuth.refresh(handle);
	if (!redirect_url) {
		errorModal('Unable to redirect', undefined, true);
		return;
	}

	window.location.href = redirect_url;
}

const convert_localStorage_relationship: DataFormat<EntityRelationship> = {
	load: (data) =>
		data
			/**
			 * Create relationship entities with account id and with an alias to account acct
			 */
			.map(([key, entity]) => [
				[entity.acct, entity],
				[entity.id, entity],
			])
			.flat()
			/**
			 * Remove undefined relationship entities
			 */
			.filter(([key, entity]) => key) as Array<[string, EntityRelationship]>,

	store: (data) =>
		/**
		 * Remove account acct aliases from stored relationship entities to save space
		 */
		data.filter(([key, value]) => key.includes('@')),
};

const convert_localStorage_search_history: DataFormat<SearchHistory> = {
	/**
	 * Order map and initialize store
	 */
	load: (data) => {
		const ordered = data.sort((a, b) => {
			const [aKey] = a;
			const [bKey] = b;

			return aKey.toLowerCase() > bKey.toLowerCase() ? -1 : 1;
		});

		stSearchHistoryUpdate.update((v) => ++v);
		return ordered;
	},
	/**
	 * Update store
	 */
	store: (data) => {
		stSearchHistoryUpdate.update((v) => ++v);
		return data;
	},
};

// Get accounts infos
async function getAccountsInfos(
	mirrors: Array<MirrorAPI>,
	instances: Map<String, Entity.Instance>
) {
	const account_entities: Array<Entity.Account | null> = await Promise.all(
		mirrors.map((instance) => instance.Accounts.verifyCredentials())
	);

	let default_account: string = '';
	const domains: Array<string> = [];
	const accounts = new Map<string, TootDeckAccount>();

	for (const i in mirrors) {
		const mirror = mirrors[i];
		const handle = mirror.handle_string;
		const entity = account_entities[i];

		if (!entity) {
			// Refresh instance token
			notificationModal.create(
				NotificationModalType.Info,
				'Click to refresh your session for ' + handle,
				false,
				() => refreshInstanceToken(handle)
			);
			return null;
		}

		const domain = mirror.domain;
		if (!domains.includes(domain)) {
			domains.push(domain);
		}

		const account_handle = mirror.handle_string;

		if (!default_account) {
			default_account = account_handle;
		}

		const options: TootDeckAccountOptions = {
			publishConfirmationStep: false,
			defaultAccount: false,
			moderator: false,
		};

		const account: TootDeckAccount = {
			mirrorAPI: mirror,
			// @ts-ignore
			entity: entity,
			instance: instances.get(domain)!,
			relationship: new CacheStorage<EntityRelationship>('relationships_' + handle, {
				threshold: 10,
				wait: 30 * 1000,
				formatData: convert_localStorage_relationship,
			}),
			options: writable(options),
			searchHistory: new CacheStorage<SearchHistory>('search_history_' + handle, {
				threshold: 1,
				limit: 100,
				formatData: convert_localStorage_search_history,
			}),
		};

		account.entity = AccountsMananger.store(account, entity);

		accounts.set(account_handle, account);
	}

	if (!accounts.size) {
		return null;
	}

	return { accounts, default_account, same_instance: domains.length === 1 };
}

export async function init() {
	/**
	 * Verify login state
	 */
	loadingUpdateState('Login in');
	const me = await TootDeckAPI.Auth.me();
	if (!me) {
		return false;
	}

	/**
	 * Create MirrorAPI instance
	 */
	const mirrors = createMirrors(me);

	/**
	 * Get instances infos
	 */
	loadingUpdateState('Getting instances info');
	const instances = await getInstancesInfos(mirrors);
	if (!instances) {
		return false;
	}

	/**
	 * Get accounts infos
	 */
	loadingUpdateState('Getting accounts infos');
	const accounts_infos = await getAccountsInfos(mirrors, instances);
	if (!accounts_infos) {
		return false;
	}

	/**
	 * Set stores
	 */
	stAccounts.set(accounts_infos.accounts);
	stAccountDefault.set(accounts_infos.default_account);
	stAccountLogin.set(Array.from(accounts_infos.accounts.values())[0]);
	stAccountsSameInstance.set(accounts_infos.same_instance);
	stMe.set([me.main, ...me.secondary]);

	return true;
}

/**
 * Config
 */
let initialized: boolean = false;

function save() {
	if (!initialized) {
		return;
	}

	TootDeckConfig.save();
}

export function initializedState(value: boolean) {
	initialized = value;
}

export function initAccountsOptions(config: ConfigResponse) {
	const accounts_options = config?.value?.accounts_options;
	if (accounts_options) {
		accounts_options.forEach((account_opts) => {
			const account = Account.get(account_opts.handle, false);
			if (account) {
				account.options.set(account_opts.value);

				// Set default account
				if (account_opts.value.defaultAccount) {
					stAccountDefault.set(account_opts.handle);
				}

				// Set accounts options auto save
				let first_time = true;
				account.options.subscribe(() => {
					if (first_time) {
						first_time = false;
						return;
					}

					save();
				});
			}
		});
	}

	initialized = true;
}

stAccounts.subscribe(() => save());

//#endregion
