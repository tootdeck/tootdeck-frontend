import { writable, type Writable } from 'svelte/store';
import type { Diff } from '@sanity/diff-match-patch';

import type { TootDeckAccount } from './Account/Accounts';

import type { IColumn, IColumnType } from '../lib/Columns/types/interfaces';
import type { ReplyAccount } from '../lib/ComponentFunctions/getReplyingAccounts';
import type { ThreadColumn } from '../lib/Columns/ThreadColumn';

import type { ParsedAccount, ParsedStatus } from '../types/contentParsed';
import type { RemoteActions } from '../types/remoteActions';
import type { SettingsState } from '../types/settings';
import type { IterableMedia } from '../types/media';
import type { FilterState } from '../types/filters';
import type { Entity } from '../types/mastodonEntities';

/**
 * Types
 */
export type GlobalWindowAllowed =
	| GlobalWindowSetting
	| GlobalWindowReblog
	| GlobalWindowNewColumn
	| GlobalWindowNewColumnWhichAccount
	| GlobalWindowFullMedia
	| GlobalWindowFullMediaWithThread
	| GlobalWindowAlt
	| GlobalWindowReply
	| GlobalWindowList
	| GlobalWindowListManager
	| GlobalWindowRemoteActions
	| GlobalWindowConfirm
	| GlobalWindowStatusHistory
	| GlobalWindowWhoInteracted
	| GlobalWindowWhoFollow
	| GlobalWindowAccountsList
	| GlobalWindowFilters
	| GlobalWindowChangelog
	| null;

export enum GlobalWindowState {
	Setting = 1,
	Reblog = 2,
	NewColumn = 3,
	WhichAccount = 4,
	FullMedia = 5,
	FullMediaWithThread = 6,
	Alt = 7,
	Reply = 8,
	List = 9,
	ListManager = 10,
	RemoteActions = 11,
	Confirm = 12,
	StatusHistory = 13,
	WhoInteracted = 14,
	WhoFollow = 15,
	AccountsList = 16,
	Filters = 17,
	Changelog = 18,
}

interface GlobalWindowContent {
	state: GlobalWindowState;
	special: boolean;
}

export interface GlobalWindowSetting extends GlobalWindowContent {
	state: GlobalWindowState.Setting;
	special: false;
	category?: SettingsState;
}

export interface GlobalWindowReblog extends GlobalWindowContent {
	state: GlobalWindowState.Reblog;
	special: false;
	account: TootDeckAccount;
	status: ParsedStatus;
}

export interface GlobalWindowNewColumn extends GlobalWindowContent {
	state: GlobalWindowState.NewColumn;
	special: false;
}

export interface GlobalWindowNewColumnWhichAccount extends GlobalWindowContent {
	state: GlobalWindowState.WhichAccount;
	special: false;
	selected_account_id: string;
	selected_column_type: IColumnType;
	selected_column: IColumn | null;
}

export interface GlobalWindowFullMedia extends GlobalWindowContent {
	state: GlobalWindowState.FullMedia;
	special: true;
	clicked: number;
	media: Array<Entity.Attachment>;
}

export interface GlobalWindowFullMediaWithThread extends GlobalWindowContent {
	state: GlobalWindowState.FullMediaWithThread;
	special: true;
	clicked: number;
	media: Array<Entity.Attachment>;
	column: ThreadColumn;
	status_id: string;
}

export interface GlobalWindowAlt extends GlobalWindowContent {
	state: GlobalWindowState.Alt;
	special: false;
	alt: { text: string };
	media?: IterableMedia;
	editable?: boolean;
	diff?: Array<Diff>;
}

export interface GlobalWindowReply extends GlobalWindowContent {
	state: GlobalWindowState.Reply;
	account: TootDeckAccount;
	status_account: ParsedAccount;
	replying_to: Array<ReplyAccount>;
	readonly: boolean;
}

export interface GlobalWindowList extends GlobalWindowContent {
	state: GlobalWindowState.List;
	account: TootDeckAccount;
	status_account: ParsedAccount;
}

export interface GlobalWindowListManager extends GlobalWindowContent {
	state: GlobalWindowState.ListManager;
	special: false;
	account?: TootDeckAccount;
}

export interface GlobalWindowRemoteActions extends GlobalWindowContent {
	state: GlobalWindowState.RemoteActions;
	special: false;
	action: RemoteActions;
	status_account?: ParsedAccount;
	status?: ParsedStatus;
}

export interface GlobalWindowConfirm extends GlobalWindowContent {
	state: GlobalWindowState.Confirm;
	special: false;
	text: string;
	confirm: () => void;
	cancel?: () => void;
}

export interface GlobalWindowStatusHistory extends GlobalWindowContent {
	state: GlobalWindowState.StatusHistory;
	special: false;
	account: TootDeckAccount;
	status: ParsedStatus;
}

export interface GlobalWindowWhoInteracted extends GlobalWindowContent {
	state: GlobalWindowState.WhoInteracted;
	special: false;
	account: TootDeckAccount;
	status: ParsedStatus;
	type: 'reblog' | 'favourite';
}

export interface GlobalWindowWhoFollow extends GlobalWindowContent {
	state: GlobalWindowState.WhoFollow;
	special: false;
	account: TootDeckAccount;
	status_account: ParsedAccount;
	type: 'following' | 'followers';
}

export interface GlobalWindowAccountsList extends GlobalWindowContent {
	state: GlobalWindowState.AccountsList;
	special: false;
	account: TootDeckAccount;
	accounts: Array<ParsedAccount>;
	title: string;
}

export interface GlobalWindowFilters extends GlobalWindowContent {
	state: GlobalWindowState.Filters;
	special: false;
	category: FilterState;
}

export interface GlobalWindowChangelog extends GlobalWindowContent {
	state: GlobalWindowState.Changelog;
	special: false;
}

/**
 * Stores
 */
export let stGlobalWindow: Writable<GlobalWindowAllowed> = writable(null);

/**
 * Functions
 */
class GlobalWindowInner {
	static readonly instance = new GlobalWindowInner();

	private history: Array<GlobalWindowAllowed> = [];

	private set(content: GlobalWindowAllowed) {
		stGlobalWindow.set(content);
	}

	private destroyColumn(win: GlobalWindowAllowed | undefined) {
		if (!win) {
			return;
		}

		(win as GlobalWindowFullMediaWithThread)?.column?.destroy();
	}

	create(content: GlobalWindowAllowed) {
		this.history.push(content);
		this.set(content);
	}

	remove() {
		this.destroyColumn(this.history.pop());
		this.set(this.history.at(-1) ?? null);
	}

	clear() {
		this.history.forEach((win) => this.destroyColumn(win));
		this.history = [];
		this.set(null);
	}
}

export const GlobalWindow = GlobalWindowInner.instance;
