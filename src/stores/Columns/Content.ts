import { writable, type Writable } from 'svelte/store';

import { randomInt } from '../../lib/Utils/js/randomInt';

export let stContentUpdateTime: Writable<number> = writable(1);

setInterval(
	() => {
		stContentUpdateTime.update((v) => ++v);
	},
	randomInt(5, 20) * 1000
);
