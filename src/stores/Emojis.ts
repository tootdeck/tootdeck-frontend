import { writable } from 'svelte/store';

import { NotificationModalType, notificationModal } from './NotificationsModal';
import { loadingUpdateState } from './loading';

import type { MirrorAPI } from '../lib/APIs/MirrorAPI/MirrorAPI';
import { capitalize } from '../lib/Utils/string/capitalize';
import { EmojiPicker } from '../lib/Emoji/EmojiPicker';

import type { CustomEmoji, CustomEmojiCatergory } from '../types/emoji';
import type { Entity } from '../types/mastodonEntities';

/**
 * Emoji
 */
type DOMAIN = string;

export let stEmojis = writable(new Map<DOMAIN, EmojiPicker>());

/**
 * Functions
 */
function createCustomEmojis(emojis: Array<Entity.Emoji>) {
	const categories: Array<CustomEmojiCatergory> = [];

	for (const emoji of emojis) {
		if (emoji.visible_in_picker === false) {
			continue;
		}

		if (!emoji.category) {
			emoji['category'] = 'Custom';
		}

		const transformed_emoji: CustomEmoji = {
			id: emoji.shortcode,
			name: emoji.shortcode,
			keywords: [':' + emoji.shortcode + ':'],
			skins: [{ src: emoji.url, static_src: emoji.static_url }],
		};
		const name = capitalize(emoji.category);

		const found = categories.find((x) => x.name === name);
		if (found) {
			found.emojis.push(transformed_emoji);
		} else {
			categories.push({
				id: emoji.category.toLowerCase(),
				name: name,
				emojis: [transformed_emoji],
			});
		}
	}

	return categories;
}

export async function getEmojis(mirror: MirrorAPI) {
	const domain = mirror.domain;

	loadingUpdateState('Getting instance emojis for ' + domain);

	const response = await mirror.Instance.Emojis.get();
	if (!response) {
		notificationModal.create(
			NotificationModalType.Warn,
			`Failed to fetch custom emojis for ${domain}.`,
			true
		);
	}

	const categories = response ? createCustomEmojis(response) : [];

	const picker_instance = new EmojiPicker(domain, categories);
	await picker_instance.instance.await();

	stEmojis.update((v) => {
		v.set(domain, picker_instance);
		return v;
	});
}
