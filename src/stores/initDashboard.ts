import { writable } from 'svelte/store';

import { stEmojis } from './Emojis';
import {
	refreshInstanceToken,
	stAccountDefault,
	stAccountLogin,
	stAccounts,
	stAccountsSameInstance,
	stMe,
	type TootDeckAccount,
	type TootDeckAccountOptions,
	type EntityRelationship,
} from './Account/Accounts';
import { NotificationModalType, notificationModal } from './NotificationsModal';
import { init as initOptions, stOptionsReadOnly } from './Options';
import { initializedState, loadingUpdateState } from './loading';

import { MirrorAPI } from '../lib/APIs/MirrorAPI/MirrorAPI';
import type { AuthGetResponse } from '../lib/APIs/TootDeckAPI/Methods/Auth/types';
import { errorModal } from '../lib/errorModal';
import { EmojiPicker } from '../lib/Emoji/EmojiPicker';
import { TootDeckAPI } from '../lib/APIs/TootDeckAPI/TootDeckAPI';
import { CacheStorage } from '../lib/Cache/CacheStorage';
import { AccountsMananger } from '../lib/Managers/AccountsManager';
import { TootDeckConfig, type ConfigResponse } from '../lib/TootDeckConfig';

import type { Entity } from '../types/mastodonEntities';

function createMirror(me: AuthGetResponse) {
	return new MirrorAPI({ username: me.main.username, domain: me.main.domain }, me.main.type);
}

export async function getEmojis(mirror: MirrorAPI) {
	const domain = mirror.domain;

	loadingUpdateState('Getting instance emojis for ' + domain);

	const picker_instance = new EmojiPicker(domain);
	await picker_instance.instance.await();

	stEmojis.update((v) => {
		v.set(domain, picker_instance);
		return v;
	});
}

async function getInstanceInfos(mirror: MirrorAPI) {
	// Get instance infos
	const instance_entity: {
		domain: string;
		data: Entity.Instance | null;
	} = await mirror.Instance.get().then((r) => ({
		domain: mirror.domain,
		data: r,
	}));

	if (!instance_entity || !instance_entity.data) {
		errorModal(`Unable to get instance informations`, undefined, true);
		return null;
	}

	const instances = new Map<String, Entity.Instance>();
	const instance = instance_entity.data;

	instance.support_emoji_reactions = false;

	instances.set(instance_entity.domain, instance);

	// Needed for Redis tabs
	await getEmojis(mirror);

	return instances;
}

async function getAccountInfos(mirror: MirrorAPI, instances: Map<String, Entity.Instance>) {
	const accounts = new Map<string, TootDeckAccount>();

	const handle = mirror.handle_string;

	const entity: Entity.Account | null = await mirror.Accounts.verifyCredentials();
	if (!entity) {
		// Refresh instance token
		notificationModal.create(
			NotificationModalType.Info,
			'Click to refresh your session for ' + handle,
			false,
			() => refreshInstanceToken(handle)
		);
		return null;
	}

	const domain = mirror.domain;
	const account_id = mirror.handle_string;
	const default_account = account_id;
	const options: TootDeckAccountOptions = {
		publishConfirmationStep: false,
		defaultAccount: false,
		moderator: false,
	};
	const account: TootDeckAccount = {
		mirrorAPI: mirror,
		// @ts-ignore
		entity: entity,
		instance: instances.get(domain)!,
		relationship: new CacheStorage<EntityRelationship>('relationships_' + handle, {
			threshold: 10,
			wait: 30 * 1000,
		}),
		options: writable(options),
	};

	account.entity = AccountsMananger.store(account, entity);

	accounts.set(account_id, account);
	stAccountLogin.set(account);

	if (!accounts.size) {
		return null;
	}

	return { accounts, default_account, same_instance: false };
}

export async function initAccount() {
	/**
	 * Verify login state
	 */
	loadingUpdateState('Login in');
	const me = await TootDeckAPI.Auth.me();
	if (!me) {
		return false;
	}

	/**
	 * Create MirrorAPI instance
	 */
	const mirror = createMirror(me);

	/**
	 * Get instances infos
	 */
	loadingUpdateState('Getting instances info');
	const instances = await getInstanceInfos(mirror);
	if (!instances) {
		return false;
	}

	/**
	 * Get accounts infos
	 */
	loadingUpdateState('Getting accounts infos');
	const accounts_infos = await getAccountInfos(mirror, instances);
	if (!accounts_infos) {
		return false;
	}

	/**
	 * Set stores
	 */
	stAccounts.set(accounts_infos.accounts);
	stAccountDefault.set(accounts_infos.default_account);
	stAccountLogin.set(Array.from(accounts_infos.accounts.values())[0]);
	stAccountsSameInstance.set(accounts_infos.same_instance);
	stMe.set([me.main, ...me.secondary]);

	return true;
}

async function initConfig() {
	const config = (await TootDeckConfig.import()) as ConfigResponse;

	initOptions(config);
	// initSoundsLibrary(config);

	return true;
}

export async function initStores() {
	stOptionsReadOnly.set(true);

	const account = await initAccount();
	if (!account) {
		errorModal('Unable to initialize accounts', undefined, true);
		return false;
	}

	const config = await initConfig();
	if (!config) {
		errorModal('Unable to initialize app with user configuration', undefined, true);
		return false;
	}

	initializedState(true);

	return true;
}
