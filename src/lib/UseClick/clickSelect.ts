import type { ActionReturn } from 'svelte/action';

// TODO
type Attributes = any;

function findClassInStatus(target: HTMLElement | null, class_name: string) {
	if (!target || target.classList.contains('status')) {
		return false;
	}

	if (target.classList.contains(class_name)) {
		return true;
	}

	return findClassInStatus(target.parentElement, class_name);
}

function skipClick(e: MouseEvent): boolean {
	const target = e.target as HTMLElement;

	return (
		// Anchor
		target.tagName === 'A' ||
		target.parentElement?.tagName === 'A' ||
		target.parentElement?.parentElement?.tagName === 'A' ||
		// content warning
		findClassInStatus(target, 'content-warning') ||
		// expand status btn
		findClassInStatus(target, 'show') ||
		// Not status text
		!findClassInStatus(target, 'text')
	);
}

/**
 * Allow selecting text without triggering click
 */
export function onClickSelect(
	node: HTMLElement,
	args: { enable: boolean; cb: (e: MouseEvent) => void }
): ActionReturn<Attributes> {
	if (!args.enable) {
		return {};
	}

	let click_x: number = 0;
	let click_y: number = 0;

	function clickStart(e: MouseEvent) {
		click_x = e.clientX;
		click_y = e.clientY;
	}

	function clickEnd(e: MouseEvent) {
		if (e.button !== 0 || skipClick(e)) {
			return;
		}

		let x = Math.abs(click_x - e.clientX);
		let y = Math.abs(click_y - e.clientY);

		if (x < 10 && y < 10) {
			args.cb(e);
		}
	}

	node.addEventListener('mousedown', clickStart, { passive: true });
	node.addEventListener('mouseup', clickEnd, { passive: true });

	return {
		destroy() {
			node.removeEventListener('mousedown', clickStart);
			node.removeEventListener('mouseup', clickEnd);
		},
	};
}
