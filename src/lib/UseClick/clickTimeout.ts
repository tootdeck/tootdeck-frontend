import type { ActionReturn } from 'svelte/action';

import { calcSpeedFactor } from '../Transitions/calcSpeedFactor';

interface Attributes extends HTMLButtonElement {
	'on:clickTimeout': (e: CustomEvent) => void;
}

export function onClickTimeout(node: HTMLElement, time: number = 500): ActionReturn<Attributes> {
	let lock: boolean = false;

	const handleClick = (event: MouseEvent) => {
		if (!node.contains(event.target as HTMLElement) || lock) {
			return;
		}

		lock = true;
		node.blur();
		node.dispatchEvent(new CustomEvent('clickTimeout'));

		setTimeout(() => {
			lock = false;
		}, time);
	};

	document.addEventListener('click', handleClick, { passive: true });

	return {
		destroy() {
			document.removeEventListener('click', handleClick);
		},
	};
}

export function onClickTimeoutTransition(
	node: HTMLElement,
	time: number = 500
): ActionReturn<Attributes> {
	let lock: boolean = false;

	const handleClick = (event: MouseEvent) => {
		if (!node.contains(event.target as HTMLElement) || lock) {
			return;
		}

		lock = true;
		node.blur();
		node.dispatchEvent(new CustomEvent('clickTimeout'));

		setTimeout(() => {
			lock = false;
		}, calcSpeedFactor(time));
	};

	document.addEventListener('click', handleClick, { passive: true });

	return {
		destroy() {
			document.removeEventListener('click', handleClick);
		},
	};
}
