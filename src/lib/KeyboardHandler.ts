import { filterMutate } from './Utils/filter/filterMutate';

type Key = string;
type Subscription = string;
/**
 * @param event `KeyboardEvent`
 * @returns `true` to stop processing of other subscriptions
 */
type Callback = (event: KeyboardEvent) => boolean;
type Entry = { name: Subscription; callback: Callback };

class KeyboardHandlerInner {
	static readonly instance = new KeyboardHandlerInner();

	private readonly subscriptions = new Map<Key, Array<Entry>>();

	constructor() {
		window.addEventListener('keydown', (event) => {
			this.update(event);
		});
	}

	private update(event: KeyboardEvent) {
		const key_subscriptions = this.subscriptions.get(event.code);
		if (!key_subscriptions) {
			return;
		}

		const len = key_subscriptions.length;
		for (let i = len - 1; i >= 0; i--) {
			const entry = key_subscriptions[i];
			if (entry.callback(event)) {
				return;
			}
		}
	}

	subscribe(name: Subscription, key: Key, callback: Callback) {
		const entry: Entry = { name, callback };

		let key_subscription = this.subscriptions.get(key);
		if (key_subscription) {
			key_subscription.push(entry);
			return;
		}

		this.subscriptions.set(key, [entry]);
	}

	unsubscribe(name: Subscription, key: Key) {
		let key_subscriptions = this.subscriptions.get(key);
		if (!key_subscriptions) {
			return;
		}

		filterMutate(key_subscriptions, (subscription) => subscription.name !== name);
	}
}

export const KeyboardHandler = KeyboardHandlerInner.instance;
