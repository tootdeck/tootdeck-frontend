import { get } from 'svelte/store';
import PersistentStorage from 'persistent-storage';

import { stAccountLogin } from '../stores/Account/Accounts';
import { stSessionsUpdate } from '../stores/Sessions';

import type { OpenSessionResponse } from './APIs/TootDeckAPI/Methods/Auth/types';
import { TootDeckAPI } from './APIs/TootDeckAPI/TootDeckAPI';
import { errorModal } from './errorModal';

class SessionsInner {
	static readonly instance = new SessionsInner();

	private readonly config_key: string = 'sessions_';
	private readonly storage = new PersistentStorage({
		keyPrefix: this.config_key,
		useCompression: true,
		useCache: true,
		storageBackend: window.localStorage,
	});
	private _sessions: Array<OpenSessionResponse> = [];
	private _diff: Array<OpenSessionResponse> = [];

	private stringToDate(s: OpenSessionResponse) {
		s.created_at = new Date(s.created_at);
		s.updated_at = new Date(s.updated_at);

		return s;
	}

	async init(save: boolean): Promise<boolean> {
		const login = get(stAccountLogin).mirrorAPI.handle_string;

		const local = this.getLocal(login);
		const remote = await this.get();

		this._diff = this.diff(local, remote);
		this._sessions = remote;

		if (save) {
			this.saveLocal(login, remote);
		}

		// Refreshes window if opened
		stSessionsUpdate.update((v) => ++v);

		return this._diff.length !== 0;
	}

	getDisplayable() {
		return {
			sessions: this._sessions,
			diff: this._diff,
		};
	}

	async get(): Promise<Array<OpenSessionResponse>> {
		const response = await TootDeckAPI.Sessions.get();

		const sessions = response?.map(this.stringToDate) ?? [];

		return sessions;
	}

	async revoke(session_uuid: string): Promise<boolean> {
		const response = await TootDeckAPI.Sessions.revoke(session_uuid);
		if (!response) {
			errorModal(`Failed to revoke session ${session_uuid}`, undefined, true);
			return false;
		}

		const found = this._sessions.find((x) => x.uuid === session_uuid);
		if (found) {
			found.revoked = true;
		}

		this._diff = this._diff.filter((x) => x.uuid !== session_uuid);

		return true;
	}

	async delete(session_uuid: string): Promise<boolean> {
		const response = await TootDeckAPI.Sessions.delete(session_uuid);
		if (!response) {
			errorModal(`Failed to delete session ${session_uuid}`, undefined, true);
			return false;
		}

		this._sessions = this._sessions.filter((x) => x.uuid !== session_uuid);

		return true;
	}

	diff(
		local_sessions: Array<OpenSessionResponse>,
		remote_sessions: Array<OpenSessionResponse>
	): Array<OpenSessionResponse> {
		const remote_no_current = remote_sessions.filter((x) => !x.current);
		const local = local_sessions?.map(this.stringToDate) ?? [];

		if (!remote_no_current.length) {
			return [];
		}

		const diff: Array<OpenSessionResponse> = [];
		for (const session of remote_no_current) {
			if (!local.find((x) => x.created_at.valueOf() === session.created_at.valueOf())) {
				diff.push(session);
			}
		}

		if (!diff.length) {
			return [];
		}

		return diff;
	}

	getLocal(login_handle: string): Array<OpenSessionResponse> {
		return this.storage.getItem(login_handle) ?? [];
	}

	saveLocal(login_handle: string, sessions: Array<OpenSessionResponse>) {
		this.storage.setItem(login_handle, sessions);
	}
}

export const Sessions = SessionsInner.instance;
