export const VERSION: string = import.meta.env.VITE_VERSION ?? 'dev';
/**
 * A limited amount of features have a different behavior or are disabled in dev environment,
 *
 * You can change this value to restore them.
 */
export const BYPASS_DEV_MODE: boolean = false;
