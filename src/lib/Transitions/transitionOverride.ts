import {
	blur,
	fade,
	fly,
	slide,
	type BlurParams,
	type FadeParams,
	type FlyParams,
	type ScaleParams,
	type SlideParams,
	type TransitionConfig,
} from 'svelte/transition';
import { cubicOut } from 'svelte/easing';

import { calcSpeedFactor } from './calcSpeedFactor';
import { flyNoFade } from './flyNoFade';

function splitCCSUnit(value: string | number): [number, string] {
	const split = typeof value === 'string' && value.match(/^\s*(-?[\d.]+)([^\s]*)\s*$/);
	return split ? [parseFloat(split[1]), split[2] || 'px'] : [value as number, 'px'];
}

function transitionOverride(
	node: Element,
	fn: typeof blur | typeof fade | typeof fly | typeof slide | typeof scale,
	args?: BlurParams | FadeParams | FlyParams | SlideParams | ScaleParams
): TransitionConfig {
	// 500 is the default svelte animations duration
	let duration = Math.floor(calcSpeedFactor(args?.duration ?? 500));

	// @ts-ignore
	return fn(node, { ...args, duration });
}

function scaleBlur(
	node: Element,
	{
		delay = 0,
		duration = 400,
		easing = cubicOut,
		start = 0,
		opacity = 0,
		amount = 5,
		origin = 'top center',
	}: CustomScaleParams & BlurParams
) {
	const style = getComputedStyle(node);
	const target_opacity = +style.opacity;
	const transform = style.transform === 'none' ? '' : style.transform;
	const sd = 1 - start;
	const od = target_opacity * (1 - opacity);
	const f = style.filter === 'none' ? '' : style.filter;
	const [value, unit] = splitCCSUnit(amount);
	return {
		delay,
		duration,
		easing,
		css: (_t: number, u: number) => `
			transform-origin: ${origin};
			transform: ${transform} scale(${1 - sd * u});
			opacity: ${target_opacity - od * u};
			filter: ${f} blur(${u * value}${unit});
		`,
	};
}

function scale(
	node: Element,
	{
		delay = 0,
		duration = 400,
		easing = cubicOut,
		start = 0,
		opacity = 0,
		origin = 'top center',
	}: CustomScaleParams
) {
	const style = getComputedStyle(node);
	const target_opacity = +style.opacity;
	const transform = style.transform === 'none' ? '' : style.transform;
	const sd = 1 - start;
	const od = target_opacity * (1 - opacity);
	return {
		delay,
		duration,
		easing,
		css: (_t: number, u: number) => `
			transform-origin: ${origin};
			transform: ${transform} scale(${1 - sd * u});
			opacity: ${target_opacity - od * u};
		`,
	};
}

// prettier-ignore
export const transitionBlur = (node: Element, args?: BlurParams) => transitionOverride(node, blur, args);
// prettier-ignore
export const transitionFade = (node: Element, args?: FadeParams) => transitionOverride(node, fade, args);
// prettier-ignore
export const transitionFly = (node: Element, args?: FlyParams) => transitionOverride(node, fly, args);
// prettier-ignore
export const transitionFlyNoFade = (node: Element, args?: FlyParams) => transitionOverride(node, flyNoFade, args);
// prettier-ignore
export const transitionSlide = (node: Element, args?: SlideParams) => transitionOverride(node, slide, args);
// prettier-ignore
export const transitionScale = (node: Element, args?: CustomScaleParams) => transitionOverride(node, scale, args);
// prettier-ignore
export const transitionScaleBlur = (node: Element, args?: CustomScaleParams & BlurParams) => transitionOverride(node, scaleBlur, args);

export interface CustomScaleParams extends ScaleParams {
	/**
	 * `transform-origin` css property
	 *
	 * https://developer.mozilla.org/en-US/docs/Web/CSS/transform-origin
	 */
	origin?: string;
}

export type CustomTransiton =
	| typeof transitionBlur
	| typeof transitionFade
	| typeof transitionFly
	| typeof transitionFlyNoFade
	| typeof transitionSlide
	| typeof transitionScale
	| typeof transitionScaleBlur;
export type CustomTransitonArgs =
	| BlurParams
	| FadeParams
	| FlyParams
	| FlyParams
	| SlideParams
	| ScaleParams
	| (ScaleParams & BlurParams);
