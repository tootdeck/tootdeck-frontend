import { flip } from 'svelte/animate';
import type { AnimationConfig, FlipParams } from 'svelte/animate';

import { calcSpeedFactor } from './calcSpeedFactor';

function animateOverride(
	node: Element,
	fn: typeof flip,
	{
		from,
		to,
	}: {
		from: DOMRect;
		to: DOMRect;
	},
	args?: FlipParams
): AnimationConfig {
	// 120 is the default svelte animations duration
	// @ts-ignore
	let duration = Math.floor(calcSpeedFactor(args?.duration || 120));

	return fn(
		node,
		{
			from,
			to,
		},
		{ ...args, duration }
	);
}

export const animateFlip = (
	node: Element,
	{
		from,
		to,
	}: {
		from: DOMRect;
		to: DOMRect;
	},
	args?: FlipParams
) => animateOverride(node, flip, { from, to }, args);
