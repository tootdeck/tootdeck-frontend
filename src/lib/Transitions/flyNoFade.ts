import { cubicOut } from 'svelte/easing';
import type { FlyParams, TransitionConfig } from 'svelte/transition';

function split_css_unit(value: number | string): [number, string] {
	const split = typeof value === 'string' && value.match(/^\s*(-?[\d.]+)([^\s]*)\s*$/);
	return split ? [parseFloat(split[1]), split[2] || 'px'] : [value as number, 'px'];
}

export function flyNoFade(
	node: Element,
	{ delay = 0, duration = 400, easing = cubicOut, x = 0, y = 0 }: FlyParams = {}
): TransitionConfig {
	const style = getComputedStyle(node);
	const transform = style.transform === 'none' ? '' : style.transform;

	const [xValue, xUnit] = split_css_unit(x);
	const [yValue, yUnit] = split_css_unit(y);

	if (!duration) {
		duration = 1;
	}

	return {
		delay,
		duration,
		easing,
		css: (t, _) => `
			transform: ${transform} translate(${(1 - t) * xValue}${xUnit}, ${(1 - t) * yValue}${yUnit});`,
	};
}
