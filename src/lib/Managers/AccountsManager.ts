import { Account, stRefreshAccount, type TootDeckAccount } from '../../stores/Account/Accounts';

import { accountParser } from '../ContentParser/ContentParser';
import { updateObject } from '../Utils/js/updateObject';
import { stringifyHandle } from '../Utils/handle/stringifyHandle';
import { getHandleFromAccount } from '../Utils/handle/getHandleFromAccount';

import type { InstanceHandle } from '../../types/handle';
import type { ParsedAccount } from '../../types/contentParsed';
import type { Entity } from '../../types/mastodonEntities';

type ID = string;
type Handle = string;

interface DomainMap {
	ids: Map<ID, ParsedAccount>;
	handle: Map<Handle, ParsedAccount>;
}

class AccountsManangerInner {
	static readonly instance = new AccountsManangerInner();

	private readonly domains = new Map<string, DomainMap>();

	/**
	 * Add
	 */
	// #region

	private async addByID(account: TootDeckAccount, id: string): Promise<ParsedAccount | null> {
		const response = await account.mirrorAPI.Accounts.get(id);
		if (!response) {
			return null;
		}

		return this.store(account, response);
	}

	private async addByHandle(
		account: TootDeckAccount,
		handle: string
	): Promise<ParsedAccount | null> {
		const response = await account.mirrorAPI.Search.accounts(handle, {
			limit: 1,
			resolve: true,
		});
		if (!response || !response.length) {
			return null;
		}

		return this.store(account, response[0]);
	}

	// #endregion

	/**
	 * Store
	 */
	private findByID(domain: string, id: string): ParsedAccount | undefined {
		return this.domains.get(domain)?.ids.get(id);
	}

	private findByHandle(domain: string, handle: string): ParsedAccount | undefined {
		return this.domains.get(domain)?.handle.get(handle);
	}

	store(
		account: TootDeckAccount,
		incoming_account: Entity.Account | ParsedAccount
	): ParsedAccount {
		const parsed = accountParser(account, incoming_account);

		const domain = account.mirrorAPI.domain;
		const handle = getHandleFromAccount(parsed, true);

		const inner = this.domains.get(domain);
		if (inner) {
			let entity = inner.handle.get(handle);
			if (entity) {
				const visual_diff =
					parsed.avatar !== entity.avatar ||
					parsed.tootdeck.plain_content !== entity.tootdeck.plain_content;

				const { tootdeck, ...extracted } = parsed;
				updateObject(entity, extracted);

				if (visual_diff) {
					// Update profile in columns
					entity.tootdeck.update++;

					// Update profile in sidebar and composer
					if (Account.get(handle, false)) {
						stRefreshAccount.update((v) => ++v);
					}
				}

				return entity;
			}

			inner.ids.set(parsed.id, parsed);
			inner.handle.set(handle, parsed);
		} else {
			const domains_map: DomainMap = {
				ids: new Map<string, ParsedAccount>(),
				handle: new Map<string, ParsedAccount>(),
			};

			domains_map.ids.set(parsed.id, parsed);
			domains_map.handle.set(handle, parsed);

			this.domains.set(domain, domains_map);
		}

		return parsed;
	}

	/**
	 * Get
	 */
	// #region

	async getByID(account: TootDeckAccount, id: string): Promise<ParsedAccount | null> {
		const entity = this.findByID(account.mirrorAPI.domain, id);
		if (!entity) {
			return await this.addByID(account, id);
		}

		return entity;
	}

	getByIDSync(account: TootDeckAccount, id: string): ParsedAccount | null {
		const entity = this.findByID(account.mirrorAPI.domain, id);
		if (!entity) {
			return null;
		}

		return entity;
	}

	async getByHandle(account: TootDeckAccount, handle: string): Promise<ParsedAccount | null>;
	async getByHandle(
		account: TootDeckAccount,
		handle: InstanceHandle
	): Promise<ParsedAccount | null>;
	async getByHandle(
		account: TootDeckAccount,
		handle: InstanceHandle | string
	): Promise<ParsedAccount | null> {
		let stringified: string;
		if (typeof handle === 'string') {
			stringified = handle;
		} else {
			stringified = stringifyHandle(handle);
		}
		const entity = this.findByHandle(account.mirrorAPI.domain, stringified);
		if (!entity) {
			return await this.addByHandle(account, stringified);
		}

		return entity;
	}

	getAll(domain: string): Array<ParsedAccount> {
		const cache = this.domains.get(domain)?.handle;
		if (!cache) {
			return [];
		}

		return Array.from(cache.values());
	}

	// #endregion
}

export const AccountsMananger = AccountsManangerInner.instance;
