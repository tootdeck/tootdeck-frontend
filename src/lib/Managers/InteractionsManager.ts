import { ContentManager } from './ContentManager';
import { debugLogger } from '../debugLogger';

import type { InstanceHandle } from '../../types/handle';
import type { RemoteInteractions } from '../../types/remoteInteractions';

type Domain = string;
type ID = string;

class InteractionsManagerInnner {
	static readonly instance = new InteractionsManagerInnner();

	private readonly interactions_cache = new Map<Domain, Map<ID, RemoteInteractions>>();

	store(handle: InstanceHandle, interactions: RemoteInteractions): void {
		const domain_map = this.interactions_cache.get(handle.domain);
		if (!domain_map) {
			const inner = new Map();
			inner.set(interactions.id, interactions);
			this.interactions_cache.set(handle.domain, inner);

			ContentManager.Status.updateInteractions(handle, interactions);

			debugLogger(
				`InteractionsManager: store: Created new domain map and stored ${handle.domain}:${interactions.id}`
			);

			return;
		}

		domain_map.set(interactions.id, interactions);

		ContentManager.Status.updateInteractions(handle, interactions);

		debugLogger(`InteractionsManager: store: Stored ${handle.domain}:${interactions.id}`);
	}

	get(domain: string, id: string): RemoteInteractions | undefined {
		const domain_map = this.interactions_cache.get(domain);
		if (!domain_map) {
			return;
		}

		const result = domain_map.get(id);

		debugLogger(`InteractionsManager: get: ${result ? 'Found' : 'Not found'} ${domain}:${id}`);

		return result;
	}

	delete(domain: string, id: string): boolean {
		const domain_map = this.interactions_cache.get(domain);
		if (!domain_map) {
			return false;
		}

		const result = domain_map.delete(id);

		debugLogger(
			`InteractionsManager: remove: ${result ? 'Removed' : 'Not found'} ${domain}:${id}`
		);

		return result;
	}
}

export const InteractionsManager = InteractionsManagerInnner.instance;
