import { debugLogger } from '../debugLogger';

interface RequestsCache {
	promise: Promise<any>;
	expiry: Date;
	timeout: NodeJS.Timeout;
}

class RequestsManagerInnner {
	static readonly instance = new RequestsManagerInnner();

	private readonly requests_cache = new Map<string, RequestsCache>();

	private createEntry(
		request_id: string,
		promise: Promise<any>,
		cache_time: number
	): RequestsCache {
		return {
			promise,
			expiry: new Date(new Date().valueOf() + cache_time),
			timeout: setTimeout(() => {
				debugLogger('CachedRequestsManager: Clear -> ' + request_id);
				this.requests_cache.delete(request_id);
			}, cache_time),
		};
	}

	increaseExpiry(request_id: string, cache_time: number) {
		const cache = this.requests_cache.get(request_id);
		if (!cache) {
			return;
		}

		clearTimeout(cache.timeout);
		const expiry = cache.expiry.valueOf() + cache_time;

		this.requests_cache.set(request_id, this.createEntry(request_id, cache.promise, expiry));
	}

	get<T>(request_id: string): Promise<T> | undefined {
		const cache = this.requests_cache.get(request_id);
		if (!cache) {
			return;
		}

		return cache.promise;
	}

	await<T>(request_id: string, request: () => Promise<T>, cache_time: number = 5000): Promise<T> {
		const cache = this.requests_cache.get(request_id);
		if (cache) {
			debugLogger('CachedRequestsManager: Getting from cache -> ' + request_id);
			return cache.promise;
		}

		const promise = request();

		debugLogger('CachedRequestsManager: Set -> ' + request_id);
		this.requests_cache.set(request_id, this.createEntry(request_id, promise, cache_time));

		return promise;
	}

	cancel(request_id: string) {
		this.requests_cache.delete(request_id);
	}
}

export const RequestsManager = RequestsManagerInnner.instance;
