import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { ThreadColumn } from '../Columns/ThreadColumn';
import { StandardColumn } from '../Columns/StandardColumn';
import { StandardNotificationsColumn } from '../Columns/StandardNotificationColumn';
import { EventsManager } from '../Events/EventsManager';
import { TootDeckEvents } from '../Events/TootDeckEvents';
import type { PayloadEvent } from '../Events/types';
import { statusParser } from '../ContentParser/ContentParser';
import { interactionsReplacer } from '../ContentParser/interactionsReplacer';
import { AccountsMananger } from './AccountsManager';
import { QuoteManager } from './QuoteManager';
import { InteractionsManager } from './InteractionsManager';
import { errorModal } from '../errorModal';
import { UpdatePoll } from '../updatePolls';
import { debugLogger } from '../debugLogger';
import { updateObject } from '../Utils/js/updateObject';
import { filterDuplicate } from '../Utils/filter/filterDuplicates';
import { stringifyHandle } from '../Utils/handle/stringifyHandle';

import type {
	LinkedToStatus,
	ParsedAccountNotification,
	ParsedAccountStatus,
	ParsedNotification,
	ParsedStatus,
} from '../../types/contentParsed';
import type { InstanceHandle } from '../../types/handle';
import type { RemoteInteractions } from '../../types/remoteInteractions';
import { Entity } from '../../types/mastodonEntities';

enum MapType {
	Status = 1,
	Notification = 2,
}

type Parsed = ParsedStatus | ParsedNotification;
type Domain = string; // Account domain
type HandleID = string; // [key]@[handle]
type ID = string; // Status id

interface ReferenceCountedStatusDomain {
	key: HandleID;
	value: ReferenceCountedStatus;
}

interface ReferenceCountedStatus {
	used_in: Array<StandardColumn<Parsed>>;
	status: ParsedStatus;
}

interface ReferenceCountedNotification {
	used_in: Array<StandardNotificationsColumn>;
	notification: ParsedNotification;
}

class ContentManagerInner {
	static readonly instance = new ContentManagerInner();

	private readonly domains = new Map<Domain, Map<ID, Array<ReferenceCountedStatusDomain>>>();
	private readonly statuses = new Map<ID, ReferenceCountedStatus>();
	private readonly notifications = new Map<ID, ReferenceCountedNotification>();

	constructor() {
		EventsManager.subscribe('column_content_manager_mutation', (source, payload) => {
			switch (source) {
				case TootDeckEvents.Message:
					return;
				case TootDeckEvents.Notification:
					this.parseAccountNotifications(payload);
					this.notificationPipeline(payload);
					return;
				default:
					this.parseAccountStatuses(payload);
					this.statusPipeline(payload);
					return;
			}
		});

		EventsManager.subscribeAfter('column_content_manager_after', (source, payload) => {
			switch (source) {
				case TootDeckEvents.Message:
					return;
				case TootDeckEvents.Notification:
					this.garbageCollect(MapType.Notification, payload);
					return;
				default:
					this.garbageCollect(MapType.Status, payload);
					return;
			}
		});
	}

	/**
	 * Parse account
	 */
	// #region

	private parseAccountNotification(account: TootDeckAccount, n: Entity.Notification) {
		const notification = n as any as ParsedAccountNotification;

		notification.account = AccountsMananger.store(account, notification.account);

		if (notification.status) {
			notification.status.account = AccountsMananger.store(
				account,
				notification.status.account
			);
		}
	}

	private parseAccountNotifications(payload: PayloadEvent<Entity.Notification>) {
		if (payload.data.length === 1) {
			this.parseAccountNotification(payload.account, payload.data[0]);
			return;
		}

		const accounts = payload.data
			.map((n) => [n.account, n.status?.account])
			.flat()
			.filter((x) => x) as Array<Entity.Account>;
		const filtered = filterDuplicate(accounts, 'id');

		filtered.forEach((a) => {
			const stored_account = AccountsMananger.store(payload.account, a);

			payload.data.forEach((n) => {
				const notification = n as any as ParsedAccountNotification;

				if (notification.account.id === stored_account.id) {
					notification.account = stored_account;
				}

				if (notification.status?.account.id === stored_account.id) {
					notification.status.account = stored_account;
				}
			});
		});
	}

	private parseAccountStatus(account: TootDeckAccount, s: Entity.Status) {
		const status = s as any as ParsedAccountStatus;

		status.account = AccountsMananger.store(account, status.account);

		if (status.reblog) {
			status.reblog.account = AccountsMananger.store(account, status.reblog.account);
		}
	}

	private parseAccountStatuses(payload: PayloadEvent<Entity.Status>) {
		if (payload.data.length === 1) {
			this.parseAccountStatus(payload.account, payload.data[0]);
			return;
		}

		const accounts = payload.data
			.map((s) => [s.account, s.reblog?.account])
			.flat()
			.filter((x) => x) as Array<Entity.Account>;
		const filtered = filterDuplicate(accounts, 'id');

		filtered.forEach((a) => {
			const stored_account = AccountsMananger.store(payload.account, a);

			payload.data.forEach((s) => {
				const status = s as any as ParsedAccountStatus;

				if (status.account.id === stored_account.id) {
					status.account = stored_account;
				}

				if (status.reblog?.account.id === stored_account.id) {
					status.reblog.account = stored_account;
				}
			});
		});
	}

	// #endregion

	/**
	 * Utils
	 */
	// #region

	private getLinkedColumnsForStatuses(
		domain: string,
		links: Array<LinkedToStatus> | undefined
	): Array<StandardColumn<Parsed> | undefined> {
		if (!links) {
			return [];
		}

		return links
			.map((link) => {
				const values = this.getDomainEntities(domain, link.id)?.entities?.map(
					(rc) => rc.value
				);

				values?.forEach((entity) => entity.status.tootdeck.update++);

				return values?.map((entity) => entity.used_in).flat();
			})
			.flat();
	}

	private getStatusUsedIn(
		domain: string,
		entity: ReferenceCountedStatus | ReferenceCountedNotification
	) {
		let status: ParsedStatus | null;
		if ((entity as ReferenceCountedStatus).status) {
			status = (entity as ReferenceCountedStatus).status;
		} else {
			status = (entity as ReferenceCountedNotification).notification.status;
		}

		const rc_statuses = this.getLinkedColumnsForStatuses(domain, status?.tootdeck.in_status);

		const used_in = rc_statuses.filter((x) => x).flat() as Array<StandardColumn<Parsed>>;

		return filterDuplicate([...entity.used_in, ...used_in], 'id');
	}

	private getStatusesUsedIn(
		domain: string,
		entities: Array<ReferenceCountedStatus | ReferenceCountedNotification>
	) {
		return entities.map((entity) => this.getStatusUsedIn(domain, entity)).flat();
	}

	// #endregion

	/**
	 * Pipeline
	 */
	//#region

	private notificationPipeline(payload: PayloadEvent<ParsedAccountNotification>) {
		// Feed cache
		const payload_entities = payload.data.map((notification) =>
			this.fillNotifications(payload.account, notification)
		);

		// Destructurate cache response
		const payload_data = payload_entities.map((x) => x.notification);

		const data_used_in_other = payload_data
			.map((x) => {
				if (!x.status) {
					return;
				}

				const entity = this.getInMapByID(
					MapType.Status,
					this.getID(payload.account, x.status.id)
				)!;

				return entity.used_in;
			})
			.filter((x) => x)
			.flat() as Array<StandardColumn<Parsed>>;

		// Remove Columns instances duplicates
		const used_in = filterDuplicate(data_used_in_other, 'id');

		// Update Columns content
		used_in.forEach((column) => column.updateContent());

		// Send cached content to the Event queue
		(payload.data as any) = payload_data;
	}

	private statusPipeline(payload: PayloadEvent<ParsedAccountStatus>) {
		// Feed cache
		const payload_entities = payload.data.map((status) =>
			this.fillStatuses(payload.account, status)
		);

		// Destructurate cache response
		const used_in = this.getStatusesUsedIn(payload.account.mirrorAPI.domain, payload_entities);
		const payload_data = payload_entities.map((x) => x.status);

		// Update Columns content
		used_in.forEach((column) => column.updateContent());

		// Send cached content to the Event queue
		(payload.data as any) = payload_data;
	}

	private fillNotifications(
		account: TootDeckAccount,
		notification: ParsedAccountNotification
	): ReferenceCountedNotification {
		const id = this.getID(account, notification.id);

		const entity = this.notifications.get(id);
		if (entity) {
			return entity;
		} else {
			let parsed_status: ParsedStatus | null = null;
			if (notification.status) {
				parsed_status = this.fillStatuses(account, notification.status).status;
				parsed_status.tootdeck.in_notification_id.push(notification.id);
			}

			const parsed_notification: ParsedNotification = {
				...notification,
				accounts: [notification.account],
				notify: (notification as any as ParsedNotification)?.notify ?? false,
				status: parsed_status,
				type: notification.type,
				uuid: window.crypto.randomUUID(),
			};

			const create_entity = { used_in: [], notification: parsed_notification };
			this.notifications.set(id, create_entity);

			return create_entity;
		}
	}

	private fillStatuses(
		account: TootDeckAccount,
		status: ParsedAccountStatus
	): ReferenceCountedStatus {
		const id = this.getID(account, status.id);

		const entity = this.statuses.get(id);
		if (entity) {
			this.updateHandleReblog(account, entity, status);

			return entity;
		} else {
			const parsed_status = statusParser(account, status);

			if (status.reblog) {
				parsed_status.reblog = this.fillStatuses(account, status.reblog).status;
				const linked_to: LinkedToStatus = {
					id: parsed_status.id,
					quote: false,
				};
				parsed_status.reblog.tootdeck.in_status.push(linked_to);
			}

			const create_entity: ReferenceCountedStatus = { used_in: [], status: parsed_status };
			this.statuses.set(id, create_entity);

			this.fillDomains(account.mirrorAPI.domain, { key: id, value: create_entity });

			return create_entity;
		}
	}

	//#endregion

	/**
	 * Domains
	 */
	//#region

	private fillDomains(domain: string, entity: ReferenceCountedStatusDomain) {
		const domains_map = this.domains.get(domain);

		const id = entity.value.status.id;

		if (domains_map) {
			const domain_entities = domains_map.get(id);
			if (domain_entities) {
				domain_entities.push(entity);
			} else {
				domains_map.set(id, [entity]);
			}
		} else {
			this.domains.set(domain, new Map([[id, [entity]]]));
		}
	}

	private getDomainEntities(domain: string, id: string) {
		const domain_map = this.domains.get(domain);

		if (!domain_map) {
			debugLogger('ContentManager: getDomainEntities: No map for ' + domain);
			return {};
		}

		// All statuses related to this id
		const domain_entities = domain_map.get(id);
		if (!domain_entities) {
			debugLogger(
				'ContentManager: getDomainEntities: No entities for ' + id + ' in map ' + domain,
				domain_map
			);
			return {};
		}

		return {
			entities: domain_entities,
			domain_map,
		};
	}

	private removeAllFromDomain(domain: string, id: string) {
		const { entities, domain_map } = this.getDomainEntities(domain, id);
		if (!entities && !domain_map) {
			return;
		}

		const columns: Array<StandardColumn<Parsed>> = [];

		for (const entry of entities) {
			const value = entry.value;
			const status = value.status;

			// For each content used in column
			value.used_in.forEach((column) => {
				// If content not already deleted from that column
				if (!columns.find((c) => Object.is(c, column))) {
					QuoteManager.delete(column, status.id);
					InteractionsManager.delete(domain, status.id);

					this.deleteLinked(MapType.Status, column.account, status.tootdeck.in_status);
					this.deleteLinked(
						MapType.Notification,
						column.account,
						status.tootdeck.in_notification_id
					);

					columns.push(column);
				}
			});
			this.delete(MapType.Status, entry.key);

			debugLogger('ContentManager: removeAllFromDomain: Removed content ' + entry.key);
		}

		columns.forEach((column) => column.removeContent(id));

		domain_map.delete(id);
		debugLogger('ContentManager: removeAllFromDomain: Removed entries ' + id);

		if (domain_map.size === 0) {
			this.domains.delete(domain);
			debugLogger('ContentManager: removeAllFromDomain: ' + domain + ' completely removed');
		} else {
			debugLogger(
				'ContentManager: removeAllFromDomain: ' +
					domain +
					', reference ' +
					domain_map.size +
					' content'
			);
		}
	}

	private removeFromDomain(domain: string, entry_key: string) {
		const id = entry_key.split('@')[0];

		// domain => Domain string
		// entities => All statuses related to this id
		// domain_map => Domain map
		const { entities, domain_map } = this.getDomainEntities(domain, id);
		if (!entities && !domain_map) {
			return;
		}

		const new_entities = entities.filter((x) => x.key !== entry_key);

		// No more statuses with this id
		if (new_entities.length === 0) {
			domain_map.delete(id);
			debugLogger(
				'ContentManager: removeFromDomain: ' + entry_key + ' completely removed, ' + id
			);
		}
		// Update array with remaning statuses with this id
		else {
			domain_map.set(id, new_entities);
			debugLogger(
				'ContentManager: removeFromDomain: ' +
					entry_key +
					' unreferenced, ref in ' +
					new_entities.length
			);
		}

		// No more entry for this domain
		if (domain_map.size === 0) {
			this.domains.delete(domain);
			debugLogger('ContentManager: removeFromDomain: ' + domain + ' completely removed');
		} else {
			debugLogger(
				'ContentManager: removeFromDomain: ' +
					domain +
					', reference ' +
					domain_map.size +
					' content'
			);
		}
	}

	//#endregion

	/**
	 * Utils
	 */
	//#region

	private getID(handle: InstanceHandle, id: string): string;
	private getID(handle: string, id: string): string;
	private getID(instance: StandardColumn<Parsed>, id: string): string;
	private getID(account: TootDeckAccount, id: string): string;
	private getID(instance_or_account: any, id: string): string {
		if (
			instance_or_account instanceof StandardColumn ||
			instance_or_account instanceof ThreadColumn
		) {
			return id + '@' + instance_or_account.account.mirrorAPI.handle_string;
		} else if (instance_or_account?.mirrorAPI) {
			return id + '@' + instance_or_account.mirrorAPI.handle_string;
		} else if (instance_or_account?.username && instance_or_account?.domain) {
			return id + '@' + instance_or_account?.username + '@' + instance_or_account?.domain;
		}
		if (typeof instance_or_account === 'string') {
			return id + '@' + instance_or_account;
		} else {
			errorModal(
				'Fatal ERROR',
				[
					"ContentManager: first argument doesn't have an account property.",
					instance_or_account,
				],
				true
			);
			throw new Error();
		}
	}

	private getInMapByID(
		map_type: MapType.Notification,
		id: string
	): ReferenceCountedNotification | undefined;
	private getInMapByID(map_type: MapType.Status, id: string): ReferenceCountedStatus | undefined;
	private getInMapByID(
		map_type: MapType,
		id: string
	): ReferenceCountedStatus | ReferenceCountedNotification | undefined;
	private getInMapByID(
		map_type: MapType,
		id: string
	): ReferenceCountedStatus | ReferenceCountedNotification | undefined {
		switch (map_type) {
			case MapType.Status:
				return this.statuses.get(id);
			case MapType.Notification:
				return this.notifications.get(id);
			default:
				errorModal(
					'Fatal ERROR',
					["ContentManager: first argument isn't in MapType enum.", map_type],
					true
				);
				throw new Error();
		}
	}

	//#endregion

	/**
	 * Owner
	 */
	//#region

	addOwner(map_type: MapType, that: StandardColumn<Parsed>, content_id: string) {
		const id = this.getID(that, content_id);

		const entity = this.getInMapByID(map_type, id);
		if (entity && !entity.used_in.find((x) => Object.is(x, that))) {
			// @ts-ignore
			entity.used_in.push(that);

			debugLogger(
				'ContentManager: ' + content_id + ' referenced, ref in ' + entity.used_in.length
			);
		}
	}

	removeOwner(map_type: MapType, that: StandardColumn<Parsed>, content_id: string) {
		const id = this.getID(that, content_id);

		const entity = this.getInMapByID(map_type, id);
		if (!entity) {
			return;
		}

		if (map_type === MapType.Status) {
			const status = (entity as ReferenceCountedStatus).status;
			const reblog = status.reblog;

			if (reblog) {
				this.removeOwner(map_type, that, reblog.id);
				debugLogger('ContentManager: ' + content_id + ' removed reblog ownership');
			}
		} else if (map_type === MapType.Notification) {
			const status = (entity as ReferenceCountedNotification).notification.status;
			if (status) {
				status.tootdeck.in_notification_id = status.tootdeck.in_notification_id.filter(
					(x) => x !== (entity as ReferenceCountedNotification).notification.id
				);
				this.removeOwner(map_type, that, status.id);
				debugLogger('ContentManager: ' + content_id + ' removed status ownership');
			}
		}

		entity.used_in = entity.used_in.filter((x) => !Object.is(x, that));

		if (entity.used_in.length === 0) {
			if (map_type === MapType.Status) {
				UpdatePoll.remove(content_id);
				this.statuses.delete(id);
			} else if (map_type === MapType.Notification) {
				this.notifications.delete(id);
			}

			this.removeFromDomain(that.account.mirrorAPI.domain, id);

			debugLogger('ContentManager: ' + content_id + ' completely removed');

			if (!this.domains.size && !this.statuses.size && !this.notifications.size) {
				debugLogger('ContentManager: NO LEAKS');
				return;
			}

			return;
		}

		debugLogger(
			'ContentManager: ' + content_id + ' unreferenced, ref in ' + entity.used_in.length
		);
	}

	//#endregion

	get(
		map_type: MapType.Notification,
		account: TootDeckAccount,
		content_id: string
	): ParsedNotification | null;
	get(
		map_type: MapType.Status,
		account: TootDeckAccount,
		content_id: string
	): ParsedStatus | null;
	get(
		map_type: MapType,
		account: TootDeckAccount,
		content_id: string
	): ParsedStatus | ParsedNotification | null {
		const entity = this.getInMapByID(map_type, this.getID(account, content_id));
		if (entity) {
			switch (map_type) {
				case MapType.Status:
					return (entity as ReferenceCountedStatus).status;
				case MapType.Notification:
					return (entity as ReferenceCountedNotification).notification;
			}
		}

		return null;
	}

	find(handle: InstanceHandle, url: string): ParsedStatus | undefined {
		/**
		 * domains
		 * Map<Account domain, Map<[key]@[handle], ReferenceCountedStatus>>
		 */
		const domain_map = this.domains.get(handle.domain);
		if (!domain_map) {
			return;
		}

		/**
		 * domain_map
		 * Map
		 * Key: Status id
		 * value: Map<[key]@[handle], Array<ReferenceCountedStatus>>
		 */
		for (const domain_id of domain_map.values()) {
			/**
			 * domain_id
			 * Array
			 * key: [key]@[handle]
			 * value: ReferenceCountedStatus
			 */
			for (const account_status of domain_id) {
				const status = account_status.value.status;

				// Check both possible URLs
				if ([status.url, status.uri].includes(url)) {
					const visibility = status.visibility;

					// check if account already have this status
					if (!account_status.key.includes(stringifyHandle(handle))) {
						// check if others accounts have this status
						continue;
					}

					if (visibility === Entity.Visibility.Direct) {
						return;
					}

					return status;
				}
			}
		}
	}

	/**
	 * Update
	 */
	//#region

	private updateHandleReblog(
		account: TootDeckAccount,
		entity: ReferenceCountedStatus,
		incoming_status: ParsedAccountStatus
	) {
		if (entity.status.reblog) {
			const { reblog, ...excluded } = incoming_status;

			const { tootdeck, ...parsed } = statusParser(account, incoming_status.reblog!);
			updateObject(entity.status.reblog, parsed);

			entity.status.reblog.tootdeck.quote_urls = tootdeck.quote_urls;
			entity.status.reblog.tootdeck.embed = tootdeck.embed;
			entity.status.reblog.tootdeck.update++;

			// Update raw status
			updateObject(entity.status, excluded);

			entity.status.tootdeck.update++;
		} else {
			const { tootdeck, ...parsed } = statusParser(account, incoming_status);

			updateObject(entity.status, parsed);

			entity.status.tootdeck.quote_urls = tootdeck.quote_urls;
			entity.status.tootdeck.embed = tootdeck.embed;
			entity.status.tootdeck.update++;
		}
	}

	private updateHandleNotification(
		account: TootDeckAccount,
		entity: ReferenceCountedNotification,
		incoming_notification: ParsedAccountNotification
	) {
		if (entity.notification.status) {
			const { status, ...patched } = incoming_notification;

			const { tootdeck, ...parsed } = statusParser(account, incoming_notification.status!);
			updateObject(entity.notification.status, parsed);

			entity.notification.status.tootdeck.quote_urls = tootdeck.quote_urls;
			entity.notification.status.tootdeck.embed = tootdeck.embed;
			entity.notification.status.tootdeck.update++;

			updateObject(entity.notification, patched);
		} else {
			updateObject(entity.notification, incoming_notification);
		}
	}

	update(
		map_type: MapType.Notification,
		account: TootDeckAccount,
		incoming_notification: Entity.Notification | ParsedNotification
	): void;
	update(
		map_type: MapType.Status,
		account: TootDeckAccount,
		incoming_status: Entity.Status | ParsedStatus
	): void;
	update(map_type: MapType, account: TootDeckAccount, incoming_content: any): void {
		const id = this.getID(account, incoming_content.id);

		const entity = this.getInMapByID(map_type, id);
		if (entity) {
			if (map_type === MapType.Status) {
				this.updateHandleReblog(
					account,
					entity as ReferenceCountedStatus,
					incoming_content
				);
			} else if (map_type === MapType.Notification) {
				this.updateHandleNotification(
					account,
					entity as ReferenceCountedNotification,
					incoming_content
				);
			}

			const used_id = this.getStatusUsedIn(account.mirrorAPI.domain, entity);
			used_id.forEach((column) => column.updateContent());
		}
	}

	updateInteractions(handle: InstanceHandle, interactions: RemoteInteractions) {
		const id = this.getID(handle, interactions.id);

		const entity = this.getInMapByID(MapType.Status, id);
		if (entity) {
			entity.status.tootdeck.update++;

			interactionsReplacer(entity.status, interactions);

			const used_id = this.getStatusUsedIn(handle.domain, entity);
			used_id.forEach((column) => column.updateContent());
		}
	}

	forceUpdate(handle: InstanceHandle, status_id: string) {
		const id = this.getID(handle, status_id);

		const entity = this.getInMapByID(MapType.Status, id);
		if (entity) {
			entity.status.tootdeck.update++;

			const used_id = this.getStatusUsedIn(handle.domain, entity);
			used_id.forEach((column) => column.updateContent());
		}
	}

	//#endregion

	store(
		map_type: MapType.Notification,
		that: StandardColumn<ParsedNotification>,
		incoming_notification: Entity.Notification
	): ParsedNotification;
	store(
		map_type: MapType.Status,
		that: StandardColumn<ParsedStatus>,
		incoming_status: Entity.Status
	): ParsedStatus;
	store(map_type: MapType, that: StandardColumn<Parsed>, incoming_content: any): any {
		const account = that.account;

		let parsed: ParsedStatus | ParsedNotification;

		switch (map_type) {
			case MapType.Status:
				this.parseAccountStatus(account, incoming_content);
				parsed = this.fillStatuses(account, incoming_content).status;
				if (parsed.reblog) {
					this.addOwner(map_type, that, parsed.reblog.id);
				}
				break;
			case MapType.Notification:
				this.parseAccountNotification(account, incoming_content);
				parsed = this.fillNotifications(account, incoming_content).notification;
				if (parsed.status) {
					this.addOwner(map_type, that, parsed.status.id);
				}
				break;
			default:
				errorModal(
					'Fatal ERROR',
					["ContentManager: first argument isn't in MapType enum.", map_type],
					true
				);
				throw new Error();
		}

		this.addOwner(map_type, that, parsed.id);

		debugLogger('ContentManager: ' + parsed.id + ' stored');

		return parsed;
	}

	/**
	 * Delete
	 */
	//#region

	private garbageCollect(
		map_type: MapType,
		incoming_content: PayloadEvent<Entity.Notification> | PayloadEvent<Entity.Status>
	): void {
		let removed: number = 0;

		if (!incoming_content.data.length) {
			return;
		}

		incoming_content.data.forEach((content) => {
			const id = this.getID(incoming_content.account, content.id);
			const entity = this.getInMapByID(map_type, id);

			if (entity && !entity.used_in.length) {
				this.forceRemove(map_type, incoming_content.account.mirrorAPI.handle, id);
				removed++;
			}
		});

		debugLogger(`ContentManager: garbageCollect: Removed ${removed} unused content`);
	}

	private delete(map_type: MapType, map_id: string) {
		switch (map_type) {
			case MapType.Status:
				this.statuses.delete(map_id);
				break;
			case MapType.Notification:
				this.notifications.delete(map_id);
				break;
			default:
				errorModal(
					'Fatal ERROR',
					["ContentManager: first argument isn't in MapType enum.", map_type],
					true
				);
				throw new Error();
		}
	}

	private deleteLinked(
		map_type: MapType,
		account: TootDeckAccount,
		links: Array<string> | Array<LinkedToStatus>
	) {
		for (const linked_to of links) {
			if ((linked_to as LinkedToStatus)?.quote) {
				continue;
			}
			const link = linked_to as string;
			const id = this.getID(account, link);

			const linked_entity = this.getInMapByID(map_type, id);
			if (linked_entity) {
				linked_entity.used_in.forEach((column) => column.removeContent(link));

				this.delete(map_type, link);

				debugLogger(
					`ContentManager: Deleted linked ${
						map_type === MapType.Status ? 'status' : 'notification'
					} ${link}`
				);
			}
		}

		links.splice(0, links.length);
	}

	removeLinked(account: TootDeckAccount, content_id: string) {
		const id = this.getID(account, content_id);
		const entity = this.statuses.get(id);
		if (entity) {
			this.deleteLinked(MapType.Status, account, entity.status.tootdeck.in_status);
			this.deleteLinked(
				MapType.Notification,
				account,
				entity.status.tootdeck.in_notification_id
			);
		}
	}

	forceRemove(map_type: MapType, handle: InstanceHandle, content_id: string) {
		if (map_type === MapType.Status) {
			this.removeAllFromDomain(handle.domain, content_id);
		} else if (map_type === MapType.Notification) {
			const id = this.getID(handle, content_id);

			const entity = this.notifications.get(id);
			if (entity) {
				const status = entity.notification.status;
				if (status) {
					entity.used_in.forEach((column) => {
						this.removeOwner(MapType.Status, column, status.id);
					});
				}
				this.delete(map_type, id);

				entity.used_in.forEach((column) => {
					column.removeContent(content_id);
				});
			}
		}
	}

	//#endregion
}

export class ContentManager {
	static readonly Status = class {
		/**
		 * Owner
		 */
		//#region

		static addOwner(that: StandardColumn<any>, status_id: string): void {
			ContentManagerInner.instance.addOwner(MapType.Status, that, status_id);
		}

		static removeOwner(that: StandardColumn<any>, status_id: string): void {
			ContentManagerInner.instance.removeOwner(MapType.Status, that, status_id);
		}

		//#endregion

		static get(account: TootDeckAccount, status_id: string): ParsedStatus | null {
			return ContentManagerInner.instance.get(MapType.Status, account, status_id);
		}

		static find(handle: InstanceHandle, url: string): ParsedStatus | undefined {
			return ContentManagerInner.instance.find(handle, url);
		}

		/**
		 * Update
		 */
		//#region

		static update(account: TootDeckAccount, status: Entity.Status | ParsedStatus): void {
			ContentManagerInner.instance.update(MapType.Status, account, status);
		}

		static updateInteractions(handle: InstanceHandle, interactions: RemoteInteractions): void {
			ContentManagerInner.instance.updateInteractions(handle, interactions);
		}

		static forceUpdate(handle: InstanceHandle, id: string): void {
			ContentManagerInner.instance.forceUpdate(handle, id);
		}

		//#endregion

		static store(that: StandardColumn<any>, status: Entity.Status): ParsedStatus {
			return ContentManagerInner.instance.store(MapType.Status, that, status);
		}

		/**
		 * Delete
		 */
		//#region

		static removeLinked(account: TootDeckAccount, status_id: string): void {
			ContentManagerInner.instance.removeLinked(account, status_id);
		}

		static forceRemove(handle: InstanceHandle, status_id: string): void {
			ContentManagerInner.instance.forceRemove(MapType.Status, handle, status_id);
		}

		//#endregion
	};

	static readonly Notification = class {
		/**
		 * Owner
		 */
		//#region

		static addOwner(that: StandardColumn<ParsedNotification>, notification_id: string): void {
			ContentManagerInner.instance.addOwner(MapType.Notification, that, notification_id);
		}

		static removeOwner(
			that: StandardColumn<ParsedNotification>,
			notification_id: string
		): void {
			ContentManagerInner.instance.removeOwner(MapType.Notification, that, notification_id);
		}

		//#endregion

		static get(account: TootDeckAccount, notification_id: string): ParsedNotification | null {
			return ContentManagerInner.instance.get(MapType.Notification, account, notification_id);
		}

		/**
		 * Update
		 */
		//#region

		static update(
			account: TootDeckAccount,
			notification: Entity.Notification | ParsedNotification
		): void {
			ContentManagerInner.instance.update(MapType.Notification, account, notification);
		}

		//#endregion

		static store(
			that: StandardColumn<ParsedNotification>,
			notification: Entity.Notification
		): ParsedNotification {
			return ContentManagerInner.instance.store(MapType.Notification, that, notification);
		}

		/**
		 * Delete
		 */
		//#region

		static forceRemove(handle: InstanceHandle, notification_id: string): void {
			ContentManagerInner.instance.forceRemove(MapType.Notification, handle, notification_id);
		}

		//#endregion
	};
}
