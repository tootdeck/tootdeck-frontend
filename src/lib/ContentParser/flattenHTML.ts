import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { isolateEmojis, type TextChunk } from './emojisParser';
import { getAction, type ParsedAction } from './anchorsParser';
import { trimMentions } from './trimMentions';
import { nativeEmojisReplacer } from '../Emoji/nativeEmojisReplacer';
import { randomInt } from '../Utils/js/randomInt';
import { parseHandle } from '../Utils/handle/parseHandle';
import { truncateLink } from '../Utils/truncateLink';
import { ANCHOR_ARGS } from '../Constants/Anchor';

import type { Entity } from '../../types/mastodonEntities';

export interface Flags {
	bigger: boolean;
	big: boolean;
	bold: boolean;
	italic: boolean;
	code: boolean;
	pre: boolean;
	underline: boolean;
	strikethrough: boolean;
	del: boolean;
	ins: boolean;
	li: boolean;
}

export interface Attributes {
	action?: ParsedAction;
	alt?: string;
	class?: string;
	href?: string;
	src?: string;
	style?: string;
}

export interface ParsedNode {
	id: number;
	tag: 'text' | 'a' | 'img' | 'ul' | 'ol' | 'blockquote';
	text: Array<TextChunk> | null;
	li?: Array<{ id: number; nodes: Array<ParsedNode> }>;
	blockquote?: Array<{ id: number; nodes: Array<ParsedNode> }>;
	attributes?: Attributes;
}

let id: number = 0;

export function formatFlags(flags: Flags): string {
	const ret = Object.entries(flags)
		.filter(([key, value]) => value)
		.map(([key, value]) => key);

	return ret.join(' ');
}

function parseText(
	child: ChildNode,
	flat: Array<ParsedNode>,
	emoji_data_set: Array<Entity.Emoji>,
	flags: Flags
) {
	let text = child.nodeValue;
	if (!text) {
		return;
	}

	flat.push({
		id: id++,
		tag: 'text',
		text: isolateEmojis(text, emoji_data_set),
		attributes: {
			class: formatFlags(flags),
		},
	});
}

function parseAnchor(
	child: ChildNode,
	flat: Array<ParsedNode>,
	emoji_data_set: Array<Entity.Emoji>,
	flags: Flags
) {
	if (child.childNodes.length === 0) {
		return;
	}

	// Standard A
	const anchor_text = child.childNodes[0].nodeValue;
	// A with a nested child
	const nested_anchor_text = Array.from(child.childNodes[0].childNodes ?? [])
		.map((c) => c.nodeValue)
		.join(' ');
	if (!anchor_text && !nested_anchor_text) {
		// empty anchor
		return;
	}

	const href = anchor_text ?? nested_anchor_text;

	// Truncate links to 30 char
	const { trunc_anchor_text, truncated } = truncateLink(href, 30);

	flat.push({
		id: id++,
		tag: 'a',
		text: trunc_anchor_text ? isolateEmojis(trunc_anchor_text, emoji_data_set) : null,
		attributes: {
			class: [
				formatFlags(flags),
				(child as Element).classList.contains('mention') ? 'mention' : '',
				truncated ? 'ellipsis' : '',
			].join(' '),
			href: (child as Element).getAttribute('href')!,
		},
	});
}

function parseIMG(child: ChildNode, flat: Array<ParsedNode>) {
	const cast = child as Element;
	flat.push({
		id: id++,
		tag: 'img',
		text: null,
		attributes: {
			alt: cast.getAttribute('alt') ?? undefined,
			class: cast.getAttribute('class') ?? undefined,
			src: cast.getAttribute('src') ?? undefined,
			style: cast.getAttribute('style') ?? undefined,
		},
	});
}

function parseList(
	child: ChildNode,
	flat: Array<ParsedNode>,
	emoji_data_set: Array<Entity.Emoji>,
	tag: 'ul' | 'ol'
) {
	const cast = child as Element;

	const li = Array.from(cast.childNodes).map((child) => {
		const children = Array.from(child.childNodes);

		const nodes: Array<ParsedNode> = [];
		children.forEach((child) => {
			recurse(child, nodes, emoji_data_set);
		});

		return { id: id++, nodes };
	});

	flat.push({
		id: id++,
		tag,
		text: null,
		li,
	});
}

function parseBlockquote(
	child: ChildNode,
	flat: Array<ParsedNode>,
	emoji_data_set: Array<Entity.Emoji>
) {
	const cast = child as Element;

	const blockquote = Array.from(cast.childNodes).map((child) => {
		const children = [child, ...Array.from(child.childNodes)];

		const nodes: Array<ParsedNode> = [];
		children.forEach((child) => {
			recurse(child, nodes, emoji_data_set);
		});

		// Trim end
		const trimed = nodes.map((node) => {
			const text = node.text;
			if (text) {
				const last_text = text.at(-1);
				if (last_text?.is_raw) {
					last_text.content = (last_text.content as string).trimEnd();
				}
			}

			return node;
		});

		return { id: id++, nodes: trimed };
	});

	flat.push({
		id: id++,
		tag: 'blockquote',
		text: null,
		blockquote,
	});
}

function recurse(
	child: ChildNode,
	flat: Array<ParsedNode>,
	emoji_data_set: Array<Entity.Emoji>,
	flags: Flags = {
		bigger: false,
		big: false,
		bold: false,
		italic: false,
		code: false,
		pre: false,
		underline: false,
		strikethrough: false,
		del: false,
		ins: false,
		li: false,
	}
): void {
	switch (child.nodeName) {
		case '#text':
			parseText(child, flat, emoji_data_set, flags);
			return;
		case 'A':
			parseAnchor(child, flat, emoji_data_set, flags);
			return;
		case 'IMG':
			parseIMG(child, flat);
			return;
		case 'UL':
			parseList(child, flat, emoji_data_set, 'ul');
			return;
		case 'OL':
			parseList(child, flat, emoji_data_set, 'ol');
			return;
		case 'LI':
			return;
		case 'BLOCKQUOTE':
			parseBlockquote(child, flat, emoji_data_set);
			return;

		case 'B':
		case 'STRONG':
			flags.bold = true;
			break;
		case 'EM':
		case 'I':
			flags.italic = true;
			break;
		case 'CODE':
			flags.code = true;
			break;
		case 'PRE':
			flags.pre = true;
			break;
		case 'U':
			flags.underline = true;
			break;
		case 'S':
			flags.strikethrough = true;
			break;
		case 'DEL':
			flags.del = true;
			break;
		case 'INS':
			flags.ins = true;
			break;
		case 'H1':
			flags.bigger = true;
		case 'H2':
			flags.big = true;
		case 'H3':
		case 'H4':
			flags.bold = true;
			break;

		default:
			console.log(child.nodeName);
	}

	Array.from(child.childNodes).forEach((child) =>
		recurse(child, flat, emoji_data_set, structuredClone(flags))
	);
}

function parse(child: ChildNode, flat: Array<ParsedNode>, emoji_data_set: Array<Entity.Emoji>) {
	recurse(child, flat, emoji_data_set);
}

function fixTwitterTag(raw_html: string) {
	return raw_html.replaceAll(/(@[a-zA-Z0-9\_]+@(?:(?:twitter|x)\.com))/gm, (match) => {
		const handle = parseHandle(match);
		if (!handle) {
			return match;
		}

		const args = Object.entries(ANCHOR_ARGS)
			.map(([key, value]) => `${key}="${value}"`)
			.join(' ');
		return `<a href="https://${handle.domain}/${handle.username}" ${args}>${match}</a>`;
	});
}

function preparseHTML(raw_html: string, trim?: boolean): [[string, string], string] {
	// Remove all <p>, <span>, <br>, leading and trailing spaces
	// Replace escaped HTML entities (&lt; &gt;) with random values
	// because otherwise they will be executed by innerHTML
	const random_char: [string, string] = [
		btoa(randomInt(0, 999).toString()),
		btoa(randomInt(0, 999).toString()),
	];
	let preparse = raw_html.replace(
		/<p[^>]*>|<span[^>]*>|<\/span>|<\/p>|<br[^>]*>|&lt;|&gt;/g,
		(match) => {
			if (match.includes('span') || match.includes('<p')) {
				return '';
			}

			if (match.includes('br')) {
				return '\n';
			}

			switch (match) {
				case '</p>':
					return '\n\n';
				case '&lt;':
					return random_char[0];
				case '&gt;':
					return random_char[1];
				default:
					return match;
			}
		}
	);
	if (trim) {
		preparse = preparse.trim();
	}
	preparse = fixTwitterTag(preparse);

	return [random_char, preparse];
}

function parseHTML(
	raw_html: string,
	emoji_data_set: Array<Entity.Emoji>,
	optional?: { account?: TootDeckAccount; status?: Entity.Status; trim?: boolean }
) {
	const [random_char, preparse] = preparseHTML(raw_html, optional?.trim);

	// Create DOM nodes
	const div = document.createElement('div');
	div.innerHTML = preparse;

	// Replace emojis with Twemoji
	nativeEmojisReplacer(div);

	// Parse all nodes recursion
	const flat: Array<ParsedNode> = [];
	const children = Array.from(div.childNodes);
	children.forEach((child) => parse(child, flat, emoji_data_set));

	div.remove();

	// Replace random values with HTML entities
	flat.forEach((x) => {
		if (x.tag !== 'text' || !x.text) {
			return;
		}

		x.text.forEach((chunk) => {
			if (!chunk.is_raw) {
				return;
			}

			chunk.content = (chunk.content as string).replace(
				new RegExp(`${random_char[0]}|${random_char[1]}`, 'g'),
				(match) => {
					switch (match) {
						case random_char[0]:
							return '<';
						case random_char[1]:
							return '>';
						default:
							return match;
					}
				}
			);
		});
	});

	return flat;
}

export function emojisOnly(content: string, emoji_data_set: Array<Entity.Emoji>) {
	return parseHTML(content, emoji_data_set);
}

export function flattenHTML(
	raw_html: string,
	emoji_data_set: Array<Entity.Emoji>,
	optional?: { account?: TootDeckAccount; status?: Entity.Status; trim?: boolean }
): Array<ParsedNode> {
	const flat = parseHTML(raw_html, emoji_data_set, optional);

	// Parse anchors and prepare click callback
	if (optional && optional.account) {
		flat.forEach((x) => {
			if (x.tag === 'text') {
				return;
			}

			if (x.attributes && x.attributes.href) {
				x.attributes.action = getAction(optional.account!, x.attributes.href);
			}
		});
	}

	// Remove mentions
	if (optional && optional.account && optional.status) {
		trimMentions(flat, optional.account, optional.status);
	}

	return flat;
}

export function convertNativeEmoji(emoji: string): Array<ParsedNode> {
	// Create DOM nodes
	const div = document.createElement('div');
	div.innerHTML = emoji;

	// Replace emojis with Twemoji
	nativeEmojisReplacer(div);

	// Remove all nodes recursion
	const flat: Array<ParsedNode> = [];
	const children = Array.from(div.childNodes);
	children.forEach((child) => parse(child, flat, []));

	return flat;
}
