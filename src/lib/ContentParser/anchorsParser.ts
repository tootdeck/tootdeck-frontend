import type { TootDeckAccount } from '../../stores/Account/Accounts';
import { ExpandedPanel, ExpandedPanelState } from '../../stores/ExpandedPanel';

import { AccountsMananger } from '../Managers/AccountsManager';

const excluded_domain = ['twitter.com', 'x.com', 'instagram.com', 'facebook.com', 'youtube.com'];

export enum ActionEvent {
	Click = 'click',
	ClickQuote = 'clickQuote',
	TwitterEmbed = 'TwitterEmbed',
	YoutubeEmbed = 'YoutubeEmbed',
	DailymotionEmbed = 'DailymotionEmbed',
	Hashtag = 'hashtag',
}

export interface ParsedAction {
	event: ActionEvent;
	detail?: any;
	callback?: (e: MouseEvent) => void;
}

function getHandleFromURL(href: string) {
	const url = new URL(href);
	const path = url.pathname.split('/').filter((x) => x);

	/**
	 * https://masto.ai/users/AlexWinter
	 * masto.ai | users | AlexWinter
	 * masto.ai | AlexWinter
	 *
	 * https://masto.ai/@AlexWinter
	 * masto.ai | @AlexWinter
	 *
	 * https://masto.ai/@AlexWinter@masto.ai
	 * masto.ai | @AlexWinter@masto.ai
	 * @AlexWinter@masto.ai
	 *
	 * https://www.youtube.com/watch?v=D3jmQDEQ-fo
	 * www.youtube.com | watch | ?v=D3jmQDEQ-fo
	 */

	const domain = url.hostname;

	if (path.length === 1 && !path.find((x) => x.includes('@'))) {
		return;
	}

	if (
		path.length === 0 ||
		path.length > 2 ||
		!url.protocol.includes('http') ||
		url.search !== '' ||
		excluded_domain.includes(domain) ||
		domain.includes('tumblr.com')
	) {
		return;
	}

	if (['users', 'profile'].includes(path[0])) {
		path.shift();
	}

	if (path.length !== 1) {
		return;
	}

	const is_handle = path[0].split('@').filter((x) => x);

	if (is_handle.length === 2) {
		return is_handle.join('@');
	}

	const username = path[0].replace('@', '');

	return username + '@' + domain;
}

function getProfileCallback(account: TootDeckAccount, href: string) {
	const profile = getHandleFromURL(href);
	if (!profile) {
		return;
	}

	const status_account = () => AccountsMananger.getByHandle(account, profile);

	return async (e: MouseEvent) => {
		ExpandedPanel.update({
			state: ExpandedPanelState.Profile,
			account,
			status_account,
		});

		e.preventDefault();
	};
}

export function getStatusID(href: string): string | undefined {
	const url = new URL(href);
	const path = url.pathname.split('/').filter((x) => x);

	/**
	 * https://masto.ai/users/AlexWinter/statuses/111507726043523253
	 * AlexWinter/statuses/111507726043523253
	 * 111507726043523253
	 *
	 * https://masto.ai/@AlexWinter/111507726043523253
	 * 111507726043523253
	 *
	 * https://seafoam.space/notice/AcNgW2ttKZqSqXIe9o
	 * AcNgW2ttKZqSqXIe9o
	 *
	 * https://misskey.design/notes/9mqssbedqb
	 * 9mqssbedqb
	 *
	 * https://social.translunar.academy/objects/c1896ee4-2f3a-4cb0-81eb-944d58ffd43e
	 * objects/c1896ee4-2f3a-4cb0-81eb-944d58ffd43e
	 * c1896ee4-2f3a-4cb0-81eb-944d58ffd43e
	 *
	 * https://www.threads.net/@0xjessel/post/C9P0YQ1O8Gt
	 * post/C9P0YQ1O8Gt
	 * C9P0YQ1O8Gt
	 */

	const domain = url.hostname;

	if (
		path.length < 2 ||
		path.length > 4 ||
		!url.protocol.includes('http') ||
		url.search !== '' ||
		excluded_domain.includes(domain) ||
		domain.includes('tumblr.com')
	) {
		return undefined;
	}

	if (['users', 'notice', 'objects', 'notes'].includes(path[0]) || path[0].includes('@')) {
		path.shift();
	}

	if (path[0] === 'post') {
		path.shift();
	} else if (path[1] === 'statuses') {
		path.shift();
		path.shift();
	}

	if (path.length !== 1 || ['followers', 'following'].includes(path[0])) {
		return undefined;
	}

	return path[0];
}

export function getHashtag(href: string): string | undefined {
	const url = new URL(href);
	const path = url.pathname.split('/').filter((x) => x);

	/**
	 * https://mastodon.social/tags/pok%C3%A9mon
	 * pok%C3%A9mon
	 *
	 * https://nederland.gay/tag/digitalart
	 * digitalart
	 *
	 * https://misskey.io/tags/pixelart
	 * pixelart
	 *
	 */

	const domain = url.hostname;

	if (
		path.length !== 2 ||
		!url.protocol.includes('http') ||
		url.search !== '' ||
		excluded_domain.includes(domain) ||
		domain.includes('tumblr.com') ||
		!['tag', 'tags'].includes(path[0])
	) {
		return undefined;
	}

	return path[1];
}

export function isTwitterLink(href: string): boolean {
	if (!href) {
		return false;
	}

	const { hostname, pathname } = new URL(href);

	return (
		[
			'twitter.com',
			'vxtwitter.com',
			'fxtwitter.com',
			'twittpr.com',
			'x.com',
			'fixupx.com',
			'xcancel.com',
		].includes(hostname) && pathname.includes('status')
	);
}

export function isYoutubeLink(href: string): boolean {
	const url = new URL(href);
	if (url.hostname === 'youtu.be') {
		return true;
	}

	if (
		!url.hostname.includes('youtube.com') ||
		(!url.pathname.includes('watch') && !url.pathname.includes('shorts'))
	) {
		return false;
	}

	return true;
}

export function isDailymotionLink(href: string): boolean {
	const url = new URL(href);
	if (!url.hostname.includes('dailymotion.com') || !url.pathname.includes('video')) {
		return false;
	}

	return true;
}

export function getAction(account: TootDeckAccount, href: string): ParsedAction | undefined {
	const profile = getProfileCallback(account, href);
	if (profile) {
		return { event: ActionEvent.Click, callback: profile };
	}

	if (getStatusID(href)) {
		return { event: ActionEvent.ClickQuote, detail: { url: href } };
	}

	const hashtag = getHashtag(href);
	if (hashtag) {
		return { event: ActionEvent.Hashtag, detail: { url: href } };
	}

	if (isTwitterLink(href)) {
		return { event: ActionEvent.TwitterEmbed, detail: { url: href } };
	}

	if (isYoutubeLink(href)) {
		return { event: ActionEvent.YoutubeEmbed, detail: { url: href } };
	}

	if (isDailymotionLink(href)) {
		return { event: ActionEvent.DailymotionEmbed, detail: { url: href } };
	}
}
