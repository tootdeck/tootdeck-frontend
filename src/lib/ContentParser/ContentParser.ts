import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { getRawText, rawRemoveHandles } from './rawText';
import { emojisOnly, flattenHTML, type ParsedNode } from './flattenHTML';
import { InteractionsManager } from '../Managers/InteractionsManager';
import { interactionsReplacer } from './interactionsReplacer';
import { ActionEvent } from './anchorsParser';

import {
	type ParsedAccount,
	type ParsedAccountStatus,
	type ParsedStatus,
} from '../../types/contentParsed';
import type { Entity } from '../../types/mastodonEntities';
import { showMedia } from '../ComponentFunctions/showMedia';

function findActionEventURL(flat: Array<ParsedNode>, events: Array<ActionEvent>) {
	const found: Record<string, Array<string>> = {};

	for (const event of events) {
		found[event] = [];
	}

	for (const node of flat) {
		if (node.tag === 'a') {
			const action = node.attributes?.action;
			if (action && events.includes(action.event)) {
				found[action.event].push(action.detail.url);
			}
		}
	}

	return found;
}

/**
 * Trigger
 */
// #region

export function accountPipeline(
	account: TootDeckAccount,
	content: string,
	emojis: Array<Entity.Emoji>
): [string, Array<ParsedNode>] {
	const raw = getRawText(content);
	const flat = flattenHTML(content, emojis, { account, trim: true });

	return [raw, flat];
}

export function statusPipeline(
	account: TootDeckAccount,
	status: Entity.Status
): {
	parsed: Array<ParsedNode> | null;
	plain_text: string;
	quote_urls: Array<string>;
	twitter_embed: Array<string>;
	youtube_embed: Array<string>;
	dailymotion_embed: Array<string>;
} {
	const raw = getRawText(status.content);
	const raw_no_hanldes = rawRemoveHandles(raw);
	const no_hanldes = rawRemoveHandles(status.content);
	const empty = !raw.length;

	if (empty) {
		return {
			parsed: null,
			plain_text: raw_no_hanldes,
			quote_urls: [],
			twitter_embed: [],
			youtube_embed: [],
			dailymotion_embed: [],
		};
	}

	const flat = flattenHTML(no_hanldes, status.emojis, { account, status, trim: true });

	// prettier-ignore
	const {
		clickQuote,
		TwitterEmbed,
		YoutubeEmbed,
		DailymotionEmbed,
	} = findActionEventURL(flat, [
		ActionEvent.ClickQuote,
		ActionEvent.TwitterEmbed,
		ActionEvent.YoutubeEmbed,
		ActionEvent.DailymotionEmbed,
	]);

	return {
		parsed: flat,
		plain_text: raw_no_hanldes,
		quote_urls: clickQuote,
		twitter_embed: TwitterEmbed,
		youtube_embed: YoutubeEmbed,
		dailymotion_embed: DailymotionEmbed,
	};
}

export function contentPipeline(
	content: string,
	emojis: Array<Entity.Emoji>
): [string, Array<ParsedNode>] {
	const raw = getRawText(content);
	const flat = emojisOnly(content, emojis);
	return [raw, flat];
}

// #endregion

/**
 * Initiator
 */
// #region

export function accountParser(
	account: TootDeckAccount,
	incoming_entity: Entity.Account | ParsedAccount
): ParsedAccount {
	if (typeof incoming_entity.display_name !== 'string') {
		return incoming_entity as ParsedAccount;
	}

	const cast = incoming_entity as Entity.Account;

	const [plain_content, display_name] = contentPipeline(
		cast.display_name,
		incoming_entity.emojis
	);
	const [_2, note] = accountPipeline(account, cast.note, incoming_entity.emojis);
	const fields = cast.fields.map((field) => {
		const [_, value] = accountPipeline(account, field.value, incoming_entity.emojis);
		return {
			...field,
			value,
		};
	});

	const final = structuredClone(incoming_entity) as ParsedAccount;
	final.display_name = display_name;
	final.note = note;
	final.fields = fields;
	final.tootdeck = {
		plain_content,
		update: 1,
		uuid: window.crypto.randomUUID(),
	};

	return final;
}

export function statusParser(
	account: TootDeckAccount,
	incoming_entity: Entity.Status | ParsedAccountStatus | ParsedStatus
): ParsedStatus {
	if (typeof incoming_entity.content !== 'string') {
		return incoming_entity as ParsedStatus;
	}

	const { parsed, quote_urls, plain_text, twitter_embed, youtube_embed, dailymotion_embed } =
		statusPipeline(account, incoming_entity as Entity.Status);

	/**
	 * The account is possibly already a `ParsedAccount` so `structuredClone` won't work
	 * Setting account property to null
	 */
	const entity_account = incoming_entity.account;
	if (typeof entity_account.display_name !== 'string') {
		// @ts-expect-error
		incoming_entity.account = null;
	}

	/**
	 * The account in reblog is possibly already a `ParsedAccount` so `structuredClone` won't work
	 * Setting reblog property to null
	 */
	const entity_reblog = incoming_entity.reblog;
	incoming_entity.reblog = null;

	const final = structuredClone(incoming_entity) as ParsedStatus;
	final.account = entity_account as ParsedAccount; // Need to be parsed by `AccountsMananger` after `statusParser`
	final.content = parsed;
	final.plain_content = plain_text;
	final.emoji_reactions = final.emoji_reactions.map((reaction) => {
		if (reaction.uuid) {
			return reaction;
		}

		return {
			...reaction,
			uuid: window.crypto.randomUUID(),
		};
	});
	final.emoji_reactions.sort((a, b) => b.count - a.count);
	final.tootdeck = {
		height: 0,
		in_status: [],
		in_notification_id: [],
		show_cw: false,
		media_attachments_show: null,
		quote_urls,
		update: 1,
		uuid: window.crypto.randomUUID(),
		is_twitter_embed: false,
		embed: {
			twitter: twitter_embed,
			youtube: youtube_embed,
			dailymotion: dailymotion_embed,
		},
	};

	const interactions = InteractionsManager.get(account.mirrorAPI.domain, incoming_entity.id);
	if (interactions) {
		interactionsReplacer(final, interactions);
	}

	/**
	 * The account is possibly already a `ParsedAccount` so `structuredClone` won't work
	 * Restoring account property
	 */
	if (typeof entity_account.display_name !== 'string') {
		incoming_entity.account = entity_account;
	}

	/**
	 * The account in reblog is possibly already a `ParsedAccount` so `structuredClone` won't work
	 * Restoring reblog property
	 */
	incoming_entity.reblog = entity_reblog;

	return final;
}

// #endregion
