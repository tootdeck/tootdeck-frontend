import { rdOptionalFeaturesCacheStatusInteractions } from '../../stores/OptionalFeatures';

import { AppInit } from '../../AppInit';

import type { EmojiReaction, ParsedStatus } from '../../types/contentParsed';
import type { RemoteInteractions } from '../../types/remoteInteractions';
import type { Entity } from '../../types/mastodonEntities';

let cache_status_interactions: boolean = false;

AppInit.Promise.then(() => {
	rdOptionalFeaturesCacheStatusInteractions.subscribe((v) => (cache_status_interactions = v));
});

function skip(entity: Entity.Status | ParsedStatus) {
	if (!cache_status_interactions) {
		return true;
	}

	// Status not public, skipping it
	if (entity.visibility === 'direct' || entity.visibility === 'private') {
		// this.logger.verbose('getInteractions', 'Skipped (not public)');
		return;
	}

	// Status just created, skipping it
	const created_at = new Date(entity.created_at).valueOf();
	const now = new Date().valueOf();
	if (created_at + 40000 - now > 0) {
		// this.logger.verbose('getInteractions', 'Skipped (just created)');
		return;
	}

	return false;
}

function setBasicInteractions(
	entity: Entity.Status | ParsedStatus,
	interactions: RemoteInteractions
) {
	if (skip(entity)) {
		return;
	}

	if (interactions.favourites_count) {
		entity.favourites_count = interactions.favourites_count;
	}
	if (interactions.reblogs_count) {
		entity.reblogs_count = interactions.reblogs_count;
	}
	if (interactions.replies_count) {
		entity.replies_count = interactions.replies_count;
	}
}
function mergeReactions(
	stored_emoji_reactions: Array<EmojiReaction>,
	emoji_reactions: Array<EmojiReaction | Entity.Reaction>
) {
	const all: Array<EmojiReaction | Entity.Reaction> = [
		...stored_emoji_reactions,
		...emoji_reactions,
	];
	const merged: Array<EmojiReaction | Entity.Reaction> = [];
	while (all.length) {
		const current_index = all.length - 1;
		const reaction = all.splice(all.length - 1, 1)[0]!;

		const found_index = all.findIndex(
			(stored_reaction) => stored_reaction.name === reaction.name
		);
		if (found_index !== -1 && found_index !== current_index) {
			const found = all.splice(found_index, 1)[0]!;

			if (reaction.count < found.count) {
				reaction.count = found.count;
			}
		}

		merged.push(reaction);
	}

	return merged;
}

export function interactionsReplacer(
	entity: Entity.Status | ParsedStatus,
	interactions: RemoteInteractions
) {
	setBasicInteractions(entity, interactions);

	if (interactions.application) {
		entity.application = { name: interactions.application, vapid_key: null, website: null };
	}

	entity.emoji_reactions = mergeReactions(
		interactions.emoji_reactions,
		entity.emoji_reactions
	).map((reaction) => {
		const found = entity.emoji_reactions.find((original) => original.name === reaction.name);
		if (found) {
			reaction.me = found.me;
		}

		if ((reaction as EmojiReaction).uuid) {
			return reaction;
		}

		return {
			...reaction,
			uuid: window.crypto.randomUUID(),
		};
	}) as Array<EmojiReaction>;
	entity.emoji_reactions.sort((a, b) => b.count - a.count);
}
