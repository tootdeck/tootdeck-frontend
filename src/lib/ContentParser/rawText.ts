export function getRawText(raw_html: string): string {
	return raw_html
		.replace(/<[^>]+>/g, (match) => {
			switch (match) {
				case '</p>':
					return '\n\n';
				case '<br>':
				case '<br/>':
				case '<br />':
					return '\n';
			}

			return '';
		})
		.trim();
}

export function rawRemoveHandles(raw_html: string): string {
	const text_array = raw_html.split(' ');
	let remove_index = 0;

	for (const index in text_array) {
		if (text_array[index].charAt(0) !== '@') {
			remove_index = +index;
			break;
		}
	}

	text_array.splice(0, remove_index);

	return text_array.join(' ');
}
