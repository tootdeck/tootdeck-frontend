import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { AccountsMananger } from '../Managers/AccountsManager';
import { ContentManager } from '../Managers/ContentManager';
import { getHandleFromURL } from '../Utils/handle/getHandleFromURL';
import { statusParser } from '../ContentParser/ContentParser';

import type { ParsedAccount, ParsedStatus } from '../../types/contentParsed';

/**
 * Fetchs `target_account` from remote instance if needed, return `target_account` otherwise.
 *
 * @param account
 * @param target_account
 */
export async function remoteFetchAccount(
	account: TootDeckAccount,
	target_account: ParsedAccount
): Promise<ParsedAccount | null> {
	const handle = getHandleFromURL(target_account.url);
	return AccountsMananger.getByHandle(account, handle);
}

/**
 * Fetchs `target_status` from remote instance if needed, return `target_status` otherwise.
 *
 * @param account
 * @param target_status
 */
export async function remoteFetchStatus(
	account: TootDeckAccount,
	target_status: ParsedStatus
): Promise<ParsedStatus | null> {
	// Check if status is already loaded
	const status = ContentManager.Status.get(account, target_status.id);
	if (Object.is(status, target_status)) {
		return target_status as any;
	}

	// Cache status in this instance
	const fetch_status = await account.mirrorAPI.Search.statuses(target_status.url, {
		limit: 1,
		resolve: true,
	});

	// Unable to find remote status
	const remote_status = fetch_status?.[0];
	if (!remote_status) {
		console.warn(
			`Unable to find remote status. It may have been deleted or you don't have authorization to view it.`
		);
		return null;
	}

	return statusParser(account, remote_status);
}
