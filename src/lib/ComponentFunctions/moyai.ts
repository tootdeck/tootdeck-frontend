/*
 * From https://github.com/Vendicated/Vencord/blob/30ac25607023752031aa98060cbf8a736109992d/src/plugins/moyai/index.ts
 *
 * Vencord, a modification for Discord's desktop app
 * Copyright (c) 2022 Vendicated and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { get } from 'svelte/store';

import {
	rdOptionsBehaviorNotificationPlayCustomSounds,
	rdOptionsBehaviorNotificationPlayCustomSoundVolume,
	rdOptionsBehaviorNotificationPlayCustomSoundLimit,
} from '../../stores/Options';

import { CustomAudio } from '../Utils/js/audio';

export const MOYAI = '🗿';
export const MOYAI_URL = 'moyai.mp3';

const already_played = new Map<string, boolean>();

export interface CustomMoyai {
	name: string;
	src: string;
	find: string;
}

export function getRegex(sounds: Array<CustomMoyai>) {
	const transformed = sounds.map((custom) => `(${custom.find.replaceAll(',', '|')})`).join('|');
	return new RegExp(transformed, 'gmi');
}

export function getOccurrences(message: string, sounds: Array<CustomMoyai>, limit: number) {
	const occurrences: Array<CustomMoyai> = [];

	const regex = getRegex(sounds);

	for (const match of message.matchAll(regex)) {
		if (occurrences.length >= limit) {
			break;
		}

		const sound = sounds.find((custom) => custom.find.includes(match[0]))!;

		occurrences.push(sound);
	}

	return occurrences;
}

export async function boom(src: string, volume: number) {
	// if (!document.hasFocus()) {
	// return;
	// }
	const audio = new CustomAudio(src);

	audio.volume = volume;
	audio.addEventListener('ended', () => audio.remove());

	return audio.play();
}

export async function playCustomSound(status_id: string, plain_content: string) {
	if (!plain_content || already_played.has(status_id)) {
		return;
	}

	// Store status id to not play sounds at the same time
	already_played.set(status_id, true);
	// Clear after 1min
	setTimeout(() => already_played.delete(status_id), 60 * 1000);

	const custom_sound = get(rdOptionsBehaviorNotificationPlayCustomSounds);
	const limit = get(rdOptionsBehaviorNotificationPlayCustomSoundLimit);
	const volume = get(rdOptionsBehaviorNotificationPlayCustomSoundVolume);

	const custom_moyai = getOccurrences(plain_content, custom_sound, limit);

	for (const { src } of custom_moyai) {
		await boom(src, volume);
	}
}
