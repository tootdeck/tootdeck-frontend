import { fetchAPI } from '../../APIs/fetchAPI';

import type { ParsedAccount } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';
import type { MisskeyEntity } from '../../../types/misskeyEntities';

async function getRemoteMisskeyAccountID(origin: string, username: string) {
	return fetchAPI<MisskeyEntity.Account>('POST', `${origin}/api/users/show`, {
		body: { username },
		hide_error: true,
		cache_time: 60 * 60 * 1000,
	}).then((r) => {
		if (!r || r.status !== 200) {
			return undefined;
		}

		return r.data.id;
	});
}

export async function getRemoteAccountID(account: ParsedAccount): Promise<string | undefined> {
	const origin = new URL(account.url).origin;

	return fetchAPI<{ accounts: [Entity.Account] }>(
		'GET',
		`${origin}/api/v2/search?q=${account.username}&type=accounts&limit=1`,
		{ hide_error: true, cache_time: 60 * 60 * 1000 }
	).then((r) => {
		if (!r || r.status !== 200 || !r.data.accounts.length) {
			if (r?.status === 404) {
				return getRemoteMisskeyAccountID(origin, account.username);
			}
			return;
		}

		return r.data.accounts[0].id;
	});
}
