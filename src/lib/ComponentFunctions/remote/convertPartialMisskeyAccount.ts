import type { Entity } from '../../../types/mastodonEntities';
import type { MisskeyEntity } from '../../../types/misskeyEntities';

export function convertPartialMisskeyAccount(
	origin: string,
	misskey: MisskeyEntity.Account | MisskeyEntity.PartialAccount
): Entity.Account {
	let acct = misskey.username;
	let url = `${origin}/@${misskey.username}`;
	if (misskey.host) {
		acct += '@' + misskey.host;
		url = `https://${misskey.host}/@${misskey.username}`;
	}

	return {
		acct,
		avatar_static: '',
		avatar: misskey.avatarUrl,
		bot: misskey.isBot,
		created_at: new Date().toISOString(),
		discoverable: undefined,
		display_name: misskey.name,
		emojis: Object.entries<string>(misskey.emojis).map(
			([key, value]): Entity.Emoji => ({
				shortcode: key,
				static_url: '',
				url: value,
				visible_in_picker: true,
			})
		),
		fields: [],
		followers_count: 0,
		following_count: 0,
		group: null,
		header_static: '',
		header: '',
		id: misskey.id,
		limited: null,
		locked: false,
		moved: null,
		mute_expires_at: undefined,
		noindex: null,
		note: '',
		role: undefined,
		source: undefined,
		statuses_count: 0,
		suspended: null,
		url,
		username: misskey.username,
	};
}
