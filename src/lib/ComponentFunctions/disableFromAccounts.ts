import { get } from 'svelte/store';

import { stAccounts, type TootDeckAccount } from '../../stores/Account/Accounts';

import { addFullHandle } from './addFullHandle';

import type { Composer } from '../../types/composer';
import { Entity } from '../../types/mastodonEntities';

export async function disableFromAccounts(composer: Composer) {
	const replying_to_status = get(composer.replying_to_status);

	if (
		!replying_to_status ||
		replying_to_status.visibility === Entity.Visibility.Public ||
		replying_to_status.visibility === Entity.Visibility.Unlisted
	) {
		return;
	}

	const account = get(composer.account);
	const accounts = Array.from(get(stAccounts));

	const status_account_handle = addFullHandle(account, replying_to_status!.account);

	const to_enable = await Promise.all(
		accounts.map(async ([handle, entity]) => {
			if (account.mirrorAPI.handle_string === handle) {
				return [handle, true];
			}

			let relation = entity.relationship.get(status_account_handle);
			if (!relation) {
				const status = await entity.mirrorAPI.Search.statuses(replying_to_status!.url, {
					limit: 1,
				}).then((r) => (r ? r[0] : null));
				if (!status) {
					return [handle, false];
				}

				const id = status.account.id;
				const response = await entity.mirrorAPI.Accounts.getRelationship(id);
				if (!response) {
					return [handle, false];
				}

				relation = { ...response, acct: status_account_handle };

				entity.relationship.set(id, relation);
				entity.relationship.set(status_account_handle, relation);
			}

			return [handle, relation.following];
		})
	).then((values) => values.filter(([_, relation]) => relation).map(([handle, _]) => handle));

	const enabled = accounts.map(([handle, entity]) => [
		handle,
		entity,
		to_enable.includes(handle),
	]) as Array<[string, TootDeckAccount, boolean]>;

	composer.enabled_accounts.set(enabled);
}
