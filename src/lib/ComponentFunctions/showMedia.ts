import type { ParsedAccountStatus, ParsedStatus } from '../../types/contentParsed';
import type { Entity } from '../../types/mastodonEntities';

export function showMedia(entity: Entity.Status | ParsedAccountStatus | ParsedStatus) {
	return entity.sensitive ? false : !entity.spoiler_text;
}
