import { Account, accountsSameInstance } from '../../stores/Account/Accounts';
import { stLayout } from '../../stores/Columns/Layout';

import { TootDeckAPI } from '../APIs/TootDeckAPI/TootDeckAPI';
import { TootDeckConfig } from '../TootDeckConfig';
import { errorModal } from '../errorModal';

export async function unlinkAccount(handle: string) {
	const reponse = await TootDeckAPI.User.unlink(handle);
	if (!reponse) {
		errorModal(`Failed to unlink ${handle}`, undefined, true);
		return;
	}

	const [username, domain] = handle.split('@');
	// Remove account entity
	Account.remove({ username, domain });
	accountsSameInstance();

	// Remove all related config linked with it
	const config = TootDeckConfig.importLocal();
	if (config.value) {
		config.value.accounts_options = config.value.accounts_options.filter(
			(x) => x.handle !== handle
		);
		config.value.columns = config.value.columns.filter(
			(x) => !(x.params.account.username === username && x.params.account.domain === domain)
		);
		TootDeckConfig.saveLocal(config);
		TootDeckConfig.upload(config.value);
	}

	// Remove account columns
	stLayout.update((v) =>
		v.filter((x) => {
			const column = x.params.account;
			return !(column.username === username && column.domain === domain);
		})
	);
}
