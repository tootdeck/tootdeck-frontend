import { get } from 'svelte/store';

import type { TootDeckAccount } from '../../stores/Account/Accounts';
import { Layout, stLayout } from '../../stores/Columns/Layout';

import { columnFactory } from '../Columns/ColumnFactory';
import { TootDeckColumnIdentifier } from '../Columns/TootDeckColumnTypes';
import type { HashtagColumn } from '../Columns/TootDeckColunms/HashtagColumn';
import { EventsManager } from '../Events/EventsManager';
import { TootDeckEvents } from '../Events/TootDeckEvents';

async function appendStatusesToColumn(column: HashtagColumn, hashtag: string): Promise<void> {
	hashtag = hashtag.toLowerCase();

	const request = await column.account.mirrorAPI.Timeline.getTag(hashtag);
	EventsManager.sendEvent(TootDeckEvents.HashtagColumn, {
		account: column.account,
		data: structuredClone(request ?? []).map((s) => ({
			...s,
			animateIn: false,
			animateOut: true,
		})),
	});
}

function generateHashtagColumn(account: TootDeckAccount, hashtag: string): HashtagColumn {
	hashtag = hashtag.toLowerCase();

	return columnFactory.create(TootDeckColumnIdentifier.Hashtag, {
		account: {
			username: account.mirrorAPI.handle.username,
			domain: account.mirrorAPI.handle.domain,
		},
		options: {
			collapsed: false,
			sound_uuid: null,
			override_title: {
				toggle: false,
				value: null,
			},
		},
		hashtag,
	});
}

export async function createAndInsertHashtagColumn(
	account: TootDeckAccount,
	hashtag: string,
	index: number
) {
	hashtag = hashtag.toLowerCase();

	// Check for duplicate
	for (const instance of get(stLayout)) {
		if (
			instance.identifier === TootDeckColumnIdentifier.Hashtag &&
			(instance as HashtagColumn).bindToHashtag === hashtag
		) {
			return;
		}
	}

	const column = generateHashtagColumn(account, hashtag);

	Layout.add(column, index);

	await appendStatusesToColumn(column, hashtag);
}
