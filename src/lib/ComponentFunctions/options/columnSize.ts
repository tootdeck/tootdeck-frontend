import { stOptions } from '../../../stores/Options';

import { bodyChangeClass } from '../../Utils/bodyChangeClass';

export enum ColumnWidthName {
	Narrow = 'Narrow',
	Medium = 'Medium',
	Wide = 'Wide',
	Custom = 'Custom',
}

export enum ColumnWidth {
	Narrow = '270px',
	Medium = '310px',
	Wide = '370px',
}

export const column_width_class: Record<ColumnWidthName, string> = {
	[ColumnWidthName.Narrow]: 'column-narrow',
	[ColumnWidthName.Medium]: 'column-medium',
	[ColumnWidthName.Wide]: 'column-wide',
	[ColumnWidthName.Custom]: 'column-custom',
};

function updateStore(value: string) {
	stOptions.update((v) => {
		v.appearance.column.width = value;
		return v;
	});
}

export function setColumnSize(selected: ColumnWidthName | string) {
	const class_names = Object.values(column_width_class);
	switch (selected) {
		case ColumnWidthName.Narrow:
			bodyChangeClass(class_names, column_width_class[ColumnWidthName.Narrow]);
			updateStore(ColumnWidth.Narrow);
			break;
		case ColumnWidthName.Medium:
			bodyChangeClass(class_names, column_width_class[ColumnWidthName.Medium]);
			updateStore(ColumnWidth.Medium);
			break;
		case ColumnWidthName.Wide:
			bodyChangeClass(class_names, column_width_class[ColumnWidthName.Wide]);
			updateStore(ColumnWidth.Wide);
			break;
		default:
			bodyChangeClass(class_names, column_width_class[ColumnWidthName.Custom]);
			updateStore(selected);
			break;
	}
}
