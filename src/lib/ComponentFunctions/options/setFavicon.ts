import { stOptions } from '../../../stores/Options';

import { randomInt } from '../../Utils/js/randomInt';

import { prideLogo } from '../../../assets/prideLogo';

function isPrideMonth() {
	return new Date().getMonth() === 5;
}

function selectRandomPrideLogo() {
	const keys = Object.keys(prideLogo);

	return keys[randomInt(0, keys.length - 1)];
}

function selectPrideLogo(options: {
	toggle: boolean;
	scheduled: boolean;
	random: boolean;
	src: string;
	userDefined: string;
}) {
	return options.random ? selectRandomPrideLogo() : options.userDefined || options.src;
}

export function setFavicon(options: {
	toggle: boolean;
	scheduled: boolean;
	random: boolean;
	src: string;
	userDefined: string;
}) {
	let src = 'mastodon';

	switch (options.toggle) {
		case false:
			break;
		case true:
			switch (options.scheduled) {
				case false:
					src = selectPrideLogo(options);
					break;
				case true:
					src = isPrideMonth() ? selectPrideLogo(options) : 'mastodon';
					break;
			}
	}

	stOptions.update((v) => {
		v.appearance.extra.prideLogo.src = src;
		return v;
	});

	document.head
		.querySelector("[rel='icon']")
		?.setAttribute('href', prideLogo[src] ?? 'tootdeck.svg');
}
