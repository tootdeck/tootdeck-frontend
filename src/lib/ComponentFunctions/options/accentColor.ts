import { stOptions } from '../../../stores/Options';

import { type RGB } from '../../Utils/convertRGBA';

export function customAccentColor(custom: RGB) {
	document.body.setAttribute('style', `--accent: ${custom};`);
}

function updateStore(value: RGB) {
	stOptions.update((v) => {
		v.appearance.accentColor = value;
		return v;
	});
}

export function setAccentColor(value: RGB) {
	customAccentColor(value);
	updateStore(value);
}
