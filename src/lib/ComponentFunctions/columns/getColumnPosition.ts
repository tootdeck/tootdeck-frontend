import { get } from 'svelte/store';

import { stLayout } from '../../../stores/Columns/Layout';

import type { IColumn } from '../../Columns/types/interfaces';
import type { ThreadColumn } from '../../Columns/ThreadColumn';
import { TootDeckColumnIdentifier } from '../../Columns/TootDeckColumnTypes';

export function getColumnPosition(column: IColumn) {
	const layout = get(stLayout);
	for (let i = 0; i < layout.length; i++) {
		let c = layout[i];
		if (c.identifier === TootDeckColumnIdentifier.Thread) {
			const parent = (c as ThreadColumn).parentColumn;
			if (parent) {
				c = parent;
			}
		}

		if (Object.is(c, column)) {
			return i + 1;
		}
	}

	return 0;
}
