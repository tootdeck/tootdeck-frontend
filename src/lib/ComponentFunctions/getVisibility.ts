import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { Entity } from '../../types/mastodonEntities';

export function getVisibility(
	account: TootDeckAccount
): Exclude<Entity.Visibility, Entity.Visibility.Direct> {
	return (
		(account.entity.source?.privacy as Exclude<Entity.Visibility, Entity.Visibility.Direct>) ??
		Entity.Visibility.Public
	);
}
