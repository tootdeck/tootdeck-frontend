import type { TootDeckAccount } from '../../stores/Account/Accounts';

import type { ParsedStatus } from '../../types/contentParsed';
import { InstanceType } from '../../types/megalodon';

export function generateLocalStatusURL(account: TootDeckAccount, status: ParsedStatus): string {
	switch (account.mirrorAPI.instance_type) {
		case InstanceType.Mastodon:
			return `https://${account.mirrorAPI.domain}/@${status.account.acct}/${status.id}`;

		case InstanceType.Pleroma:
			return `https://${account.mirrorAPI.domain}/notice/${status.id}`;

		//TODO: Add support for Misskey
		// case InstanceType.Misskey:
		// return
	}

	return '';
}

export function generateStatusURL(account: TootDeckAccount, status: ParsedStatus) {
	if (status.visibility === 'direct' || status.visibility === 'private') {
		return generateLocalStatusURL(account, status);
	}

	return status.url;
}
