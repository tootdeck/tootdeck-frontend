import { get } from 'svelte/store';

import type { TootDeckAccount } from '../../../stores/Account/Accounts';
import { NotificationModalType, notificationModal } from '../../../stores/NotificationsModal';

import type { ThreadColumn } from '../../Columns/ThreadColumn';
import { AccountsMananger } from '../../Managers/AccountsManager';
import { ContentManager } from '../../Managers/ContentManager';

import type { PartialThreadStatus, ThreadStatus } from '../../../types/thread';
import type { ParsedAccount, ParsedStatus } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';

/**
 * Algorithm
 */
// #region

/**
 * Algorithm util
 */
// #region

/**
 * Generate fake status in case of a deleted status in a thread
 *
 * @param status_account
 * @param previous_status
 * @returns fake deleted status
 */
export function deleteNotify(
	status_account: Entity.Account | ParsedAccount,
	previous_status: Entity.Status | ParsedStatus
): PartialThreadStatus<any> {
	const status: ParsedStatus = {
		account: status_account as any,
		animateIn: false,
		animateOut: false,
		application: null,
		bookmarked: false,
		card: null,
		content: '⚠️ This status has been deleted ⚠️' as any,
		created_at: previous_status.created_at,
		edited_at: null,
		emoji_reactions: [],
		emojis: [],
		favourited: false,
		favourites_count: 0,
		id: previous_status.id + '_deleted',
		in_reply_to_id: null,
		in_reply_to_account_id: null,
		language: 'en',
		media_attachments: [],
		mentions: [],
		muted: false,
		quote: false,
		reblog: null,
		reblogged: false,
		reblogs_count: 0,
		replies_count: 0,
		pinned: false,
		plain_content: 'This status has been deleted',
		poll: null,
		sensitive: false,
		spoiler_text: '',
		tags: [],
		uri: '',
		url: '',
		visibility: previous_status.visibility,
		tootdeck: {
			height: 0,
			in_notification_id: [],
			in_status: [],
			show_cw: false,
			media_attachments_show: null,
			quote_urls: [],
			update: -1,
			uuid: window.crypto.randomUUID(),
			is_twitter_embed: false,
			embed: {
				twitter: [],
				youtube: [],
				dailymotion: [],
			},
		},
	};

	return {
		status: status,
		lines: {
			up: false,
			down: true,
		},
	};
}

// #endregion

/**
 * Filter replies not directly related to the thread
 *
 * Insert fake status at the top in case of a deleted status in a thread
 *
 * `ancestors` array is shrink when function returns
 *
 * @param target selected status
 * @param ancestors
 * @returns thread
 */
function buildUpThread(
	account: TootDeckAccount,
	target: Entity.Status,
	ancestors: Array<Entity.Status>
): Array<PartialThreadStatus<Entity.Status>> {
	let ret: Array<PartialThreadStatus<Entity.Status>> = [];
	let last: Entity.Status = target;
	let ancestor_index: number;
	while (
		last.in_reply_to_id &&
		(ancestor_index = ancestors.findIndex((x) => x.id === last.in_reply_to_id)) !== -1
	) {
		const ancestor = ancestors[ancestor_index];

		ret.push({
			status: ancestor,
			lines: {
				up: !!ancestor.in_reply_to_id || !!ancestor.in_reply_to_account_id,
				down: true,
			},
		});
		last = ancestor;

		ancestors.splice(ancestor_index, 1);
	}

	// if status deleted
	if (!last.in_reply_to_id && last.in_reply_to_account_id) {
		const deleted = deleteNotify(
			AccountsMananger.getByIDSync(account, last.in_reply_to_account_id) ?? last.account,
			last
		);
		ret.push(deleted);
		last.in_reply_to_id = deleted.status.id;
	}

	return ret.reverse();
}

/**
 * Find and order next thread statuses, replies are filtered to another array
 *
 * `descendants` array is shrink when function returns
 *
 * @param target selected status
 * @param descendants
 * @returns Found and ordered thread statuses and a array of unfiltered replies
 */
function FindNextStatusesThread(
	target: Entity.Status,
	descendants: Array<Entity.Status>
): [Array<PartialThreadStatus<Entity.Status>>, Array<Entity.Status>] {
	let ret: Array<PartialThreadStatus<Entity.Status>> = [];
	let replying_to: Entity.Status = target;
	let descendant_index: number;

	ret.push({
		status: target,
		lines: {
			up: true,
			down: false,
		},
	});

	// While replies are linked and replies owner matches `target` account
	while (
		(descendant_index = descendants.findIndex(
			(x) =>
				replying_to.id === x.in_reply_to_id &&
				target.in_reply_to_account_id === x.in_reply_to_account_id
		)) !== -1
	) {
		const descendant = descendants[descendant_index];

		ret.push({
			status: descendant,
			lines: {
				up: true,
				down: false,
			},
		});
		replying_to = descendant;

		descendants.splice(descendant_index, 1);
	}

	return [ret, descendants];
}

/**
 * Removes divergent node of a reply to a reply
 *
 * `descendants` array is empty when function returns
 *
 * @param target selected status
 * @param descendants grouped thread descendants
 * @returns grouped thread descendants
 */
function removeDivergentReplyNode(
	target: Entity.Status,
	descendants: Array<PartialThreadStatus<Entity.Status>>
) {
	let ret: Array<PartialThreadStatus<Entity.Status>> = [];
	let descendant_index: number = 0;

	while (descendants.length) {
		const node = descendants[descendant_index];
		const next_index = descendants.findIndex((x) => node.status.id === x.status.in_reply_to_id);
		if (
			// Directly replying to selected status
			node.status.in_reply_to_id === target.id ||
			// Replying to a thread of replies
			next_index === descendant_index + 1 ||
			// Last reply of a thread of replies
			(next_index === -1 && ret.at(-1)?.status.id === node.status.in_reply_to_id)
		) {
			ret.push(node);
		}

		descendants.splice(descendant_index, 1)[0];
	}

	return ret;
}

/**
 * Recursively group replies
 *
 * @param target status to find replies from
 * @param descendants
 * @param found parameter used for and by recursivity, do not provide it
 * @returns group of replies
 */
function findNextDescendants(
	target: Entity.Status,
	descendants: Array<Entity.Status>,
	found: Array<PartialThreadStatus<Entity.Status>> = []
): Array<PartialThreadStatus<Entity.Status>> {
	let descendant_index: number;
	descendant_index = descendants.findIndex((x) => target.id === x.in_reply_to_id);
	if (descendant_index === -1) {
		return found;
	}

	const next = descendants.splice(descendant_index, 1)[0];
	found.push({
		status: next,
		lines: {
			up: false,
			down: true,
		},
	});

	return findNextDescendants(next, descendants, found);
}

/**
 * Filter and group thread descendants, remove divergent replies
 *
 * `descendants` array is empty when function returns
 *
 * @param target selected status
 * @param descendants
 * @returns Filtered and grouped thread descendants
 */
function filterAndGroupDescendants(
	target: Entity.Status,
	descendants: Array<Entity.Status>
): Array<PartialThreadStatus<Entity.Status>> {
	let ret: Array<PartialThreadStatus<Entity.Status>> = [];
	let descendant_index: number = 0;

	while (descendants.length !== 0) {
		const node = descendants[descendant_index];

		ret.push({
			status: node,
			lines: {
				up: false,
				down: false,
			},
		});
		descendants.splice(descendant_index, 1)[0];

		if (node.in_reply_to_id) {
			ret.push(...findNextDescendants(node, descendants));
		}
	}

	return removeDivergentReplyNode(target, ret);
}

// #endregion

/**
 * Utils
 */
// #region

/**
 * Provide linking informations for an ordered statuses array
 *
 * Modify inner statuses
 *
 * @param partials ordered statuses in thread
 */
export function linkStatuses(
	partials: Array<PartialThreadStatus<Entity.Status>> | Array<ThreadStatus>
): void {
	for (let i = 0; i < partials.length; i++) {
		const previous = partials[i - 1];
		const current = partials[i];
		const next = partials[i + 1];
		if (previous) {
			current.lines.up = current.status.in_reply_to_id === previous.status.id;
		} else {
			current.lines.up = false;
		}

		if (next) {
			current.lines.down = next.status.in_reply_to_id === current.status.id;
		} else {
			current.lines.down = false;
		}
	}
}

/**
 * Convert `PartialThreadStatus<Entity.Status>`
 *
 * to `PartialThreadStatus<ParsedStatus>`
 *
 * @param account Logged account entity
 * @param column thread column previously instantiated
 * @param x PartialThreadStatus<Entity.Status>`
 * @returns `PartialThreadStatus<ParsedStatus>`
 */
function remapWithParsedContent(
	column: ThreadColumn,
	x: PartialThreadStatus<Entity.Status>
): PartialThreadStatus<ParsedStatus> {
	// @ts-ignore
	const status = ContentManager.Status.store(column, x.status);

	return { ...x, status };
}

// #endregion

/**
 * Trigger
 */

/**
 * Retrieve selected status context, organize the replies and provide linking informations to each of them
 *
 * @param account Logged account entity
 * @param selected_status
 * @param column thread column previously instantiated
 * @returns `null` on failure
 * @returns column's content store
 */
export async function getThreadContext(
	account: TootDeckAccount,
	selected_status: ParsedStatus,
	column: ThreadColumn
) {
	/**
	 * Casting to `Entity.Status` to hide TypeScript linter errors
	 * Using `Entity.Status` or `ParsedStatus` has no impact when
	 * values (content, tootdeck, account.note, account.display_name, account.fields, account.tootdeck) aren't accessed
	 */
	const status = selected_status as any as Entity.Status;

	// Retrieve selected status context
	const ctx = await account.mirrorAPI.Statuses.getContext(status.id);
	if (!ctx) {
		notificationModal.create(
			NotificationModalType.Warn,
			'Unable to retrieve status informations.',
			true
		);
		return null;
	}

	ctx.ancestors = ctx.ancestors.map((s) => ({ ...s, animateIn: false, animateOut: false }));
	ctx.descendants = ctx.descendants.map((s) => ({ ...s, animateIn: false, animateOut: false }));

	// Filter and organize ancestors
	const filtered_ancestors = buildUpThread(account, status, [...ctx.ancestors]);

	// Find and order next thread of statuses, replies are filtered
	const [filtered_descendants, remaing_descendants] = FindNextStatusesThread(status, [
		...ctx.descendants,
	]);
	// Add lines informations to statuses

	// Filter and organize replies
	const ordered_descendants = filterAndGroupDescendants(status, remaing_descendants);

	/**
	 * Parse statuses and assigning them to the column
	 */
	const thread_ancestors = filtered_ancestors.map((x) => remapWithParsedContent(column, x));
	const thread_descendants = filtered_descendants.map((x) => remapWithParsedContent(column, x));
	const next_descendants = ordered_descendants.map((x) => remapWithParsedContent(column, x));

	// Retrieve `selected_status` from the thread and remove it from the thread.
	const thread_current = thread_descendants.shift()!;

	// Cleaning map before updating
	const stColumnContent = column.content;
	const column_content = get(stColumnContent);
	if (column_content.length) {
		column.clear(false);
	}

	column.append(thread_ancestors);
	column.append(thread_current, true);
	column.append(thread_descendants);
	column.append(next_descendants);

	// Add lines informations to statuses
	linkStatuses(column_content.get());

	return stColumnContent;
}
