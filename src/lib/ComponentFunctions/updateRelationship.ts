import type { TootDeckAccount, EntityRelationship } from '../../stores/Account/Accounts';
import { stRelationshipUpdate } from '../../stores/relationship';

import { updateObject } from '../Utils/js/updateObject';

import type { Entity } from '../../types/mastodonEntities';

/**
 * Updates `cached_relationship` with given entry.
 *
 * @param account
 * @param acct
 * @param incoming_relationship
 * @param cached_relationship
 */
export function updateRelationship(
	account: TootDeckAccount,
	acct: string,
	incoming_relationship: Entity.Relationship,
	cached_relationship?: Entity.Relationship
) {
	const entity = incoming_relationship as EntityRelationship;
	entity.acct = acct;

	if (cached_relationship) {
		updateObject(cached_relationship, incoming_relationship);
	}

	account.relationship.set(entity.id, entity);
	account.relationship.set(entity.acct, entity);

	stRelationshipUpdate.update((v) => ++v);
}

/**
 * Gets relationship from cache and update it with given entry.
 *
 * @param account
 * @param acct
 * @param incoming_relationship
 */
export function getUpdateRelationship(
	account: TootDeckAccount,
	acct: string,
	incoming_relationship: Entity.Relationship
) {
	const cached_relationship = account.relationship.get(acct);
	updateRelationship(account, acct, incoming_relationship, cached_relationship);
}

/**
 * Gets relationship from cache and update it with given entry.
 *
 * @param account
 * @param target
 */
export async function fetchUpdateRelationship(
	account: TootDeckAccount,
	target: { id: string; acct: string }
) {
	const relationship = await account.mirrorAPI.Accounts.getRelationship(target.id);
	if (relationship) {
		getUpdateRelationship(account, target.acct, relationship);
	}
}
