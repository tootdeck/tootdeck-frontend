import type { ParsedStatus } from '../../types/contentParsed';

export const getRebloggedStatus = (status: ParsedStatus) =>
	status.reblog ? status.reblog : status;
