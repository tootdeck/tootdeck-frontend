import type { TootDeckAccount } from '../../stores/Account/Accounts';
import { type LayoutColumnList } from '../../stores/Columns/Layout';

import { columnFactory } from '../Columns/ColumnFactory';
import type { StandardColumn } from '../Columns/StandardColumn';
import { TootDeckColumnIdentifier } from '../Columns/TootDeckColumnTypes';
import type { ListColumn } from '../Columns/TootDeckColunms/ListColumn';
import type { IColumnType } from '../Columns/types/interfaces';
import type { StandardColumnListParams } from '../Columns/types/options';

import { notImplemented } from '../Utils/noImplemented';
import { errorModal } from '../errorModal';

import type { ParsedStatus } from '../../types/contentParsed';
import type { Entity } from '../../types/mastodonEntities';

class WhichAccountInner {
	static readonly instance = new WhichAccountInner();

	private column: StandardColumn<any> | null = null;

	regenColumn(
		account: TootDeckAccount,
		column_type: IColumnType,
		list?: Entity.List
	): StandardColumn<any> | null {
		if (column_type.identifier === TootDeckColumnIdentifier.List && !list) {
			errorModal(
				'WhichAccountInner: Unable to create ListColumn, undefined list id.',
				undefined,
				true
			);
			return null;
		}

		const column_list = this.column?.props as
			| LayoutColumnList<ListColumn, ParsedStatus>
			| undefined;

		if (
			this.column?.params.account.username === account.entity.id &&
			column_list?.list.id === list?.id
		) {
			return this.column;
		}

		this.destroy(true);

		try {
			this.column = columnFactory.create(column_type.identifier, {
				account: {
					username: account.entity.username,
					domain: account.mirrorAPI.domain,
				},
				options: {
					collapsed: false,
				},
				list,
			} as StandardColumnListParams);

			return this.column;
		} catch (e: any) {
			console.error(e);
			notImplemented();
			return null;
		}
	}

	destroy(all: boolean = false) {
		if (all) {
			this.column?.destroy();
		}
		this.column = null;
	}
}

export const WhichAccount = WhichAccountInner.instance;
