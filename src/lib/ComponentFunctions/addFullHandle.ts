import type { TootDeckAccount } from '../../stores/Account/Accounts';

export function addFullHandle(account: TootDeckAccount, entity: { acct: string }) {
	const split = entity.acct.split('@');
	if (split[1]) {
		return entity.acct;
	}

	return entity.acct.split('@')[0]! + '@' + account.mirrorAPI.domain;
}

export const addFullHandleIfNecessary = (same_instance: boolean, account: TootDeckAccount) =>
	same_instance ? account.entity.username : account.mirrorAPI.handle_string;
