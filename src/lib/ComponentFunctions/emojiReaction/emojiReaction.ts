import type { TootDeckAccount } from '../../../stores/Account/Accounts';

import { statusParser } from '../../ContentParser/ContentParser';
import { EventsManager } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import { RequestsManager } from '../../Managers/RequestsManager';
import { errorModal } from '../../errorModal';

import type { ParsedStatus } from '../../../types/contentParsed';

export async function createEmojiReaction(
	account: TootDeckAccount,
	status: ParsedStatus,
	name: string
): Promise<ParsedStatus | null> {
	name = name.replaceAll(':', '');
	return await RequestsManager.await(
		`React:${account.mirrorAPI.handle_string}:${status.id}:${encodeURI(name)}`,
		async () => {
			const response = await account.mirrorAPI.Statuses.Reaction.create(status.id, name);
			if (!response) {
				errorModal('Failed to create reaction.', undefined, true);
				return null;
			}

			const found = response.emoji_reactions.find((reaction) => reaction.name === name);
			if (found) {
				const old = status.emoji_reactions.find((reaction) => reaction.name === name);
				if (old?.count === found.count) {
					found.count++;
				}
			}

			EventsManager.sendEvent(TootDeckEvents.Status, {
				account,
				data: [response],
			});

			return statusParser(account, response);
		}
	);
}

export async function removeEmojiReaction(
	account: TootDeckAccount,
	status: ParsedStatus,
	name: string
): Promise<ParsedStatus | null> {
	name = name.replaceAll(':', '');
	return await RequestsManager.await(
		`Unreact:${account.mirrorAPI.handle_string}:${status.id}:${encodeURI(name)}`,
		async () => {
			const response = await account.mirrorAPI.Statuses.Reaction.remove(status.id, name);
			if (!response) {
				errorModal('Failed to remove reaction.', undefined, true);
				return null;
			}

			const found = response.emoji_reactions.find((reaction) => reaction.name === name);
			if (found) {
				const old = status.emoji_reactions.find((reaction) => reaction.name === name);
				if (old?.count === found.count) {
					found.count--;
				}
			}

			EventsManager.sendEvent(TootDeckEvents.Status, {
				account,
				data: [response],
			});

			return statusParser(account, response);
		}
	);
}
