import type { TootDeckAccount } from '../../../stores/Account/Accounts';

import type { ParsedStatus } from '../../../types/contentParsed';

export function emojiReactionReached(account: TootDeckAccount, status: ParsedStatus) {
	const count = account.instance.configuration?.reactions?.max_reactions ?? Infinity;
	return status.emoji_reactions.filter((r) => r.me).length >= count;
}
