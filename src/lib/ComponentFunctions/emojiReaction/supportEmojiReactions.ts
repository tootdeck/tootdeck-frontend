import type { TootDeckAccount } from '../../../stores/Account/Accounts';
import type { InstanceOverride } from '../../../stores/Options';

export function supportEmojiReactions(
	account: TootDeckAccount,
	rdOptionsBehaviorInstance: InstanceOverride
) {
	const detected = account.instance.support_emoji_reactions;
	const override = rdOptionsBehaviorInstance[account.mirrorAPI.domain]?.emoji_reaction || null;

	return (detected && override === null) || override;
}
