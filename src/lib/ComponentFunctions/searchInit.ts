import { derived, get, writable } from 'svelte/store';
import type { Readable } from 'svelte/motion';

import { composerInit } from './composerInit';

import type { Search } from '../../types/search';

export function getEnabledValue(value: { [k: string]: boolean }) {
	return Object.entries(value)
		.map(([k, v]) => (v ? k : ''))
		.join('');
}

export function searchInit(): Search {
	const composer = composerInit();
	const { account, language: default_language } = composer;

	const view = writable({
		simple: true,
		advanced: false,
	});
	const toggle = writable({
		type: false,
		has: false,
		is: false,
		language: false,
		from: false,
		before: false,
		during: false,
		after: false,
		in: false,
	});
	const type = writable({
		accounts: false,
		hashtags: false,
		statuses: false,
	});
	const has = writable({
		media: false,
		poll: false,
		embed: false,
	});
	const is = writable({
		reply: false,
		sensitive: false,
	});
	const language = writable(get(default_language));
	const from = writable({
		acct: '',
		id: '',
	});
	const me = writable(false);
	const before = writable('');
	const during = writable('');
	const after = writable('');
	const _in = writable({
		all: false,
		library: false,
	});
	const show = {
		language_selector: writable(false),
		tag_selector: writable(false),
	};
	const query = writable('');

	const output: Readable<string> = derived(
		[toggle, has, is, language, from, me, before, during, after, _in, query],
		(
			[$toggle, $has, $is, $language, $from, $me, $before, $during, $after, $_in, $query],
			set
		) => {
			let value = '';

			const has_value = getEnabledValue($has);
			if ($toggle.has && has_value) {
				value += ` has:${has_value}`;
			}

			const is_value = getEnabledValue($is);
			if ($toggle.is && is_value) {
				value += ` is:${is_value}`;
			}

			if ($toggle.language) {
				value += ` language:${$language}`;
			}

			if ($toggle.from) {
				if ($me) {
					value += ` from:me`;
				} else if ($from.acct) {
					value += ` from:${$from.acct}`;
				}
			}

			if ($toggle.before && $before) {
				value += ` before:${$before}`;
			}

			if ($toggle.during && $during) {
				value += ` during:${$during}`;
			}

			if ($toggle.after && $after) {
				value += ` after:${$after}`;
			}

			const in_value = getEnabledValue($_in);
			if ($toggle.in && in_value) {
				value += ` in:${in_value}`;
			}

			value += ' ' + $query;

			set(value.trim());
		}
	);

	return {
		composer,
		account,
		toggle,
		props: {
			searched: writable(false),
			search_result: writable({
				accounts: [],
				hashtags: [],
				statuses: [],
			}),
			view,
		},
		values: {
			type,
			has,
			is,
			language,
			from,
			me,
			before,
			during,
			after,
			in: _in,
			query,
		},
		show,
		output,
	};
}
