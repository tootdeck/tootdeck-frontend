import type { TootDeckAccount } from '../../../stores/Account/Accounts';

import { errorModal } from '../../errorModal';
import { updateRelationship } from '../updateRelationship';
import { addFullHandle } from '../addFullHandle';

import type { ParsedAccount } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';

export async function follow(
	account: TootDeckAccount,
	status_account: ParsedAccount,
	relationship: Entity.Relationship | undefined
) {
	const response = await account.mirrorAPI.Accounts.follow(status_account.id);
	if (!response) {
		errorModal(`Unable to follow ${status_account.acct}.`, undefined, true);
		return;
	}

	updateRelationship(account, addFullHandle(account, status_account), response, relationship);
}

export async function unfollow(
	account: TootDeckAccount,
	status_account: ParsedAccount,
	relationship: Entity.Relationship | undefined
) {
	const response = await account.mirrorAPI.Accounts.unfollow(status_account.id);
	if (!response) {
		errorModal(`Unable to unfollow ${status_account.acct}.`, undefined, true);
		return;
	}

	updateRelationship(account, addFullHandle(account, status_account), response, relationship);
}
