import type { TootDeckAccount } from '../../../stores/Account/Accounts';

import { errorModal } from '../../errorModal';
import { updateRelationship } from '../updateRelationship';
import { addFullHandle } from '../addFullHandle';

import type { ParsedAccount } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';

export async function unblock(
	account: TootDeckAccount,
	status_account: ParsedAccount,
	relation: Entity.Relationship | undefined
) {
	const response = await account.mirrorAPI.Accounts.unblock(status_account.id);
	if (!response) {
		errorModal(`Unable to unblock ${status_account.acct}.`, undefined, true);
		return;
	}

	updateRelationship(account, addFullHandle(account, status_account), response, relation);
}
