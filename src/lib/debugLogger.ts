import { AppInit } from '../AppInit';
import { stDebug } from '../stores/Options';

let debug: boolean = false;
AppInit.Promise.then(() => {
	stDebug.subscribe((v) => (debug = v));
});

export const debugLogger = (...data: Array<any>) => {
	if (debug) {
		console.warn(...data);
	}
};
