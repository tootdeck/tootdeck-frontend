import { get } from 'svelte/store';
import { PickerInstance, type EmojiIndexed } from 'emoji-mart-svelte';

import {
	rdOptionsAppearanceAccentColor,
	rdOptionsAppearanceEmojiStyle,
	rdOptionsAppearanceTheme,
	rdOptionsBehaviorEmojiPickerToggleWideEmoji,
	rdOptionsBehaviorEmojiPickerTopRow,
} from '../../stores/Options';
import { ThemeAvailable } from '../ComponentFunctions/options/theme';

import { scrollTop } from '../Utils/scroll';

import type { CustomEmojiCatergory } from '../../types/emoji';

export class EmojiPicker {
	private readonly PickerInstance: PickerInstance;

	constructor(localStorageKeyPrefix: string, custom: Array<CustomEmojiCatergory> = []) {
		this.PickerInstance = new PickerInstance(
			{
				accentColor: get(rdOptionsAppearanceAccentColor),
				theme: get(rdOptionsAppearanceTheme) === ThemeAvailable.Light ? 'light' : 'dark',
				topRow: get(rdOptionsBehaviorEmojiPickerTopRow),
				toggleWideEmoji: get(rdOptionsBehaviorEmojiPickerToggleWideEmoji),
				style: get(rdOptionsAppearanceEmojiStyle),
				localStorageKeyPrefix,
				customDataset: custom as any,
			},
			{
				scrollTo: scrollTop,
			}
		);

		rdOptionsAppearanceAccentColor.subscribe((v) => {
			this.PickerInstance.setAccentColor(v).update();
		});

		rdOptionsAppearanceTheme.subscribe((v) => {
			this.PickerInstance.setTheme(v === ThemeAvailable.Light ? 'light' : 'dark').update();
		});

		rdOptionsBehaviorEmojiPickerTopRow.subscribe((v) => {
			this.PickerInstance.setTopRow(v).update();
		});

		rdOptionsBehaviorEmojiPickerToggleWideEmoji.subscribe((v) => {
			this.PickerInstance.setToggleWideEmoji(v).update();
		});

		rdOptionsAppearanceEmojiStyle.subscribe((v) => {
			this.PickerInstance.setStyle(v).update();
		});
	}

	search(
		value: string,
		limit: number,
		cb?: (value: Array<EmojiIndexed>) => void
	): Array<EmojiIndexed> {
		const emojis = this.PickerInstance.search(value, limit);

		if (cb) {
			cb(emojis);
		}
		return emojis;
	}

	getPicker() {
		return this.PickerInstance.getPickerNProps();
	}

	get instance() {
		return this.PickerInstance;
	}
}
