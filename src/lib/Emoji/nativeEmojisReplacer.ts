import { afterUpdate } from 'svelte';
import { get } from 'svelte/store';

import { stEmojis } from '../../stores/Emojis';

import type { EmojiPicker } from './EmojiPicker';

export function nativeEmojisReplacer(node: HTMLElement, args = { component: false }) {
	const picker = get(stEmojis).values().next().value as EmojiPicker;

	if (args.component) {
		afterUpdate(() => {
			picker.instance.parse(node);
		});
	} else {
		picker.instance.parse(node);
	}

	return {};
}
