import type { ActionReturn } from 'svelte/action';

import { rdOptionsBehaviorEmojiZoomOnHover } from '../../stores/Options';

import { AppInit } from '../../AppInit';

let option_enabled: boolean = true;
AppInit.Promise.then(() => {
	rdOptionsBehaviorEmojiZoomOnHover.subscribe((v) => (option_enabled = v));
});

function inject(node: Element, single: boolean) {
	const emojis = Array.from(
		single ? [node] : node.querySelectorAll('.emoji')
	) as Array<HTMLImageElement>;

	return emojis
		.map((emoji) => {
			if (emoji.getAttribute('zoom')) {
				return;
			}

			let div: HTMLDivElement | undefined = undefined;

			const mouseleave = () => {
				document.querySelectorAll('.emoji-zoom').forEach((x) => {
					try {
						x.remove();
					} catch (e) {}
				});
			};

			const mouseenter = () => {
				if (!option_enabled) {
					return;
				}

				div = document.createElement('div');
				div.classList.add('emoji-zoom');

				const inner = emoji.cloneNode(true);

				div.append(inner);

				const { x, y } = emoji.getBoundingClientRect();

				div.style.top = Math.floor(y) + 'px';
				div.style.left = Math.floor(x) + 'px';
				div.setAttribute('tabindex', '-1');

				document.querySelector('#app')!.append(div);

				div.addEventListener('blur', mouseleave);
				div.focus();
			};

			emoji.addEventListener('mouseenter', mouseenter);

			emoji.addEventListener('mouseleave', mouseleave);

			emoji.setAttribute('zoom', 'true');

			return {
				emoji,
				mouseenter,
				mouseleave,
			};
		})
		.filter((x) => x) as Array<{
		emoji: Element;
		mouseenter: () => void;
		mouseleave: () => void;
	}>;
}

export function emojiZoom(
	node: Element,
	args: { toggle: boolean; single: boolean } = { toggle: true, single: false }
): ActionReturn<any> {
	if (!args.toggle) {
		return {};
	}

	const injected = inject(node, args.single);

	return {
		destroy() {
			injected.forEach((x) => {
				x.emoji.removeEventListener('mouseenter', x.mouseenter);
				x.emoji.removeEventListener('mouseleave', x.mouseleave);
			});
		},
	};
}
