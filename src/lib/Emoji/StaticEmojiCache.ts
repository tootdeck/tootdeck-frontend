import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { TootDeckAPI } from '../APIs/TootDeckAPI/TootDeckAPI';

interface Emoji {
	name: string;
	url: string | undefined;
	static_url: string | undefined;
}

class StaticEmojiCacheInner {
	static readonly instance = new StaticEmojiCacheInner();

	private readonly cache = new Map<string, string>();

	private getIdentifier(name: string, domain: string): string {
		if (name.includes('@')) {
			return name;
		}

		return name + '@' + domain;
	}

	private isDead(static_url: string | null | undefined): boolean {
		return !static_url || static_url === 'DEAD';
	}

	private handleDeadLink(static_url: string | null | undefined): string {
		if (this.isDead(static_url)) {
			return '/px.png';
		}

		if (!static_url!.includes('http')) {
			static_url = '/custom_emojis/static/' + static_url;
		}

		return static_url!;
	}

	async get(account: TootDeckAccount, emoji: Emoji) {
		let identifier = this.getIdentifier(emoji.name, account.mirrorAPI.domain);
		let static_url = this.cache.get(identifier);
		if (static_url) {
			return static_url;
		}

		static_url = emoji.static_url;

		if (!static_url) {
			static_url = (await TootDeckAPI.Worker.staticEmoji(identifier, emoji.url)) ?? undefined;
		}

		static_url = this.handleDeadLink(static_url);

		this.cache.set(identifier, static_url);

		return static_url;
	}
}

export const StaticEmojiCache = StaticEmojiCacheInner.instance;
