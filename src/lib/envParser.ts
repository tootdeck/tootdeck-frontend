export function envParser(env: string): string {
	if (env === undefined || env === '') {
		return '';
	}

	// Remove all escaped comments
	if (env.replaceAll(/\\#/g, '').includes('#')) {
		// Variable contains a comment
		return '';
	}

	return env;
}
