export function escapeHandler(e: KeyboardEvent, event_name: string = 'click') {
	const target = e.target as HTMLElement;

	if (e.code !== 'Escape' || !target) {
		return;
	}

	target.dispatchEvent(new Event(event_name));
}
