import type { TootDeckAccount } from '../stores/Account/Accounts';

import { ContentManager } from './Managers/ContentManager';
import { debugLogger } from './debugLogger';
import { clamp } from './Utils/number/clamp';

import type { ParsedStatus } from '../types/contentParsed';

type ID = string;
interface Poll {
	account: TootDeckAccount;
	status: ParsedStatus;
	refresh_count: number;
	args: { performance_mode: boolean; expired: boolean };
	timeout: NodeJS.Timeout;
}

class UpdatePollInner {
	static readonly instance = new UpdatePollInner();

	private polls = new Map<ID, Poll>();

	private async tick(id: string) {
		const poll = this.polls.get(id);
		if (!poll) {
			return;
		}

		if (poll.args.performance_mode || poll.args.expired) {
			return;
		}

		const response = await poll.account.mirrorAPI.Statuses.Poll.get(poll.status.poll!.id);
		if (response) {
			poll.status.poll = response;
			ContentManager.Status.forceUpdate(poll.account.mirrorAPI.handle, poll.status.id);
		}

		poll.timeout = setTimeout(() => this.tick(id), clamp(poll.refresh_count++, 1, 15) * 60000);
	}

	add(
		account: TootDeckAccount,
		status: ParsedStatus,
		args: { performance_mode: boolean; expired: boolean }
	) {
		const poll = this.polls.get(status.id);
		if (poll) {
			return;
		}

		this.polls.set(status.id, {
			account,
			status,
			refresh_count: 1,
			args,
			timeout: setTimeout(() => this.tick(status.id), 1 * 60000),
		});

		debugLogger(
			`UpdatePoll: add: Added ${status.id}, Currently updating ${this.polls.size} polls`
		);
	}

	remove(id: string) {
		const poll = this.polls.get(id);
		if (!poll) {
			debugLogger(`UpdatePoll: Currently updating ${this.polls.size} polls`);
			return;
		}

		this.polls.delete(id);

		debugLogger(
			`UpdatePoll: remove: Removed ${id}, currently updating ${this.polls.size} polls`
		);
	}
}

export const UpdatePoll = UpdatePollInner.instance;
