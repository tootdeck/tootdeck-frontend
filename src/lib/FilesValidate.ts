import { NotificationModalType, notificationModal } from '../stores/NotificationsModal';

import { Entity } from '../types/mastodonEntities';

export function validateFiles(instance: Entity.Instance | undefined, files: Array<File>) {
	if (!files || !files.length) {
		return false;
	}

	if (files.length > 4) {
		notificationModal.create(
			NotificationModalType.Warn,
			"You can't attach more than 4 media.",
			true
		);
		return false;
	}

	if (!instance) {
		return true;
	}

	const mime_types = instance.configuration.media_attachments?.supported_mime_types;
	if (mime_types) {
		for (const file of files) {
			if (!mime_types.includes(file.type)) {
				notificationModal.create(
					NotificationModalType.Warn,
					`Unsupported media type for file "${file.name}".`,
					true
				);
				return false;
			}
		}
	}

	const max_image_size = instance.configuration.media_attachments?.image_size_limit;
	const max_video_size = instance.configuration.media_attachments?.video_size_limit;
	if (max_image_size && max_video_size) {
		for (const file of files) {
			let error_msg = '';
			if (file.type === Entity.AttachmentType.Image) {
				if (file.size > max_image_size) {
					error_msg = `"${file.name}" is bigger than ${(
						max_image_size /
						1024 /
						1024
					).toFixed(0)} MB.`;
				}
			} else {
				if (file.size > max_video_size) {
					error_msg = `"${file.name}" is bigger than ${(
						max_video_size /
						1024 /
						1024
					).toFixed(0)} MB.`;
				}
			}

			if (error_msg) {
				notificationModal.create(NotificationModalType.Warn, error_msg, true);
				return false;
			}
		}
	}

	return true;
}
