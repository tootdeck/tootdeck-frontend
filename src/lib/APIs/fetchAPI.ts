import { logout, stLogged } from '../../stores/Account/Auth';

import { AppInit } from '../../AppInit';
import { TootDeckAPI } from './TootDeckAPI/TootDeckAPI';
import { FetchError, formatFetchError, type CatchedError } from './Error';
import { RequestsManager } from '../Managers/RequestsManager';
import { errorModal } from '../errorModal';

import type { Entity } from '../../types/mastodonEntities';

export function formatQuery(query: object | undefined) {
	if (!query) {
		return '';
	}

	const arr = Object.entries(query)
		.map(([key, value]) => {
			if (value === undefined) {
				return '';
			}

			if (Array.isArray(value)) {
				return value.map((x) => `${key}[]=${encodeURIComponent(x)}`).join('&');
			}

			return `${key}=${encodeURIComponent(value)}`;
		})
		.filter((x) => x);

	if (!arr.length) {
		return '';
	}

	return '?' + arr.join('&');
}

function getHeaders(headers: Headers) {
	const headers_str: { [key: string]: string } = {};

	for (const str of headers) {
		const [key, value] = str;
		headers_str[key] = value;
	}

	return headers_str;
}

export const NormalMastodonResponseCodes = [200, 201, 202, 206];

export interface FetchAPIOptionalParams {
	query?: object;
	body?: object;
	header?: object;
	hide_error?: boolean;
	is_refresh?: boolean;
	is_disconnect?: boolean;
	disable_cache?: boolean;
	/**
	 * In milliseconds
	 */
	cache_time?: number;
	mirrorAPI?: boolean;
	retrying?: boolean;
}

export type APIResponse<T> = { code: number; data: T };
export type MirrorEndpoint = `/api/v1/${string}` | `/api/v2/${string}`;

let logged: boolean = false;
AppInit.Promise.then(() => {
	stLogged.subscribe((v) => (logged = v));
});

export async function getBody(r: Response) {
	const content_type = r.headers.get('content-type')!;
	const is_json = content_type?.includes('application/json');

	const body = is_json ? await r.clone().json().catch(console.error) : await r.clone().text();

	return { is_json, body };
}

async function checkTokenExpired<T>(
	method: string,
	endpoint: string,
	optional?: FetchAPIOptionalParams
): Promise<Entity.Response<T> | CatchedError | null | void> {
	const is_refresh = optional?.is_refresh ?? false;
	const is_disconnect = optional?.is_disconnect ?? false;
	const retrying = optional?.retrying ?? false;

	if (retrying || (!logged && is_refresh) || is_disconnect) {
		return null;
	}

	if (is_refresh) {
		return logout(true);
	}

	const refresh = await TootDeckAPI.Auth.refresh();
	if (!refresh) {
		// If refresh fail the user will be disconnected, nothing more will be executed
		return;
	}

	RequestsManager.cancel(`${method}:${endpoint}:` + (JSON.stringify(optional) ?? ''));

	return fetchAPI(method, endpoint, { ...(optional ?? {}), retrying: true });
}

// prettier-ignore
export async function fetchAPI<T>(method: string, endpoint: MirrorEndpoint, optional?: FetchAPIOptionalParams): Promise<Entity.Response<T> | null>;
// prettier-ignore
export async function fetchAPI<T>(method: string, endpoint: string): Promise<Entity.Response<T> | null>;
// prettier-ignore
export async function fetchAPI<T>(method: string, endpoint: string, optional: FetchAPIOptionalParams): Promise<Entity.Response<T> | null>;
// prettier-ignore
export async function fetchAPI<T>(method: string, endpoint: string, optional?: FetchAPIOptionalParams): Promise<Entity.Response<T> | Entity.Response<T> | null> {
	if (optional?.disable_cache) {
		return wrapper<T>(method, endpoint, optional);
	}

	return RequestsManager.await(`${method}:${endpoint}:` + (JSON.stringify(optional) ?? ''), () =>
		wrapper<T>(method, endpoint, optional), optional?.cache_time
	);
}

// prettier-ignore
async function wrapper<T>(method: string, endpoint: string, optional?: FetchAPIOptionalParams): Promise<Entity.Response<T> | null> {
	const query = formatQuery(optional?.query);

	let headers: HeadersInit | undefined = optional?.body
		? {
				'Content-Type': 'application/json',
			}
		: {};
	if (optional?.header) {
		headers = { ...headers, ...optional?.header };
	}

	const body = JSON.stringify(optional?.body);

	return fetch(endpoint + query, {
		method,
		headers,
		body,
	})
		.then(async (r) => {
			const { is_json, body } = await getBody(r);

			// Token expired
			if (r.status === 401 && !body.mirror) {
				return checkTokenExpired(method, endpoint, optional);
			}

			if (
				optional?.mirrorAPI && (!is_json || !NormalMastodonResponseCodes.includes(r.status))
			) {
				return formatFetchError(r, body);
			}

			if (optional?.mirrorAPI) {
				return body;
			}

			const headers = getHeaders(r.headers)
			if (r.redirected && r.url) {
				headers['redirect'] = r.url
			}

			return {
				status: r.status,
				statusText: r.statusText,
				headers,
				data: body,
			};
		})
		.catch( (e: Response | FetchError) => {
			if (e instanceof FetchError) {
				return e
			}

			if (e.status === 502) {
				errorModal('Server unreachable.', [e], true);
			} else {
				errorModal('Something went wrong.', [e], true);
			}

			return null;
		});
}
