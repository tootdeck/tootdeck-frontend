// NestJS Error
export interface NestJSError {
	statusCode: number;
	message: string;
}

// Axios Error
export interface AxiosError {
	statusCode: number;
	message: string;
	mirror: true;
	detail: any;
}

// Formated Error
export interface CatchedErrorData {
	mirror: boolean;
	message: string;
	detail?: any;
	rateLimitReset?: Date;
	error?: Response;
}

export interface CatchedError {
	code: number;
	data: CatchedErrorData;
}

export class FetchError {
	code: number;
	data: CatchedErrorData;

	constructor(args: CatchedError) {
		this.code = args.code;
		this.data = args.data;
	}
}

export function formatFetchError(
	error: Response,
	body: NestJSError | AxiosError | string
): CatchedError | null {
	const rateLimitReset = error.headers.get('x-ratelimit-reset');

	if (typeof body === 'string') {
		throw new FetchError({
			code: error.status,
			data: {
				mirror: false,
				message: body,
				error,
			},
		});
	}

	throw new FetchError({
		code: error.status,
		data: {
			mirror: !!(body as any)?.mirror,
			message: body.message,
			detail: (body as any)?.detail,
			rateLimitReset: rateLimitReset ? new Date(rateLimitReset) : undefined,
			error,
		},
	});
}
