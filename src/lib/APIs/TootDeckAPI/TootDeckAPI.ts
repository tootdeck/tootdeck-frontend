import { User } from './Methods/User';
import { Auth } from './Methods/Auth/Auth';
import { OAuth } from './Methods/Auth/OAuth';
import { Sessions } from './Methods/Auth/Sessions';
import { Others } from './Methods/Others';
import { Dashboard } from './Methods/Dashboard';
import { Worker } from './Methods/Worker';

class _TootDeckAPI {
	readonly Auth = new Auth();
	readonly OAuth = new OAuth();
	readonly Others = new Others();
	readonly Sessions = new Sessions();
	readonly User = new User();
	readonly Dashboard = new Dashboard();
	readonly Worker = new Worker();
}

export const TootDeckAPI = new _TootDeckAPI();
