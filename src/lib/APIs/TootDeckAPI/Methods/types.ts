export interface ApiResponseTime {
	count: number;
	average: number;
	last: number;
}

export interface CachedAvatarResponse {
	avatar: string;
	avatar_static: string;
}
