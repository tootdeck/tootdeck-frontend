import { fetchAPI } from '../../../fetchAPI';
import type { FetchError } from '../../../Error';

import type { AuthGetResponse } from './types';

export class Auth {
	/**
	 * GET /api/auth/me
	 *
	 * Check if user is logged and return associated accounts
	 *
	 * @returns AuthGetResponse
	 */
	async me(): Promise<AuthGetResponse | null> {
		return fetchAPI<AuthGetResponse>('GET', '/api/auth/me')
			.catch((e: FetchError) => null)
			.then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/auth/refresh
	 *
	 * Refresh auth tokens
	 *
	 * @returns boolean
	 */
	async refresh(): Promise<boolean> {
		return fetchAPI('POST', '/api/auth/refresh', {
			is_refresh: true,
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (result === null) {
					return false;
				}

				return true;
			});
	}

	/**
	 * DELETE /api/auth/disconnect
	 *
	 * Revoke auth tokens
	 *
	 * @returns boolean
	 */
	async disconnect(): Promise<boolean> {
		return fetchAPI('DELETE', '/api/auth/disconnect', {
			is_disconnect: true,
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (result === null) {
					return false;
				}

				return true;
			});
	}
}
