import { fetchAPI } from '../../../fetchAPI';
import { errorModal } from '../../../../errorModal';
import type { FetchError } from '../../../Error';

export class OAuth {
	/**
	 * GET /api/oauth/authorize
	 *
	 * Validate handle and generate a OAuth2 redirect URI
	 *
	 * @param domain string
	 * @returns string
	 */
	async authorize(domain: string): Promise<string | null> {
		return fetchAPI<string>('GET', '/api/oauth/authorize', {
			query: { domain },
		})
			.catch((e: FetchError) => {
				switch (e.code) {
					case 503:
						errorModal('Unsupported instance type.', undefined, true);
					case 400:
						errorModal(`${e.code} ${(e.data as any).message}`, undefined, true);
				}

				return null;
			})
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * GET /api/oauth/authorize/add
	 *
	 * Validate handle and generate a OAuth2 redirect URI to link a new account to this user
	 *
	 * @param domain string
	 * @returns string
	 */
	async addAccount(domain: string): Promise<string | null> {
		return fetchAPI<string>('GET', '/api/oauth/authorize/add', {
			query: { domain },
		})
			.catch((e: FetchError) => {
				switch (e.code) {
					case 503:
						errorModal('Unsupported instance type.', undefined, true);
					case 400:
						errorModal(`${e.code} ${(e.data as any).message}`, undefined, true);
				}

				return null;
			})
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * GET /api/oauth/refresh
	 *
	 * Validate handle and generate a OAuth2 redirect URI to refresh token
	 *
	 * @param handle string
	 * @returns string
	 */
	async refresh(handle: string): Promise<string | null> {
		return fetchAPI<string>('GET', '/api/oauth/authorize/refresh', {
			query: { handle },
		})
			.catch((e: FetchError) => {
				switch (e.code) {
					case 503:
						errorModal('Unsupported instance type.', undefined, true);
					case 400:
						errorModal(`${e.code} ${(e.data as any).message}`, undefined, true);
				}

				return null;
			})
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}
}
