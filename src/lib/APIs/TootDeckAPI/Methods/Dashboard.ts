import { fetchAPI } from '../../fetchAPI';
import type { FetchError } from '../../Error';

import type { RemoteInteractions } from '../../../../types/remoteInteractions';
import type { Dashboard as NDashboard } from '../../../../types/dashboard';

class Redis {
	/**
	 * GET /api/dashboard/redis/interactions
	 *
	 * Get all cached interactions
	 *
	 * @returns `Array<RemoteInteractions>`
	 */
	async getInteractions(): Promise<Array<RemoteInteractions> | null> {
		return fetchAPI<Array<RemoteInteractions>>('GET', '/api/dashboard/redis/interactions')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * DELETE /api/dashboard/redis/interactions
	 *
	 * Delete cached interactions
	 *
	 * @returns `Array<DashboardExcludedDomains>`
	 */
	async delInteraction(keys: Array<string>): Promise<boolean> {
		return fetchAPI<void>('DELETE', '/api/dashboard/redis/interactions', {
			body: {
				keys,
			},
		})
			.catch((e: FetchError) => null)
			.then((result) => result?.status === 200 || result?.status === 404);
	}

	/**
	 * PUT /api/dashboard/redis/interactions
	 *
	 * Delete Force remove cached interactions from excluded domain
	 *
	 * @returns `number`
	 */
	async cleanInteraction(): Promise<number | null> {
		return fetchAPI<number>('PUT', '/api/dashboard/redis/interactions/clean')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return +result.data;
			});
	}

	/**
	 * GET /api/dashboard/redis/excluded_domains
	 *
	 * Get all excluded domain
	 *
	 * @returns `Array<Dashboard.Redis.ExcludedDomains>`
	 */
	async getExcludedDomains(): Promise<Array<NDashboard.Redis.ExcludedDomains> | null> {
		return fetchAPI<Array<NDashboard.Redis.ExcludedDomains>>(
			'GET',
			'/api/dashboard/redis/excluded_domains'
		)
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * DELETE /api/dashboard/redis/excluded_domains
	 *
	 * Delete excluded domains
	 *
	 * @returns `Array<DashboardExcludedDomains>`
	 */
	async delExcludedDomain(keys: Array<string>): Promise<boolean> {
		return fetchAPI<void>('DELETE', '/api/dashboard/redis/excluded_domains', {
			body: {
				keys,
			},
		})
			.catch((e: FetchError) => null)
			.then((result) => result?.status === 200 || result?.status === 404);
	}

	/**
	 * GET /api/dashboard/redis/instances
	 *
	 * Get all cached instances
	 *
	 * @returns `Array<Dashboard.Redis.Instances>`
	 */
	async getInstances(): Promise<Array<NDashboard.Redis.Instances> | null> {
		return fetchAPI<Array<NDashboard.Redis.Instances>>('GET', '/api/dashboard/redis/instances')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * DELETE /api/dashboard/redis/instances
	 *
	 * Delete cached instances
	 *
	 * @returns `Array<DashboardExcludedDomains>`
	 */
	async delInstance(keys: Array<string>): Promise<boolean> {
		return fetchAPI<void>('DELETE', '/api/dashboard/redis/instances', {
			body: {
				keys,
			},
		})
			.catch((e: FetchError) => null)
			.then((result) => result?.status === 200 || result?.status === 404);
	}

	/**
	 * GET /api/dashboard/redis/twitter
	 *
	 * Get all cached twitter embed
	 *
	 * @returns `Array<Dashboard.Redis.TwitterEmbed>`
	 */
	async getTwitterEmbed(): Promise<Array<NDashboard.Redis.TwitterEmbed> | null> {
		return fetchAPI<Array<NDashboard.Redis.TwitterEmbed>>('GET', '/api/dashboard/redis/twitter')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * DELETE /api/dashboard/redis/twitter
	 *
	 * Delete twitter embed
	 *
	 * @returns `Array<DashboardExcludedDomains>`
	 */
	async delTwitterEmbed(keys: Array<string>): Promise<boolean> {
		return fetchAPI<void>('DELETE', '/api/dashboard/redis/twitter', {
			body: {
				keys,
			},
		})
			.catch((e: FetchError) => null)
			.then((result) => result?.status === 200 || result?.status === 404);
	}
}

export class Dashboard {
	public Redis = new Redis();

	/**
	 * GET /api/dashboard/stats
	 *
	 * Get data for the dashboard
	 *
	 * @returns `Dashboard.Data`
	 */
	async data(): Promise<NDashboard.Data | null> {
		return fetchAPI<NDashboard.Data>('GET', '/api/dashboard/stats')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * GET /api/dashboard/charts
	 *
	 * Get data for the dashboard charts
	 *
	 * @returns `Array<Dashboard.ChartsData>`
	 */
	async charts(limit: number): Promise<Array<NDashboard.ChartsData> | null> {
		return fetchAPI<Array<NDashboard.ChartsData>>('GET', '/api/dashboard/charts', {
			query: {
				limit,
			},
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * GET /api/dashboard/users_stats
	 *
	 * Get corrently connected users
	 *
	 * @returns `Array<DashboardConnectedUsers>`
	 */
	async usersStats(): Promise<NDashboard.Stats.Users | null> {
		return fetchAPI<NDashboard.Stats.Users>('GET', '/api/dashboard/users_stats')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * POST /api/dashboard/broadcast_message
	 *
	 * Broadcast a message to all connected clients
	 *
	 * @returns `true` on success
	 */
	async broadcastMessage(message: string): Promise<boolean> {
		return fetchAPI<void>('POST', '/api/dashboard/broadcast_message', { body: { message } })
			.catch((e: FetchError) => null)
			.then((result) => (result === null ? false : true));
	}
}
