import type { Entity } from '../../../types/mastodonEntities';

export interface SearchAccountOptional {
	following?: boolean;
	resolve?: boolean;
	limit?: number;
	/**
	 * All results returned will be lesser than this ID. In effect, sets an upper bound on results
	 */
	max_id?: string;
	/**
	 * All results returned will be greater than this ID. In effect, sets a lower bound on results
	 */
	since_id?: string;
}

export interface OptionalFull {
	limit?: number;
	/**
	 * All results returned will be lesser than this ID. In effect, sets an upper bound on results
	 */
	max_id?: string;
	/**
	 * All results returned will be greater than this ID. In effect, sets a lower bound on results
	 */
	since_id?: string;
	/**
	 * Returns results immediately newer than this ID. In effect, sets a cursor at this ID and paginates forward.
	 */
	min_id?: string;
}

export interface OptionalMinMax {
	limit?: number;
	/**
	 * All results returned will be lesser than this ID. In effect, sets an upper bound on results
	 */
	max_id?: string;
	/**
	 * Returns results immediately newer than this ID. In effect, sets a cursor at this ID and paginates forward.
	 */
	min_id?: string;
}

export interface OptionalSince {
	limit?: number;
	/**
	 * All results returned will be lesser than this ID. In effect, sets an upper bound on results
	 */
	max_id?: string;
	/**
	 * All results returned will be greater than this ID. In effect, sets a lower bound on results
	 */
	since_id?: string;
}

export interface TagTimelineOptional extends OptionalFull {
	local?: boolean;
	only_media?: boolean;
}

export interface NotificationsOptional extends OptionalFull {
	types?: Array<Entity.NotificationType>;
	exclude_types?: Array<Entity.NotificationType>;
	account_id?: string;
}

export interface DirectoryOptional {
	limit?: number;
	offset?: number;
	order?: 'active' | 'new';
	local?: boolean;
}

export interface MakerSaveOptional {
	home?: { last_read_id: string };
	notifications?: { last_read_id: string };
}

export interface FilterOptional {
	irreversible?: boolean;
	whole_word?: boolean;
	expires_in?: string;
}

export interface AccountFollowOptional {
	limit?: number;
	/**
	 * All results returned will be lesser than this ID. In effect, sets an upper bound on results
	 */
	max_id?: string;
	since_id?: string;
	get_all?: boolean;
	sleep_ms?: number;
}

export interface AccountStatusesOptional extends OptionalFull {
	pinned?: boolean;
	exclude_replies?: boolean;
	exclude_reblogs?: boolean;
	only_media?: boolean;
}

export interface TimelineOptional extends OptionalFull {
	only_media?: boolean;
}

export interface HomeTimelineOptional extends OptionalFull {
	local?: boolean;
}

export interface StatusOptionalPoll {
	options: Array<string>;
	expires_in: number;
	multiple?: boolean;
	hide_totals?: boolean;
}

export interface StatusOptinal {
	sensitive?: boolean;
	spoiler_text?: string;
	visibility?: Entity.Visibility;
	scheduled_at?: string;
	language?: string;
	in_reply_to_id?: string;
	quote_id?: string;
	media_ids?: Array<string>;
	poll?: StatusOptionalPoll;
}

export interface ReportOptional {
	status_ids?: Array<string>;
	comment: string;
	forward?: boolean;
	category?: Entity.ReportCategory;
	rule_ids?: Array<number>;
}

export interface AttachmentAttributes {
	id: string;
	description?: string;
	focus?: string;
}

export interface EditStatusOptinal {
	status?: string;
	spoiler_text?: string;
	sensitive?: boolean;
	language?: string;
	media_attributes?: Array<AttachmentAttributes>;
	media_ids?: Array<string>;
	poll?: {
		options?: Array<string>;
		expires_in?: number;
		multiple?: boolean;
		hide_totals?: boolean;
	};
}

export interface MediaUploadOptional {
	description?: string;
	focus?: string;
}

export interface MediaUpdateOptional extends MediaUploadOptional {
	file?: any;
	is_sensitive?: boolean;
}

export interface NotificationReadOptional {
	id?: string;
	/**
	 * All results returned will be lesser than this ID. In effect, sets an upper bound on results
	 */
	max_id?: string;
}

export interface SearchOptional extends OptionalMinMax {
	resolve?: boolean;
	offset?: number;
	following?: boolean;
	account_id?: string;
	exclude_unreviewed?: boolean;
}

export interface FollowOptional {
	reblog?: boolean;
}
