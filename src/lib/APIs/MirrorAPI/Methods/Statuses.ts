import { parseResponseLink, type ResponseLinks } from '../../../Utils/parseResponseLInk';
import { MirrorInterface } from '../Interface';

import type {
	EditStatusOptinal,
	StatusOptinal,
	MediaUploadOptional,
	OptionalSince,
} from '../types';
import type { Entity } from '../../../../types/mastodonEntities';

class Scheduled extends MirrorInterface {
	/**
	 * GET /api/v1/scheduled_statuses
	 * https://docs.joinmastodon.org/methods/scheduled_statuses/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.ScheduledStatus>`
	 */
	async getAll(): Promise<Array<Entity.ScheduledStatus> | null> {
		return this.fetchAPI<Array<Entity.ScheduledStatus>>(
			'GET',
			`/api/v1/scheduled_statuses`,
			{}
		).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/scheduled_statuses/:id
	 * https://docs.joinmastodon.org/methods/scheduled_statuses/#get-one
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Entity.ScheduledStatus`
	 */
	async getOne(id: string): Promise<Entity.ScheduledStatus | null> {
		return this.fetchAPI<Entity.ScheduledStatus>(
			'GET',
			`/api/v1/scheduled_statuses/${id}`,
			{}
		).then((r) => r?.data ?? null);
	}

	/**
	 * DELETE /api/v1/scheduled_statuses/:id
	 * https://docs.joinmastodon.org/methods/scheduled_statuses/#cancel
	 *
	 * @param `id`
	 * @returns `{}`
	 */
	async cancel(id: string): Promise<boolean> {
		return this.fetchAPI<{}>('DELETE', `/api/v1/scheduled_statuses/${id}`, {}).then((r) =>
			r ? true : false
		);
	}
}

class Poll extends MirrorInterface {
	/**
	 * GET /api/v1/polls/:id
	 * https://docs.joinmastodon.org/methods/polls/#get
	 *
	 * @param `id`
	 * @returns `Entity.Poll`
	 */
	async get(id: string): Promise<Entity.Poll | null> {
		return this.fetchAPI<Entity.Poll>('GET', `/api/v1/polls/${id}`, { hide_error: true }).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/polls/:id/votes
	 * https://docs.joinmastodon.org/methods/polls/#vote
	 *
	 * @param `id`
	 * @body `Array<choices>`
	 * @returns `Entity.Poll`
	 */
	async vote(id: string, choices: Array<number>): Promise<Entity.Poll | null> {
		return this.fetchAPI<Entity.Poll>('POST', `/api/v1/polls/${id}/votes`, {
			body: { choices },
		}).then((r) => r?.data ?? null);
	}
}

class Media extends MirrorInterface {
	/**
	 * POST /api/v2/media
	 * https://docs.joinmastodon.org/methods/media/#v2
	 *
	 * @body `FileBody`
	 * @file 'file
	 * @returns `Entity.Attachment`
	 * @returns `Entity.AsyncAttachment`
	 */
	async upload(
		file: File,
		options?: {
			media: MediaUploadOptional;
			onupload?: (ev: ProgressEvent<EventTarget>) => void;
		}
	): Promise<Entity.Attachment | Entity.AsyncAttachment | null> {
		return this.uploadAPI<Entity.Attachment | Entity.AsyncAttachment>(
			'POST',
			`/api/v2/media`,
			file,
			{
				body: options?.media,
				onupload: options?.onupload,
			}
		).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/media/:id
	 * https://docs.joinmastodon.org/methods/media/#get
	 *
	 * @param `id`
	 * @returns `Entity.Attachment`
	 */
	async get(id: string): Promise<Entity.Attachment | null> {
		return this.fetchAPI<Entity.Attachment>('GET', `/api/v1/media/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * PUT /api/v1/media/:id
	 * https://docs.joinmastodon.org/methods/media/#update
	 *
	 * @param `id`
	 * @body `FormBody.UpdateMedia`
	 * @returns `Entity.Attachment`
	 */
	async edit(id: string, options?: MediaUploadOptional): Promise<Entity.Attachment | null> {
		return this.fetchAPI<Entity.Attachment>('PUT', `/api/v1/media/${id}`, {
			body: options,
		}).then((r) => r?.data ?? null);
	}
}

class Reaction extends MirrorInterface {
	async create(id: string, name: string) {
		return this.fetchAPI<Entity.Status>(
			'POST',
			`/api/v1/statuses/${id}/react/${encodeURI(name)}`,
			{}
		).then((r) => r?.data ?? null);
	}

	async remove(id: string, name: string) {
		return this.fetchAPI<Entity.Status>(
			'POST',
			`/api/v1/statuses/${id}/unreact/${encodeURI(name)}`,
			{}
		).then((r) => r?.data ?? null);
	}
}

export class Statuses extends MirrorInterface {
	readonly Scheduled = new Scheduled(this._handle, this._instance_type);
	readonly Poll = new Poll(this._handle, this._instance_type);
	readonly Media = new Media(this._handle, this._instance_type);
	readonly Reaction = new Reaction(this._handle, this._instance_type);

	/**
	 * GET /api/v1/statuses/:id
	 * https://docs.joinmastodon.org/methods/statuses/#get
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async get(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('GET', `/api/v1/statuses/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/statuses/:id/context
	 * https://docs.joinmastodon.org/methods/statuses/#context
	 *
	 * @param `id`
	 * @returns `Entity.Context`
	 */
	async getContext(id: string): Promise<Entity.Context | null> {
		return this.fetchAPI<Entity.Context>('GET', `/api/v1/statuses/${id}/context`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/statuses/:id/reblogged_by
	 * https://docs.joinmastodon.org/methods/statuses/#reblogged_by
	 *
	 * @param `id`
	 * @returns `Array<Entity.Account>`
	 */
	async getRebloggedBy(
		id: string,
		options?: OptionalSince
	): Promise<ResponseLinks<Array<Entity.Account>> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/statuses/${id}/reblogged_by`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/statuses/:id/favourited_by
	 * https://docs.joinmastodon.org/methods/statuses/#favourited_by
	 *
	 * @param `id`
	 * @returns `Array<Entity.Account>`
	 */
	async getFavouritedBy(
		id: string,
		options?: OptionalSince
	): Promise<ResponseLinks<Array<Entity.Account>> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/statuses/${id}/favourited_by`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/statuses/:id/source
	 * https://docs.joinmastodon.org/methods/statuses/#source
	 *
	 * @param `id`
	 * @returns `Entity.StatusSource`
	 */
	async getSource(id: string) {
		return this.fetchAPI<Entity.StatusSource>('GET', `/api/v1/statuses/${id}/source`, {}).then(
			(r) => r?.data ?? null
		);
	}

	async getHistory(id: string) {
		return this.fetchAPI<Array<Entity.StatusEdit>>(
			'GET',
			`/api/v1/statuses/${id}/history`,
			{}
		).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/statuses/:id/favourite
	 * https://docs.joinmastodon.org/methods/statuses/#favourite
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async favourite(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/favourite`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/unfavourite
	 * https://docs.joinmastodon.org/methods/statuses/#unfavourite
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async unfavourite(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/unfavourite`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/reblog
	 * https://docs.joinmastodon.org/methods/statuses/#reblog
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async reblog(
		id: string,
		visibility?: Exclude<Entity.Visibility, Entity.Visibility.Direct>
	): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/reblog`, {
			body: { visibility },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/statuses/:id/unreblog
	 * https://docs.joinmastodon.org/methods/statuses/#unreblog
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async unreblog(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/unreblog`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/bookmark
	 * https://docs.joinmastodon.org/methods/statuses/#bookmark
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async bookmark(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/bookmark`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/unbookmark
	 * https://docs.joinmastodon.org/methods/statuses/#unbookmark
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async unbookmark(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/unbookmark`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/mute
	 * https://docs.joinmastodon.org/methods/statuses/#mute
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async mute(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/mute`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/unmute
	 * https://docs.joinmastodon.org/methods/statuses/#unmute
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async unmute(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/unmute`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/pin
	 * https://docs.joinmastodon.org/methods/statuses/#pin
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async pin(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/pin`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses/:id/unpin
	 * https://docs.joinmastodon.org/methods/statuses/#unpin
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async unpin(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('POST', `/api/v1/statuses/${id}/unpin`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/statuses
	 * https://docs.joinmastodon.org/methods/statuses/#create
	 *
	 * @body `FormBody.Status`
	 * @returns `Entity.Status`
	 * @returns `Entity.ScheduledStatus`
	 */
	async create(
		text: string,
		options?: StatusOptinal
	): Promise<Entity.Status | Entity.ScheduledStatus | null> {
		return this.fetchAPI<Entity.Status | Entity.ScheduledStatus>('POST', `/api/v1/statuses`, {
			body: { status: text, ...options },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * DELETE /api/v1/statuses/:id
	 * https://docs.joinmastodon.org/methods/statuses/#delete
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	async delete(id: string): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('DELETE', `/api/v1/statuses/${id}`, {
			hide_error: true,
		}).then((r) => {
			if (!r) {
				return null;
			}

			return r.data;
		});
	}

	/**
	 * PUT /api/v1/statuses/:id
	 * https://docs.joinmastodon.org/methods/statuses/#edit
	 *
	 * @param `id`
	 * @body `FormBody.Status`
	 * @returns `Entity.Status`
	 */
	async edit(id: string, options: EditStatusOptinal): Promise<Entity.Status | null> {
		return this.fetchAPI<Entity.Status>('PUT', `/api/v1/statuses/${id}`, {
			body: options,
		}).then((r) => r?.data ?? null);
	}
}
