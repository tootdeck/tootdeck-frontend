import { MirrorInterface } from '../Interface';

import type { DirectoryOptional, OptionalMinMax } from '../types';
import type { Entity } from '../../../../types/mastodonEntities';

class Announcements extends MirrorInterface {
	/**
	 * GET /api/v1/announcements
	 * https://docs.joinmastodon.org/methods/announcements/#get
	 *
	 * @returns `Array<Entity.Announcement>`
	 */
	async get(): Promise<Array<Entity.Announcement> | null> {
		return this.fetchAPI<Array<Entity.Announcement>>('GET', `/api/v1/announcements`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * PUT /api/v1/announcements/:id/reactions/:name
	 * https://docs.joinmastodon.org/methods/announcements/#put-reactions
	 *
	 * @param `id`
	 * @param `name`
	 * @returns `{}`
	 */
	async createEmojiReaction(id: string, emoji: string): Promise<{} | null> {
		return this.fetchAPI<{}>('PUT', `/api/v1/announcements/${id}/reactions/${emoji}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * DELETE /api/v1/announcements/:id/reactions/:name
	 * https://docs.joinmastodon.org/methods/announcements/#delete-reactions
	 *
	 * @param `id`
	 * @param `name`
	 * @returns `{}`
	 */
	async deleteEmojiReaction(id: string, emoji: string): Promise<{} | null> {
		return this.fetchAPI<{}>(
			'DELETE',
			`/api/v1/announcements/${id}/reactions/${emoji}`,
			{}
		).then((r) => r?.data ?? null);
	}
}

class Directory extends MirrorInterface {
	/**
	 * GET /api/v1/directory
	 * https://docs.joinmastodon.org/methods/directory/#get
	 *
	 * @query `limit`
	 * @query `offset`
	 * @query `order`
	 * @query `local`
	 * @returns `Array<Entity.Account>`
	 */
	async get(options?: DirectoryOptional): Promise<Array<Entity.Account> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/directory`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}
}

class Emojis extends MirrorInterface {
	/**
	 * GET /api/v1/custom_emojis
	 * https://docs.joinmastodon.org/methods/custom_emojis/#get
	 *
	 * @returns `Array<Entity.Emoji>`
	 */
	async get(): Promise<Array<Entity.Emoji> | null> {
		return this.fetchAPI<Array<Entity.Emoji>>('GET', `/api/v1/custom_emojis`, {}).then(
			(r) => r?.data ?? null
		);
	}
}

export class Instance extends MirrorInterface {
	readonly Announcements = new Announcements(this._handle, this._instance_type);
	readonly Directory = new Directory(this._handle, this._instance_type);
	readonly Emojis = new Emojis(this._handle, this._instance_type);

	/**
	 * GET /api/v1/instance
	 * https://docs.joinmastodon.org/methods/instance/#v1
	 *
	 * @returns `Entity.Instance`
	 */
	async get(): Promise<Entity.Instance | null> {
		return this.fetchAPI<Entity.Instance>('GET', `/api/v1/instance`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/instance/peers
	 * https://docs.joinmastodon.org/methods/instance/#peers
	 *
	 * @returns `Array<String>`
	 */
	async getPeers(): Promise<Array<string> | null> {
		return this.fetchAPI<Array<string>>('GET', `/api/v1/instance/peers`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/instance/activity
	 * https://docs.joinmastodon.org/methods/instance/#activity
	 *
	 * @returns `Entity.Activity`
	 */
	async getActivity(): Promise<Array<Entity.Activity> | null> {
		return this.fetchAPI<Array<Entity.Activity>>('GET', `/api/v1/instance/activity`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/instance/domain_blocks
	 * https://docs.joinmastodon.org/methods/instance/#domain_blocks
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<String>`
	 */
	async getDomainBlocks(options?: OptionalMinMax): Promise<Array<string> | null> {
		return this.fetchAPI<Array<string>>('GET', `/api/v1/instance/domain_blocks`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}
}
