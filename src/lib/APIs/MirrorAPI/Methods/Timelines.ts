import { MirrorInterface } from '../Interface';

import type {
	MakerSaveOptional,
	OptionalFull,
	TagTimelineOptional,
	HomeTimelineOptional,
	OptionalSince,
	TimelineOptional,
} from '../types';
import type { Entity } from '../../../../types/mastodonEntities';

class Conversations extends MirrorInterface {
	/**
	 * GET /api/v1/conversations
	 * https://docs.joinmastodon.org/methods/conversations/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Conversation>`
	 */
	async get(options?: OptionalSince): Promise<Array<Entity.Conversation> | null> {
		return this.fetchAPI<Array<Entity.Conversation>>('GET', `/api/v1/conversations`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * DELETE /api/v1/conversations/:id
	 * https://docs.joinmastodon.org/methods/conversations/#delete
	 *
	 * @params `id`
	 * @returns `{}`
	 */
	async delete(id: string): Promise<boolean> {
		return this.fetchAPI<{}>('DELETE', `/api/v1/conversations/${id}`, {}).then((r) =>
			r ? true : false
		);
	}
}

class Markers extends MirrorInterface {
	/**
	 * GET /api/v1/markers
	 * https://docs.joinmastodon.org/methods/markers/#get
	 *
	 * @query `timeline`
	 * @returns `Entity.Marker`
	 */
	async get(timeline: Array<'home' | 'notifications'>): Promise<Entity.Marker | null> {
		return this.fetchAPI<Entity.Marker>('GET', `/api/v1/markers`, {
			query: { timeline },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/markers
	 * https://docs.joinmastodon.org/methods/markers/#create
	 *
	 * @body `Body.Marker`
	 * @returns `Entity.Marker`
	 */
	async save(options?: MakerSaveOptional): Promise<Entity.Marker | null> {
		return this.fetchAPI<Entity.Marker>('POST', `/api/v1/markers`, {
			body: options,
		}).then((r) => r?.data ?? null);
	}
}

export class Lists extends MirrorInterface {
	/**
	 * GET /api/v1/lists
	 * https://docs.joinmastodon.org/methods/lists/#get
	 *
	 * @returns `Array<Entity.List>`
	 */
	async getAll(): Promise<Array<Entity.List> | null> {
		return this.fetchAPI<Array<Entity.List>>('GET', `/api/v1/lists`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/lists
	 * https://docs.joinmastodon.org/methods/lists/#get
	 *
	 * @returns `Entity.List`
	 */
	async getOne(id: string): Promise<Entity.List | null> {
		return this.fetchAPI<Entity.List>('GET', `/api/v1/lists/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/lists/:id/accounts
	 * https://docs.joinmastodon.org/methods/lists/#accounts
	 *
	 * @params `id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Entity.List`
	 */
	async getAccounts(id: string, options?: OptionalSince): Promise<Array<Entity.Account> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/lists/${id}/accounts`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/lists
	 * https://docs.joinmastodon.org/methods/lists/#create
	 *
	 * @params `id`
	 * @body `title`
	 * @returns `Entity.List`
	 */
	async create(title: string): Promise<Entity.List | null> {
		return this.fetchAPI<Entity.List>('POST', `/api/v1/lists`, {
			body: { title },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * PUT /api/v1/lists/:id
	 * https://docs.joinmastodon.org/methods/lists/#update
	 *
	 * @params id `string`
	 * @body replies_policy `Entity.RepliesPolicy`
	 * @body exclusive `boolean`
	 * @body title `string`
	 * @returns `boolean`
	 */
	async update(
		id: string,
		args: {
			title?: string;
			replies_policy?: Entity.RepliesPolicy;
			exclusive?: boolean;
		}
	): Promise<boolean> {
		if (args.exclusive === undefined && !args.replies_policy && !args.title) {
			return false;
		}

		return this.fetchAPI<Entity.List>('PUT', `/api/v1/lists/${id}`, {
			body: args,
		}).then((r) => (r ? true : false));
	}

	/**
	 * DELETE /api/v1/lists/:id
	 * https://docs.joinmastodon.org/methods/lists/#delete
	 *
	 * @params `id`
	 * @returns `{}`
	 */
	async delete(id: string): Promise<boolean> {
		return this.fetchAPI<{}>('DELETE', `/api/v1/lists/${id}`, {}).then((r) =>
			r ? true : false
		);
	}

	/**
	 * POST /api/v1/lists/:id/accounts
	 * https://docs.joinmastodon.org/methods/lists/#accounts-add
	 *
	 * @params `id`
	 * @body `account_ids`
	 * @returns `{}`
	 */
	async addAccounts(id: string, account_ids: Array<string>): Promise<boolean> {
		return this.fetchAPI<{}>('POST', `/api/v1/lists/${id}/accounts`, {
			body: { account_ids },
		}).then((r) => (r ? true : false));
	}

	/**
	 * DELETE /api/v1/lists/:id/accounts
	 * https://docs.joinmastodon.org/methods/lists/#accounts-remove
	 *
	 * @params `id`
	 * @body `account_ids`
	 * @returns `{}`
	 */
	async deleteAccounts(id: string, account_ids: Array<string>): Promise<boolean> {
		return this.fetchAPI<{}>('DELETE', `/api/v1/lists/${id}/accounts`, {
			body: { account_ids },
		}).then((r) => (r ? true : false));
	}
}

export class Timelines extends MirrorInterface {
	readonly Conversations = new Conversations(this._handle, this._instance_type);
	readonly Markers = new Markers(this._handle, this._instance_type);
	readonly Lists = new Lists(this._handle, this._instance_type);

	/**
	 * GET /api/v1/timelines/home
	 * https://docs.joinmastodon.org/methods/timelines/#home
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	async getHome(options?: HomeTimelineOptional): Promise<Array<Entity.Status> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/timelines/home`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/timelines/public
	 * https://docs.joinmastodon.org/methods/timelines/#public
	 *
	 * @query `remote`
	 * @query `only_media`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	async getPublic(options?: TimelineOptional): Promise<Array<Entity.Status> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/timelines/public`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/timelines/public
	 * https://docs.joinmastodon.org/methods/timelines/#public
	 *
	 * @query `only_media`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	async getLocal(options?: TimelineOptional): Promise<Array<Entity.Status> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/timelines/public`, {
			query: { local: true, ...options },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/timelines/tag/:hashtag
	 * https://docs.joinmastodon.org/methods/timelines/#tag
	 *
	 * @param `hashtag`
	 * @query `local`
	 * @query `only_media`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	async getTag(
		hashtag: string,
		options?: TagTimelineOptional
	): Promise<Array<Entity.Status> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/timelines/tag/${hashtag}`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/timelines/list/:list_id
	 * https://docs.joinmastodon.org/methods/timelines/#list
	 *
	 * @param `list_id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	async getList(list_id: string, options?: OptionalFull): Promise<Array<Entity.Status> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/timelines/list/${list_id}`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}
}
