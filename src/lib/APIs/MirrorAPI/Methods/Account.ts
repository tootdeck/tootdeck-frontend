import { MirrorInterface } from '../Interface';
import { parseResponseLink, type ResponseLinks } from '../../../Utils/parseResponseLInk';

import {
	type FilterOptional,
	type SearchAccountOptional,
	type OptionalFull,
	type OptionalMinMax,
	type ReportOptional,
	type AccountStatusesOptional,
	type AccountFollowOptional,
	type FollowOptional,
	type OptionalSince,
} from '../types';
import type { Entity } from '../../../../types/mastodonEntities';

class DomainBlocks extends MirrorInterface {
	/**
	 * GET /api/v1/domain_blocks
	 * https://docs.joinmastodon.org/methods/domain_blocks/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<String>`
	 */
	async get(options?: OptionalMinMax): Promise<ResponseLinks<Array<string>> | null> {
		return this.fetchAPI<Array<string>>('GET', `/api/v1/domain_blocks`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * POST /api/v1/domain_blocks
	 * https://docs.joinmastodon.org/methods/domain_blocks/#block
	 *
	 * @body `FormBody.DomainBlock`
	 * @returns `{}`
	 */
	async block(domain: string): Promise<boolean> {
		return this.fetchAPI<{}>('POST', `/api/v1/domain_blocks`, { body: { domain } }).then((r) =>
			r ? true : false
		);
	}

	/**
	 * DELETE /api/v1/domain_blocks
	 * https://docs.joinmastodon.org/methods/domain_blocks/#unblock
	 *
	 * @body `FormBody.DomainBlock`
	 * @returns `{}`
	 */
	async unblock(domain: string): Promise<boolean> {
		return this.fetchAPI<{}>('DELETE', `/api/v1/domain_blocks`, { body: { domain } }).then(
			(r) => (r ? true : false)
		);
	}
}

class Filters extends MirrorInterface {
	/**
	 * GET /api/v1/filters
	 * https://docs.joinmastodon.org/methods/filters/#get-v1
	 *
	 * @returns `Array<Entity.Filter>`
	 */
	async getAll(): Promise<Array<Entity.Filter> | null> {
		return this.fetchAPI<Array<Entity.Filter>>('GET', `/api/v1/filters`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/filters
	 * https://docs.joinmastodon.org/methods/filters/#get-v1
	 *
	 * @returns `Entity.Filter`
	 */
	async getOne(id: string): Promise<Entity.Filter | null> {
		return this.fetchAPI<Entity.Filter>('GET', `/api/v1/filters/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/filters
	 * https://docs.joinmastodon.org/methods/filters/#create-v1
	 *
	 * @body `FormBody.Filter`
	 * @returns `Entity.Filter`
	 */
	async create(
		phrase: string,
		context: Array<string>,
		options?: FilterOptional
	): Promise<Entity.Filter | null> {
		return this.fetchAPI<Entity.Filter>('POST', `/api/v1/filters`, {
			body: { phrase, context, ...options },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * PUT /api/v1/filters/:id
	 * https://docs.joinmastodon.org/methods/filters/#update-v1
	 *
	 * @param `id`
	 * @body `FormBody.Filter`
	 * @returns `Entity.Filter`
	 */
	async update(
		id: string,
		phrase: string,
		context: Array<string>,
		options?: FilterOptional
	): Promise<Entity.Filter | null> {
		return this.fetchAPI<Entity.Filter>('PUT', `/api/v1/filters/${id}`, {
			body: { phrase, context, ...options },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * DELETE /api/v1/filters/:id
	 * https://docs.joinmastodon.org/methods/filters/#delete-v1
	 *
	 * @param `id`
	 * @returns `Entity.Filter`
	 */
	async delete(id: string): Promise<Entity.Filter | null> {
		return this.fetchAPI<Entity.Filter>('DELETE', `/api/v1/filters/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}
}

class Tags extends MirrorInterface {
	/**
	 * GET /api/v1/tags/:id
	 * https://docs.joinmastodon.org/methods/tags/#get
	 *
	 * @param `id`
	 * @returns `Entity.Tag`
	 */
	async get(id: string): Promise<Entity.Tag | null> {
		return this.fetchAPI<Entity.Tag>('GET', `/api/v1/tags/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/tags/:id/follow
	 * https://docs.joinmastodon.org/methods/tags/#follow
	 *
	 * @param `id`
	 * @returns `Entity.Tag`
	 */
	async follow(id: string): Promise<Entity.Tag | null> {
		return this.fetchAPI<Entity.Tag>('POST', `/api/v1/tags/${id}/follow`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/tags/:id/unfollow
	 * https://docs.joinmastodon.org/methods/tags/#unfollow
	 *
	 * @param `id`
	 * @returns `Entity.Tag`
	 */
	async unfollow(id: string): Promise<Entity.Tag | null> {
		return this.fetchAPI<Entity.Tag>('POST', `/api/v1/tags/${id}/unfollow`, {}).then(
			(r) => r?.data ?? null
		);
	}
}

class FollowRequest extends MirrorInterface {
	/**
	 * GET /api/v1/follow_requests
	 * https://docs.joinmastodon.org/methods/follow_requests/#get
	 *
	 * @query `limit`
	 * @returns `Array<Entity.Account>`
	 */
	async get(limit?: number): Promise<Array<Entity.Account> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/follow_requests`, {
			query: { limit },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/follow_requests/:account_id/authorize
	 * https://docs.joinmastodon.org/methods/follow_requests/#accept
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async accept(account_id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>(
			'POST',
			`/api/v1/follow_requests/${account_id}/authorize`,
			{}
		).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/follow_requests/:account_id/reject
	 * https://docs.joinmastodon.org/methods/follow_requests/#reject
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async reject(account_id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>(
			'POST',
			`/api/v1/follow_requests/${account_id}/reject`,
			{}
		).then((r) => r?.data ?? null);
	}
}

export class Accounts extends MirrorInterface {
	readonly DomainBlocks = new DomainBlocks(this._handle, this._instance_type);
	readonly Filters = new Filters(this._handle, this._instance_type);
	readonly Tags = new Tags(this._handle, this._instance_type);
	readonly FollowRequest = new FollowRequest(this._handle, this._instance_type);

	/**
	 * GET /api/v1/accounts/verify_credentials
	 * https://docs.joinmastodon.org/methods/accounts/#verify_credentials
	 *
	 * @returns `Entity.Account`
	 */
	async verifyCredentials(): Promise<Entity.Account | null> {
		return this.fetchAPI<Entity.Account>('GET', `/api/v1/accounts/verify_credentials`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/accounts/search
	 * https://docs.joinmastodon.org/methods/accounts/#search
	 *
	 * @query `resolve`
	 * @query `following`
	 * @query `offset`
	 * @query `limit`
	 * @query `q`
	 * @returns `Array<Entity.Account>`
	 */
	async search(
		query: string,
		options?: SearchAccountOptional
	): Promise<Array<Entity.Account> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/accounts/search`, {
			query: { q: query, ...options },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/accounts/:id
	 * https://docs.joinmastodon.org/methods/accounts/#get
	 *
	 * @param `id`
	 * @returns `Entity.Account`
	 */
	async get(id: string): Promise<Entity.Account | null> {
		return this.fetchAPI<Entity.Account>('GET', `/api/v1/accounts/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/accounts/:id/statuses
	 * https://docs.joinmastodon.org/methods/accounts/#statuses
	 *
	 * @param `id`
	 * @query limit`
	 * @query max_id`
	 * @query since_id`
	 * @query min_id`
	 * @query exclude_replies`
	 * @query exclude_reblogs`
	 * @query only_media`
	 * @returns `Array<Entity.Status>`
	 */
	async getStatuses(
		id: string,
		options?: AccountStatusesOptional
	): Promise<Array<Entity.Status> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/accounts/${id}/statuses`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/accounts/:id/followers
	 * https://docs.joinmastodon.org/methods/accounts/#followers
	 *
	 * @param `id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `get_all`
	 * @query `sleep_ms`
	 * @returns `Array<Entity.Account>`
	 */
	async getFollowers(
		id: string,
		options?: OptionalSince
	): Promise<ResponseLinks<Array<Entity.Account>> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/accounts/${id}/followers`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/accounts/:id/following
	 * https://docs.joinmastodon.org/methods/accounts/#following
	 *
	 * @param `id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `get_all`
	 * @query `sleep_ms`
	 * @returns `Array<Entity.Account>`
	 */
	async getFollowing(
		id: string,
		options?: AccountFollowOptional
	): Promise<ResponseLinks<Array<Entity.Account>> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/accounts/${id}/following`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/accounts/relationships
	 * https://docs.joinmastodon.org/methods/accounts/#relationships
	 *
	 * @query `Array<id>`
	 * @returns `Array<Entity.Relationship>`
	 */
	async getRelationships(id: Array<string>): Promise<Array<Entity.Relationship> | null> {
		return this.fetchAPI<Array<Entity.Relationship>>('GET', `/api/v1/accounts/relationships`, {
			query: { id },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/accounts/relationships
	 * https://docs.joinmastodon.org/methods/accounts/#relationships
	 *
	 * @query `id`
	 * @returns `Entity.Relationship`
	 */
	async getRelationship(id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Array<Entity.Relationship>>('GET', `/api/v1/accounts/relationships`, {
			query: { id: [id] },
		}).then((r) => {
			if (!r) {
				return null;
			}

			return r.data[0];
		});
	}

	/**
	 * GET /api/v1/accounts/:id/lists
	 * https://docs.joinmastodon.org/methods/accounts/#lists
	 *
	 * @param `id`
	 * @returns `Array<Entity.List>`
	 */
	async getLists(id: string): Promise<Array<Entity.List> | null> {
		return this.fetchAPI<Array<Entity.List>>('GET', `/api/v1/accounts/${id}/lists`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * GET /api/v1/bookmarks
	 * https://docs.joinmastodon.org/methods/bookmarks/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	async getBookmarks(
		options?: OptionalFull
	): Promise<ResponseLinks<Array<Entity.Status>> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/bookmarks`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/favourites
	 * https://docs.joinmastodon.org/methods/favourites/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	async getFavourites(
		options?: OptionalMinMax
	): Promise<ResponseLinks<Array<Entity.Status>> | null> {
		return this.fetchAPI<Array<Entity.Status>>('GET', `/api/v1/favourites`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/mutes
	 * https://docs.joinmastodon.org/methods/mutes/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Account>`
	 */
	async getMutes(options?: OptionalMinMax): Promise<ResponseLinks<Array<Entity.Account>> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/mutes`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/blocks
	 * https://docs.joinmastodon.org/methods/blocks/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Account>`
	 */
	async getBlocks(
		options?: OptionalMinMax
	): Promise<ResponseLinks<Array<Entity.Account>> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/blocks`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/endorsements
	 * https://docs.joinmastodon.org/methods/endorsements/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @returns `Array<Entity.Account>`
	 */
	async getEndorsements(
		options?: OptionalSince
	): Promise<ResponseLinks<Array<Entity.Account>> | null> {
		return this.fetchAPI<Array<Entity.Account>>('GET', `/api/v1/endorsements`, {
			query: options,
		}).then((r) => {
			if (!r) {
				return null;
			}

			const links = parseResponseLink(r);

			return { data: r.data, links };
		});
	}

	/**
	 * GET /api/v1/preferences
	 * https://docs.joinmastodon.org/methods/preferences/#get
	 *
	 * @returns `Entity.Preferences`
	 */
	async getPreferences(): Promise<Entity.Preferences | null> {
		return this.fetchAPI<Entity.Preferences>('GET', `/api/v1/preferences`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/reports
	 * https://docs.joinmastodon.org/methods/reports/#post
	 *
	 * @body `FormBody.Report`
	 * @returns `Array<String>`
	 */
	async reports(id: string, options?: ReportOptional): Promise<Entity.Report | null> {
		return this.fetchAPI<Entity.Report>('POST', `/api/v1/reports`, {
			body: { account_id: id, ...options },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/accounts/:id/follow
	 * https://docs.joinmastodon.org/methods/accounts/#follow
	 *
	 * @param `id`
	 * @body `Body.Follow`
	 * @returns `Entity.Relationship`
	 */
	async follow(id: string, options?: FollowOptional): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>('POST', `/api/v1/accounts/${id}/follow`, {
			body: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/accounts/:id/unfollow
	 * https://docs.joinmastodon.org/methods/accounts/#unfollow
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async unfollow(id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>(
			'POST',
			`/api/v1/accounts/${id}/unfollow`,
			{}
		).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/accounts/:id/block
	 * https://docs.joinmastodon.org/methods/accounts/#block
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async block(id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>('POST', `/api/v1/accounts/${id}/block`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/accounts/:id/unblock
	 * https://docs.joinmastodon.org/methods/accounts/#unblock
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async unblock(id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>(
			'POST',
			`/api/v1/accounts/${id}/unblock`,
			{}
		).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/accounts/:id/mute
	 * https://docs.joinmastodon.org/methods/accounts/#mute
	 *
	 * @param `id`
	 * @body `Body.Mute`
	 * @returns `Entity.Relationship`
	 */
	async mute(id: string, notifications: boolean): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>('POST', `/api/v1/accounts/${id}/mute`, {
			body: { notifications },
		}).then((r) => r?.data ?? null);
	}

	/**
	 * POST /api/v1/accounts/:id/unmute
	 * https://docs.joinmastodon.org/methods/accounts/#unmute
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async unmute(id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>('POST', `/api/v1/accounts/${id}/unmute`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/accounts/:id/pin
	 * https://docs.joinmastodon.org/methods/accounts/#pin
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async pin(id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>('POST', `/api/v1/accounts/${id}/pin`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/accounts/:id/unpin
	 * https://docs.joinmastodon.org/methods/accounts/#unpin
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	async unpin(id: string): Promise<Entity.Relationship | null> {
		return this.fetchAPI<Entity.Relationship>('POST', `/api/v1/accounts/${id}/unpin`, {}).then(
			(r) => r?.data ?? null
		);
	}
}
