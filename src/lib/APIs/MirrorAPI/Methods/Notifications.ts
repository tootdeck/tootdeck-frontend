import { MirrorInterface } from '../Interface';

import type { NotificationsOptional } from '../types';
import type { Entity } from '../../../../types/mastodonEntities';

export class Notifications extends MirrorInterface {
	/**
	 * GET /api/v1/notifications
	 * https://docs.joinmastodon.org/methods/notifications/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @query `types`
	 * @query `exclude_types`
	 * @query `account_id`
	 * @returns `Array<Entity.Notification>`
	 */
	async getAll(options?: NotificationsOptional): Promise<Array<Entity.Notification> | null> {
		if (options?.types && options.types.length === 0) {
			options.types = undefined;
		}

		return this.fetchAPI<Array<Entity.Notification>>('GET', `/api/v1/notifications`, {
			query: options,
		}).then((r) => r?.data ?? null);
	}

	/**
	 * GET /api/v1/notifications/:id
	 * https://docs.joinmastodon.org/methods/notifications/#get-one
	 *
	 * @param `id`
	 * @returns `Entity.Notification`
	 */
	async getOne(id: string): Promise<Entity.Notification | null> {
		return this.fetchAPI<Entity.Notification>('GET', `/api/v1/notifications/${id}`, {}).then(
			(r) => r?.data ?? null
		);
	}

	/**
	 * POST /api/v1/notifications/clear
	 * https://docs.joinmastodon.org/methods/notifications/#clear
	 *
	 * @returns `{}`
	 */
	async dismissAll(): Promise<boolean> {
		return this.fetchAPI<{}>('POST', `/api/v1/notifications/clear`, {}).then((r) =>
			r ? true : false
		);
	}

	/**
	 * POST /api/v1/notifications/:id/dismiss
	 * https://docs.joinmastodon.org/methods/notifications/#dismiss'
	 *
	 * @param `id`
	 * @returns `{}`
	 */
	async dismissOne(id: string): Promise<boolean> {
		return this.fetchAPI<{}>('POST', `/api/v1/notifications/${id}/dismiss`, {}).then((r) =>
			r ? true : false
		);
	}
}
