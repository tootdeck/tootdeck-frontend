import { MirrorInterface } from './Interface';
import { Accounts } from './Methods/Account';
import { Instance } from './Methods/Instance';
import { Notifications } from './Methods/Notifications';
import { Statuses } from './Methods/Statuses';
import { Timelines } from './Methods/Timelines';
import { Search } from './Methods/Search';

import type { InstanceHandle } from '../../../types/handle';
import type { InstanceType } from '../../../types/megalodon';

export class MirrorAPI extends MirrorInterface {
	constructor(handle: InstanceHandle, type: InstanceType) {
		super(handle, type);
	}

	readonly Accounts = new Accounts(this._handle, this._instance_type);
	readonly Instance = new Instance(this._handle, this._instance_type);
	readonly Notifications = new Notifications(this._handle, this._instance_type);
	readonly Statuses = new Statuses(this._handle, this._instance_type);
	readonly Timeline = new Timelines(this._handle, this._instance_type);
	readonly Search = new Search(this._handle, this._instance_type);
}
