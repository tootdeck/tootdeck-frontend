import { TootDeckAPI } from './TootDeckAPI/TootDeckAPI';
import { RequestsManager } from '../Managers/RequestsManager';
import { FetchError, type AxiosError, type NestJSError } from './Error';
import { NormalMastodonResponseCodes, type APIResponse } from './fetchAPI';
import { errorModal } from '../errorModal';

import type { Entity } from '../../types/mastodonEntities';

export interface UploadAPIOptionalParams {
	onupload?: (ev: ProgressEvent<EventTarget>) => void;
	header?: object;
	hide_error?: boolean;
	body?: object;
	mirrorAPI?: boolean;
	retrying?: boolean;
}

function formatFetchError(
	error: { headers: Map<string, string>; status: number },
	body: NestJSError | AxiosError | string
) {
	const rateLimitReset = error.headers.get('x-ratelimit-reset');

	if (typeof body === 'string') {
		throw new FetchError({
			code: error.status,
			data: {
				mirror: false,
				message: body,
			},
		});
	}

	throw new FetchError({
		code: error.status,
		data: {
			mirror: !!(body as any)?.mirror,
			message: body.message,
			detail: (body as any)?.detail,
			rateLimitReset: rateLimitReset ? new Date(rateLimitReset) : undefined,
		},
	});
}

async function checkTokenExpired<T>(
	method: string,
	endpoint: string,
	file: File,
	optional?: UploadAPIOptionalParams
): Promise<any | void> {
	if (optional?.retrying) {
		return null;
	}

	const refresh = await TootDeckAPI.Auth.refresh();
	if (!refresh) {
		// If refresh fail the user will be disconnected, nothing more will be executed
		return;
	}

	RequestsManager.cancel(`TootDeckAPI:${endpoint}:` + JSON.stringify(optional));

	return uploadAPI(method, endpoint, file, { ...(optional ?? {}), retrying: true });
}

// TODO abort
export async function uploadAPI<T>(
	method: string,
	endpoint: string,
	file: File,
	optional?: UploadAPIOptionalParams
): Promise<APIResponse<T> | Entity.Response<T> | null> {
	let headers: HeadersInit | undefined = {};
	if (optional?.header) {
		headers = { ...headers, ...optional?.header };
	}

	const promise = new Promise<XMLHttpRequest>((resolve, reject) => {
		const request = new XMLHttpRequest();

		request.upload.onprogress = optional?.onupload ?? null;

		request.open(method, endpoint);
		request.onload = () => resolve(request);
		request.onerror = reject;

		if (optional?.header) {
			for (const [key, value] of Object.entries(optional.header)) {
				request.setRequestHeader(key, value);
			}
		}

		const form = new FormData();
		form.append('file', file);

		if (optional?.body) {
			for (const [key, value] of Object.entries(optional.body)) {
				form.append(key, value);
			}
		}

		request.send(form);
	});

	return promise
		.then(async (r) => {
			const headers = new Map<string, string>(
				// @ts-ignore
				r
					.getAllResponseHeaders()
					.replaceAll('\r', '')
					.split('\n')
					.map((data) => data.split(': '))
			);
			const content_type = headers.get('content-type')!;
			const is_json = content_type?.includes('application/json');

			// Token expired
			if (r.status === 401) {
				return checkTokenExpired(method, endpoint, file, optional);
			}

			const body = is_json ? JSON.parse(r.responseText) : r.responseText;

			if (!NormalMastodonResponseCodes.includes(r.status)) {
				return formatFetchError(
					{
						headers,
						status: r.status,
					},
					body
				);
			}

			if (optional?.mirrorAPI) {
				return body;
			}

			return {
				code: r.status,
				data: r.response,
			};
		})
		.catch((e) => {
			if (e instanceof FetchError) {
				return e;
			}

			if (e.status === 502) {
				errorModal('Server unreachable.', [e], true);
			} else {
				errorModal('Something went wrong.', [e], true);
			}

			return null;
		});
}
