export function UUIDToColor(str: string) {
	const offset = 0;
	const hexa = '#' + str?.slice(offset, offset + 6);
	if (!hexa) {
		console.error('undefined UUID.');
		return;
	}

	return hexa;
}
