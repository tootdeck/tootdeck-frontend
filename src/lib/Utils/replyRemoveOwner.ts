import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { ContentManager } from '../Managers/ContentManager';
import type { ReplyAccount } from '../ComponentFunctions/getReplyingAccounts';

import type { ParsedStatus } from '../../types/contentParsed';

export function replyRemoveOwner(
	account: TootDeckAccount,
	status: ParsedStatus,
	mentions: Array<ReplyAccount>
): Array<ReplyAccount> {
	if (!status.mentions.length || mentions.length === 1) {
		return mentions;
	}

	// if in a thread amd the owner of the status reply to it's own status
	if (status.in_reply_to_id) {
		const previous = ContentManager.Status.get(account, status.in_reply_to_id);
		if (previous && previous.account.id === status.account.id) {
			// Reorder to make the owner of the status the first account
			const index = mentions.findIndex((x) => x.id === status.account.id);
			if (index !== -1) {
				const first_account = mentions.splice(index, 1)[0];
				mentions.unshift(first_account);
			}

			return mentions;
		}
	}

	return mentions.filter((x) => x.id !== status.account.id);
}
