export function addPaddingZero(n: number, padding_size: number = 1): string {
	let zero_to_add = padding_size - n.toString().length + 1;

	return (zero_to_add >= 0 ? '0'.repeat(zero_to_add) : '') + n;
}
