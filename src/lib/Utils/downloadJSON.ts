export function downloadJSON(filename: string, json: Object) {
	const content = JSON.stringify(json);

	let element = document.createElement('a');
	element.setAttribute(
		'href',
		'data:application/json;charset=utf-8,' + encodeURIComponent(content)
	);
	element.setAttribute('download', filename);

	element.click();
	element.remove();
}
