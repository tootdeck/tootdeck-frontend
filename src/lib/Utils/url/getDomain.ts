export const getDomain = (url: string): string => new URL(url).hostname;
