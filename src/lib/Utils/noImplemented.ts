import { NotificationModalType, notificationModal } from '../../stores/NotificationsModal';

export const notImplemented = () =>
	notificationModal.create(NotificationModalType.Warn, 'Not yet implemented..', true);
