import { get } from 'svelte/store';

import { NotificationModalType, notificationModal } from '../../stores/NotificationsModal';
import { rdOptionsBehaviorShowChangelog } from '../../stores/Options';
import { GlobalWindow, GlobalWindowState } from '../../stores/Window';
import { rdOptionalFeaturesChangelog } from '../../stores/OptionalFeatures';

import { VERSION } from '../Constants/Version';
import { Changelog } from '../Changelog';

export function notifyNewerVersion() {
	const local_version = localStorage.getItem('version');

	localStorage.setItem('version', VERSION);

	if (local_version !== VERSION) {
		notificationModal.create(
			NotificationModalType.Info,
			`TootDeck has been updated to ${VERSION}.`,
			true
		);

		if (
			get(rdOptionalFeaturesChangelog) &&
			get(rdOptionsBehaviorShowChangelog) &&
			(Changelog.local_state.nightly || Changelog.local_state.release)
		) {
			GlobalWindow.create({
				state: GlobalWindowState.Changelog,
				special: false,
			});
		}
	}
}
