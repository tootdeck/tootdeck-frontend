import { tick } from 'svelte';

import { notificationModal } from '../../stores/NotificationsModal';

import { sleep } from './js/sleep';

export async function awaitRender(trigger: () => any | Promise<any>) {
	const notif = await notificationModal.createLoading('Rendering', 3);

	await sleep(10);
	await trigger();
	await tick();

	notif.finish();
}

export async function awaitAppReload(trigger: () => any | Promise<any>) {
	const notif = await notificationModal.createLoading('Reloading app', 3);

	await sleep(10);
	await trigger();
	await tick();

	setTimeout(() => {
		notif.finish();
	}, 1000);
}
