export function findElement(
	source: HTMLElement | Element | null,
	target: { tag: string; class: string }
): boolean;
export function findElement(
	source: HTMLElement | Element | null,
	target: { tag?: string; class: string }
): boolean;
export function findElement(
	source: HTMLElement | Element | null,
	target: { tag?: string; class: string }
): boolean {
	if (!source || Object.is(document.body, source)) {
		return false;
	}

	const has_class = source.classList.contains(target.class);
	const has_tag = target.tag && source.tagName === target.tag;
	if ((has_tag && has_class) || has_class) {
		return true;
	}

	if (!source.parentElement) {
		return false;
	}

	return findElement(source.parentElement, target);
}
