export const MacOS = navigator.userAgent.includes('Macintosh');
export const Firefox = navigator.userAgent.includes('Firefox');
export const CTRL = MacOS ? 'OPT' : 'CTRL';
export const ALT = MacOS ? 'CMD' : 'ALT';
