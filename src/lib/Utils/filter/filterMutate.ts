/**
 * Since this function mutates the array, the array is iterated backward!
 */
export function filterMutate<T>(
	array: Array<T>,
	predicate: (value: T, index: number, array: Array<T>) => boolean
) {
	for (let i = array.length - 1; i >= 0; i--) {
		if (!predicate(array[i], i, array)) {
			array.splice(i, 1);
		}
	}
}
