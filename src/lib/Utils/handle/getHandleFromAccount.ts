import { getDomain } from '../url/getDomain';
import { stringifyHandle } from './stringifyHandle';

import type { ParsedAccount } from '../../../types/contentParsed';
import type { InstanceHandle } from '../../../types/handle';
import type { Entity } from '../../../types/mastodonEntities';

export function getHandleFromAccount(
	entity: Entity.Account | ParsedAccount,
	stringify: true
): string;
export function getHandleFromAccount(
	entity: Entity.Account | ParsedAccount,
	stringify: false
): InstanceHandle;
export function getHandleFromAccount(
	entity: Entity.Account | ParsedAccount,
	stringify: boolean
): InstanceHandle | string {
	const handle: InstanceHandle = {
		username: entity.username,
		domain: getDomain(entity.url),
	};

	if (stringify) {
		return stringifyHandle(handle);
	}

	return handle;
}
