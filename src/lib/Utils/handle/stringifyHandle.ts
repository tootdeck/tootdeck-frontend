import type { InstanceHandle } from '../../../types/handle';

export const stringifyHandle = (handle: InstanceHandle) => handle.username + '@' + handle.domain;
