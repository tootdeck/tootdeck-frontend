export function bodyChangeClass(class_names: Array<string>, target: string) {
	class_names.forEach((class_name) => {
		document.body.classList.remove(class_name);
	});

	document.body.classList.add(target);
}
