export class CustomAudio extends Audio {
	constructor(src: string) {
		super(src);
	}

	override async play() {
		return new Promise<void>((resolve) => {
			super.onended = () => {
				resolve();
			};
			super.onerror = () => {
				resolve();
			};
			super.play().catch((e) => {});
		});
	}
}
