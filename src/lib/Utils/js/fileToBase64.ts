export async function fileToBase64(file: File): Promise<string> {
	return new Promise((resolve, reject) => {
		const reader = new FileReader();

		reader.onload = (e) => {
			const target = e.target!;
			resolve(target.result as string);
		};
		reader.onerror = (e) => {
			reject(e);
		};

		reader.readAsDataURL(file);
	});
}
