export const swap = (array: Array<any>, a: number, b: number) =>
	([array[b], array[a]] = [array[a], array[b]]);
