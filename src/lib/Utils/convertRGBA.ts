export type RGB = `${number}, ${number}, ${number}`;
export interface RGBA {
	r: number;
	g: number;
	b: number;
	a: number;
}

export function storeToRGBA(store: RGB): RGBA {
	const split = store.split(', ');
	return {
		r: Number.parseInt(split[0]),
		g: Number.parseInt(split[1]),
		b: Number.parseInt(split[2]),
		a: 1,
	};
}

export function RGBAtoStore(rgba: RGBA): RGB {
	const { r, g, b } = rgba;

	return `${r}, ${g}, ${b}`;
}

export function hexToRGB(hex: string): RGB {
	const normal = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
	if (normal) {
		const [r, g, b] = normal.slice(1).map((e) => parseInt(e, 16));
		return `${r}, ${g}, ${b}`;
	}

	const shorthand = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
	if (shorthand) {
		const [r, g, b] = shorthand.slice(1).map((e) => 0x11 * parseInt(e, 16));
		return `${r}, ${g}, ${b}`;
	}

	throw new Error(`${hex} is not a valid hexadecimal color`);
}
