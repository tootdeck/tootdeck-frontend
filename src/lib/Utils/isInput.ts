export function isInput(e: Event) {
	const target = e.target as HTMLElement;
	return target.tagName === 'INPUT' || target.tagName === 'TEXTAREA';
}
