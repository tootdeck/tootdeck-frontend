import { Marked } from 'marked';
import markedBidi from 'marked-bidi';
import { markedHighlight } from 'marked-highlight';
import hljs from 'highlight.js';

const _marked = new Marked(
	markedHighlight({
		langPrefix: 'hljs language-',
		highlight(code, lang) {
			const language = hljs.getLanguage(lang) ? lang : 'plaintext';
			return hljs.highlight(code, { language }).value;
		},
	})
);

_marked
	.use({
		gfm: true,
	})
	.use(markedBidi());

export const marked = _marked;
