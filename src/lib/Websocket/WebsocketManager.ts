import { get } from 'svelte/store';

import { Account, refreshInstanceToken, stAccounts } from '../../stores/Account/Accounts';
import { NotificationModalType, notificationModal } from '../../stores/NotificationsModal';
import { GlobalWindow, GlobalWindowState } from '../../stores/Window';
import { rdOptionsBehaviorSyncAutoSync, stOptionsReadOnly } from '../../stores/Options';
import { stLayout } from '../../stores/Columns/Layout';

import { TootDeckAPI } from '../APIs/TootDeckAPI/TootDeckAPI';
import type { StandardColumn } from '../Columns/StandardColumn';
import { EventsManager } from '../Events/EventsManager';
import { TootDeckEvents } from '../Events/TootDeckEvents';
import { ContentManager } from '../Managers/ContentManager';
import { ReconnectWebSocket } from './ReconnectWebsocket';
import { InteractionsManager } from '../Managers/InteractionsManager';
import { Sessions } from '../Sessions';
import { errorModal } from '../errorModal';
import { TootDeckConfig } from '../TootDeckConfig';
import { parseHandle } from '../Utils/handle/parseHandle';
// import { sleep } from '../Utils/sleep';
import { VERSION } from '../Constants/Version';

import {
	CommandType,
	AvailableSubscription,
	type OptionalParams,
	type ReferenceCountedSubscription,
	type SubscribeEvent,
	type WebsocketResponse,
	ResponseType,
	type WebsocketMirrorResponse,
	type ResponseAPI,
	ResponseAPIData,
} from './types';
import { SettingsState } from '../../types/settings';

class WebsocketSubscriptionsManagerInner {
	static readonly instance = new WebsocketSubscriptionsManagerInner();
	private readonly websocket = new ReconnectWebSocket();
	private readonly subscriptions = new Map<string, Map<string, ReferenceCountedSubscription>>();
	private websocket_ready: boolean = false;
	// private progress: any | null = null;

	constructor() {
		this.websocket.addListener('message', this.handleEvent.bind(this));
		this.websocket.addListener('error', () => {});
		this.websocket.addListener('close', () => (this.websocket_ready = false));
		// this.websocket.addListener('reconnect', this.handleReconnect.bind(this));
	}

	private handleAPIEvent(api: ResponseAPI) {
		switch (api.data) {
			case ResponseAPIData.AwaitingSubscription:
				{
					this.handleConnect();
					this.websocket_ready = true;
					this.subscriptions?.forEach((subscriptions_handle) => {
						subscriptions_handle.forEach((subscription) => {
							const handle = subscription.used_in[0].account.mirrorAPI.handle_string;
							this.send(
								this.formatSubscription(CommandType.Subscribe, subscription, handle)
							);
						});
					});
					get(stLayout).forEach((column) => {
						(column as StandardColumn<any>)?.nextContent();
					});
				}
				break;
			case ResponseAPIData.Reconnected:
				{
					const handle = api.handle!;
					this.subscriptions?.forEach((subscriptions_handle) => {
						subscriptions_handle.forEach((subscription) => {
							subscription.used_in.forEach((column) => {
								if (handle === column.account.mirrorAPI.handle_string) {
									column.nextContent();
								}
							});
						});
					});
				}
				break;
			case ResponseAPIData.Refresh:
				{
					TootDeckAPI.Auth.refresh();
				}
				break;
			case ResponseAPIData.InternalServerError:
				{
					errorModal(
						'Something wrong happened when connecting to websocket.',
						undefined,
						true
					);
				}
				break;
			case ResponseAPIData.AppExpired:
				{
					const handles = Array.from(get(stAccounts))
						.filter(([_, x]) => x.mirrorAPI.domain === (api as any).domain)
						.map(([_, x]) => x.mirrorAPI.handle_string);
					handles.forEach((handle) => {
						notificationModal.create(
							NotificationModalType.Error,
							'Click to refresh your session for ' + handle,
							true,
							() => refreshInstanceToken(handle)
						);
					});
				}
				break;
			case ResponseAPIData.NewSession:
				{
					notificationModal.create(
						NotificationModalType.Info,
						"A new session has been created, if it's not you click here to revoke the session and change your instance account password.",
						false,
						() => {
							Sessions.init(false);
							GlobalWindow.create({
								state: GlobalWindowState.Setting,
								special: false,
								category: SettingsState.Sessions,
							});
						}
					);
				}
				break;
			case ResponseAPIData.ConfigUpdate:
				{
					const uuid = api.uuid;
					if (get(rdOptionsBehaviorSyncAutoSync)) {
						TootDeckConfig.download(uuid);
					}
				}
				break;
			case ResponseAPIData.CouldNotConnect:
				{
					const handle = api.handle!;
					if (!get(stOptionsReadOnly)) {
						errorModal(`Couldn't connect websocket for ${handle}.`, undefined, true);
					}
				}
				break;
			case ResponseAPIData.Interactions:
				{
					const handle = parseHandle(api.handle!)!;
					InteractionsManager.store(handle, api.interactions!);
				}
				break;
			case ResponseAPIData.BroadcastMessage:
				notificationModal.create(NotificationModalType.Info, api.message!, false);
				break;
		}
	}

	private handleMirrorEvent(mirror: WebsocketMirrorResponse) {
		const account = Account.get(mirror.handle!, false);
		if (!account) {
			return;
		}

		switch (mirror.type) {
			case ResponseType.Update:
				{
					const stream = mirror.data.stream;
					if (stream.find((x) => x.startsWith(AvailableSubscription.User))) {
						EventsManager.sendEvent(TootDeckEvents.User, {
							account,
							data: [{ ...mirror.data.content, animateIn: true, animateOut: true }],
						});
					} else if (stream.find((x) => x.startsWith(AvailableSubscription.Local))) {
						EventsManager.sendEvent(TootDeckEvents.LocalTimeline, {
							account,
							data: [{ ...mirror.data.content, animateIn: true, animateOut: true }],
						});
					} else if (stream.find((x) => x.startsWith(AvailableSubscription.Public))) {
						EventsManager.sendEvent(TootDeckEvents.FederatedTimeline, {
							account,
							data: [{ ...mirror.data.content, animateIn: true, animateOut: true }],
						});
					} else if (stream.find((x) => x.startsWith(AvailableSubscription.Hashtag))) {
						EventsManager.sendEvent(TootDeckEvents.Hashtag, {
							account,
							data: [{ ...mirror.data.content, animateIn: true, animateOut: true }],
						});
					} else if (stream.find((x) => x.startsWith(AvailableSubscription.List))) {
						EventsManager.sendEvent(TootDeckEvents.List, {
							account,
							data: [{ ...mirror.data.content, animateIn: true, animateOut: true }],
							list_id: stream[1],
						});
					} else {
						EventsManager.sendEvent(TootDeckEvents.Status, {
							account,
							data: [{ ...mirror.data.content, animateIn: true, animateOut: true }],
						});
					}
				}
				break;
			case ResponseType.Status_update:
				{
					EventsManager.sendEvent(TootDeckEvents.Status, {
						account,
						data: [{ ...mirror.data.content, animateIn: true, animateOut: true }],
					});
				}
				break;
			case ResponseType.Notification:
				{
					EventsManager.sendEvent(TootDeckEvents.Notification, {
						account,
						data: [mirror.data.content].map((n) => {
							if (n.status) {
								n.status = { ...n.status, animateIn: true, animateOut: true };
							}
							return { ...n, notify: true, animateIn: true, animateOut: true };
						}),
					});
				}
				break;
			case ResponseType.Conversation:
				// EventsManager.sendEvent(TootDeckEvents.Status, {
				// 	account,
				// 	data: [mirror.data.conversation],
				// });
				break;
			case ResponseType.Delete:
				{
					ContentManager.Status.forceRemove(account.mirrorAPI.handle, mirror.data as any);
				}
				break;
		}
	}

	private handleEvent(data: WebsocketResponse) {
		if (data.type === ResponseType.API) {
			this.handleAPIEvent(data as ResponseAPI);
		} else {
			this.handleMirrorEvent(data as WebsocketMirrorResponse);
		}
	}

	private async handleConnect() {
		const api_version = await TootDeckAPI.Others.version();
		if (api_version && api_version !== VERSION) {
			notificationModal.create(
				NotificationModalType.Info,
				'An update is available, click here to reload.',
				false,
				() => window.location.reload()
			);
		}
	}

	// private async handleReconnect() {
	// 	if (this.progress) {
	// 		return;
	// 	}

	// 	let limit = 10;
	// 	this.progress = await notificationModal.createLoading(
	// 		'Connexion lost, attempting to reconnect...',
	// 		limit
	// 	);
	// 	for (let i = 0; i < limit; i++) {
	// 		await sleep(1000);
	// 		if (this.progress?.tick) {
	// 			this.progress?.tick();
	// 		}
	// 	}

	// 	this.progress = null;
	// }

	private formatSubscription(
		command: CommandType,
		subscription: ReferenceCountedSubscription,
		handle: string
	) {
		return {
			handle: handle,
			type: command,
			stream: subscription.stream,
			list: subscription.list,
			tag: subscription.tag,
		};
	}

	private setSubscription(
		subscription_handle: Map<string, ReferenceCountedSubscription>,
		that: StandardColumn<any>,
		key: string,
		subscription: AvailableSubscription,
		parms?: OptionalParams
	) {
		const subscribed = subscription_handle.get(key);
		if (subscribed) {
			if (
				!subscribed.used_in.find((x) => x.identifier + x.id === that.identifier + that.id)
			) {
				subscribed.used_in.push(that);
			}
		} else {
			const entry = {
				used_in: [that],
				stream: subscription,
				list: parms?.list_id,
				tag: parms?.hashtag,
			};
			subscription_handle.set(key, entry);

			const handle = that.account.mirrorAPI.handle_string;
			this.send(this.formatSubscription(CommandType.Subscribe, entry, handle));

			this.startSocket();
		}
	}

	private deleteSubscription(
		subscription_handle: Map<string, ReferenceCountedSubscription>,
		that: StandardColumn<any>,
		key: string
	) {
		const subscribed = subscription_handle.get(key);
		if (subscribed) {
			subscribed.used_in = subscribed.used_in.filter(
				(x) => x.identifier + x.id !== that.identifier + that.id
			);
			if (subscribed.used_in.length === 0) {
				subscription_handle.delete(key);

				const handle = that.account.mirrorAPI.handle_string;
				this.send(this.formatSubscription(CommandType.Unsubscribe, subscribed, handle));
			}
		}
	}

	private formatOptional(params?: OptionalParams) {
		let optional = '';
		if (params?.list_id) {
			optional = ':' + params?.list_id;
		}

		if (params?.hashtag) {
			optional = ':' + params?.hashtag;
		}

		return optional;
	}

	private handleSubscription(
		command: CommandType,
		that: StandardColumn<any>,
		subscription: AvailableSubscription,
		params?: OptionalParams
	) {
		const optional = this.formatOptional(params);
		const handle = that.account.mirrorAPI.handle_string;

		let subscription_handle = this.subscriptions.get(handle);
		if (subscription_handle) {
			switch (command) {
				case CommandType.Subscribe:
					this.setSubscription(
						subscription_handle,
						that,
						subscription + optional,
						subscription,
						params
					);
					break;
				case CommandType.Unsubscribe:
					this.deleteSubscription(subscription_handle, that, subscription + optional);
					break;
			}
		} else {
			if (command === CommandType.Subscribe) {
				subscription_handle = this.subscriptions
					.set(handle, new Map<string, ReferenceCountedSubscription>())
					.get(handle)!;
				this.setSubscription(subscription_handle, that, subscription, subscription, params);
			}
		}
	}

	private async startSocket() {
		if (!this.websocket.started) {
			this.websocket.start();
		}
	}

	subscribeColumn(
		that: StandardColumn<any>,
		subscription: AvailableSubscription,
		params?: OptionalParams
	) {
		this.handleSubscription(CommandType.Subscribe, that, subscription, params);
	}

	unsubscribeColumn(
		that: StandardColumn<any>,
		subscription: AvailableSubscription,
		parmas?: OptionalParams
	) {
		this.handleSubscription(CommandType.Unsubscribe, that, subscription, parmas);
	}

	send(data: SubscribeEvent) {
		if (this.websocket_ready) {
			this.websocket.send(JSON.stringify(data));
		}
	}

	destroy() {
		this.websocket.stop();
		this.subscriptions.clear();
	}
}

export const WebsocketSubscriptionsManager = WebsocketSubscriptionsManagerInner.instance;
