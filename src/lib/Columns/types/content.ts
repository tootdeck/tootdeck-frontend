import type { ResponseLinks } from '../../Utils/parseResponseLInk';

export type FindFunct<T, R> = (
	params: R
) => Promise<Array<T> | null> | Promise<ResponseLinks<Array<T>> | null>;
