import type { StandardColumnParamsStore } from './options';

export abstract class IColumn {
	abstract get name(): string;
	abstract get icon(): string;
	abstract get id(): number;
	abstract get identifier(): string;
	abstract get params(): StandardColumnParamsStore;
	abstract get component(): ConstructorOfATypedSvelteComponent;
	abstract get props(): any;
	abstract clear(): void;
	abstract destroy(): void;
}

export abstract class IColumnType {
	abstract get name(): string;
	abstract get icon(): string;
	abstract get identifier(): string;
	abstract get component(): ConstructorOfATypedSvelteComponent;
	abstract get props(): any;
}
