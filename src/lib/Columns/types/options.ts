import type { Writable } from 'svelte/store';

import type { IColumn } from './interfaces';

import type { ParsedAccount } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';

export interface OverrideTitle {
	title: string;
	handle: string;
}

export interface StandardColumnOptions {
	collapsed: boolean;
	sound_uuid: string | null;
	override_title: {
		value: OverrideTitle | null;
		toggle: boolean;
	};
}

export interface StandardNotificationColumnOptions extends StandardColumnOptions {
	group: boolean;
	types: Array<Entity.NotificationType>;
}

export interface StandardColumnParams {
	account: {
		username: string;
		domain: string;
	};
	options: StandardColumnOptions;
}

export interface ThreadColumnParams extends StandardColumnParams {
	status_id?: string;
	main_column?: IColumn;
}

export interface StandardColumnProfileParms extends StandardColumnParams {
	profile_account_id: string;
	request_params: { exclude_replies: boolean; pinned: boolean; only_media: boolean };
}

export interface StandardColumnListParams extends StandardColumnParams {
	list: Entity.List;
}

export interface StandardColumnUserParams extends StandardColumnParams {
	user: ParsedAccount;
}

export interface StandardColumnHashtagParams extends StandardColumnParams {
	hashtag: string;
}

export interface StandardNotificationColumnParams extends StandardColumnParams {
	options: StandardNotificationColumnOptions;
}

export interface StandardColumnParamsStore extends Omit<StandardColumnParams, 'options'> {
	options: Writable<StandardColumnOptions>;
}

export interface StandardNotificationColumnParamsStore
	extends Omit<StandardColumnParams, 'options'> {
	options: Writable<StandardNotificationColumnOptions>;
}

export interface StandardColumnListParamsStore extends Omit<StandardColumnParams, 'options'> {
	list: Entity.List;
	options: Writable<StandardColumnOptions>;
}
