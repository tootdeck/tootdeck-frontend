import { tick } from 'svelte';
import { get } from 'svelte/store';

import type { TootDeckAccount } from '../../stores/Account/Accounts';

import type { StandardColumnType } from './TootDeckColumnTypes/StandardColumnType';
import { ColumnLinkType, StandardColumn } from './StandardColumn';
import { ContentManager } from '../Managers/ContentManager';

import { debugLogger } from '../debugLogger';
import { errorModal } from '../errorModal';
import type { IndexedArray } from '../IndexedArray';
import { sleep } from '../Utils/js/sleep';

import type { ParsedStatus } from '../../types/contentParsed';
import type { StandardColumnParams } from './types/options';
import type { NextContent } from './types/return';
import type { FindFunct } from './types/content';
import type { OptionalFull } from '../APIs/MirrorAPI/types';
import { Entity } from '../../types/mastodonEntities';

export abstract class StandardStatusesColumn extends StandardColumn<ParsedStatus> {
	constructor(
		account: TootDeckAccount,
		kind: StandardColumnType,
		id: number,
		options: StandardColumnParams
	) {
		super(account, kind, id, options);
	}

	/**
	 * @returns `true` when content has been removed
	 */
	limitContent(content: IndexedArray<ParsedStatus>): boolean {
		if (!this._top || content.length <= this._size_limit + 20) {
			return false;
		}

		for (let i = content.length; i > this._size_limit; i--) {
			const status = content.deleteLast();
			ContentManager.Status.removeOwner(this, status.id);
		}

		if (!get(this._performance_mode)) {
			this._performance_mode.set(true);
			setTimeout(async () => {
				await tick();
				this._performance_mode.set(false);
			}, 1000);
		}

		return true;
	}

	/**
	 * @returns `true` when the new status is older than the tested status
	 */
	private checkOlder(first: ParsedStatus | undefined, new_status: ParsedStatus): boolean {
		if (first) {
			const _old = new Date(first.created_at);
			const _new = new Date(new_status.created_at);
			const older = _old.valueOf() > _new.valueOf();
			if (older) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Compare `created_at` and insert `status` in the right index
	 *
	 * @param content `IndexedArray<ParsedStatus>`
	 * @param status `ParsedStatus`
	 */
	protected reorder(content: IndexedArray<ParsedStatus>, status: ParsedStatus): void {
		let i = 0;
		for (const old of content.get()) {
			if (this.checkOlder(old, status)) {
				i++;
				continue;
			}

			content.setAt(status, i);
			return;
		}

		content.setAt(status, i);
	}

	/**
	 * next content
	 */
	// #region

	// Sometimes instance return the same response so last id is to stop it from loop
	async findNextContent(findFunct: FindFunct<Entity.Status, OptionalFull>): Promise<void> {
		if (this._refreshing) {
			return;
		}
		this._refreshing = true;

		const id = this.first_content_id;
		if (!id) {
			this.processQueuedEvents();
			return;
		}

		const response = await findFunct({
			limit: this._fetch_limit,
		});
		if (!response) {
			errorModal('Failed to get next statuses.', undefined, true);
			this.processQueuedEvents();
			return;
		}

		const data = this.getResponse(ColumnLinkType.Top, response);

		const end = get(this._content)
			.slice(0, this._fetch_limit)
			.reverse()
			.find((x) => data.find((y) => x.id === y.id));
		if (!end) {
			this._content.update((v) => {
				v.setTop(this.previousContentBtn());
				return v;
			});
		}

		data.reverse();

		while (data.length) {
			const statuses = data.splice(0, 3);
			this._content.update((v) => {
				statuses.forEach((status) => {
					const parsed = ContentManager.Status.store(this, status);
					v.setTop(parsed);
				});

				return v;
			});
			await sleep(500);
		}

		this.processQueuedEvents();
	}

	abstract nextContent(): Promise<void>;

	// #endregion

	/**
	 * previous content
	 */
	// #region

	previousContentBtn(): ParsedStatus {
		this._previous_btn_uuid = window.crypto.randomUUID();
		return {
			account: this._account.entity,
			animateIn: false,
			animateOut: true,
			application: null,
			bookmarked: false,
			card: null,
			content: null,
			created_at: new Date().toLocaleDateString(),
			edited_at: '',
			favourited: false,
			emojis: [],
			emoji_reactions: [],
			favourites_count: 0,
			id: this._previous_btn_uuid,
			in_reply_to_account_id: null,
			in_reply_to_id: null,
			language: null,
			media_attachments: [],
			mentions: [],
			muted: null,
			pinned: null,
			plain_content: '',
			poll: null,
			quote: false,
			reblog: null,
			reblogged: false,
			reblogs_count: 0,
			replies_count: 0,
			sensitive: false,
			spoiler_text: '',
			tags: [],
			tootdeck: {
				height: 0,
				in_notification_id: [],
				in_status: [],
				show_cw: false,
				media_attachments_show: null,
				quote_urls: [],
				update: 1,
				uuid: window.crypto.randomUUID(),
				is_twitter_embed: false,
				embed: {
					twitter: [],
					youtube: [],
					dailymotion: [],
				},
			},
			uri: 'TootDeck:btn:previous',
			url: '',
			visibility: Entity.Visibility.Private,
		};
	}

	protected async findImmediatelyPreviousContent(
		findFunct: FindFunct<Entity.Status, OptionalFull>
	): Promise<void> {
		const content = get(this._content);
		const before_btn_index = (content.findIndex(this._previous_btn_uuid) ?? 0) - 1;
		const id = content.get()[before_btn_index]?.id;
		if (!id) {
			return;
		}

		const response = await findFunct({
			max_id: id,
			limit: this._fetch_limit,
		});
		if (!response) {
			errorModal('Failed to get previous statuses.', undefined, true);
			return;
		}

		const data = this.getResponse(ColumnLinkType.Middle, response);

		this._content.update((v) => {
			data.forEach((status) => {
				const parsed = ContentManager.Status.store(this, status);
				const index = v.findIndex(parsed.id);
				const btn_index = v.findIndex(this._previous_btn_uuid);
				if (index === -1) {
					v.setAt(parsed, btn_index);
					return;
				}

				if (index !== -1 && btn_index !== -1) {
					this._previous_btn_uuid = '';
					v.delete(this._previous_btn_uuid, btn_index);
					v.setAt(parsed, btn_index);
				}
			});

			return v;
		});
	}

	abstract immediatelyPreviousContent(): Promise<void>;

	protected async findPreviousContent(
		findFunct: FindFunct<Entity.Status, OptionalFull>
	): Promise<NextContent> {
		const id = this.last_content_id;
		if (!id) {
			return true;
		}

		const response = await findFunct({
			max_id: id,
			limit: this._fetch_limit,
		});
		if (!response) {
			errorModal('Failed to get previous statuses.', undefined, true);
			return 'DEAD';
		}

		const data = this.getResponse(ColumnLinkType.Bottom, response);

		this._content.update((v) => {
			data.forEach((status) => {
				const parsed = ContentManager.Status.store(this, status);
				v.setBottom(parsed);
			});

			return v;
		});

		return data.length !== this._fetch_limit;
	}

	abstract previousContent(): Promise<NextContent>;

	// #endregion

	removeOwnership(content: ParsedStatus) {
		if (content.reblog) {
			// prettier-ignore
			debugLogger(`${this.identifier}:${this._account.entity.username} Removing content ${content.reblog.id}`);
			ContentManager.Status.removeOwner(this, content.reblog.id);
		}

		// prettier-ignore
		debugLogger(`${this.identifier}:${this._account.entity.username} Removing content ${content.id}`);
		ContentManager.Status.removeOwner(this, content.id);
	}
}
