import { StandardColumnType } from './TootDeckColumnTypes/StandardColumnType';
import { ListColumnType } from './TootDeckColumnTypes/ListColumnTypes';

// prettier-ignore
export enum TootDeckColumnIdentifier {
	Federated		= 'TootDeck:Federated',
	Local			= 'TootDeck:Local',
	Home			= 'TootDeck:Home',
	User			= 'TootDeck:User',
	Notifications	= 'TootDeck:Notifications',
	Search			= 'TootDeck:Search',
	List			= 'TootDeck:List',
	Trending		= 'TootDeck:Trending',
	Favourites		= 'TootDeck:Favourites',
	Messages		= 'TootDeck:Messages',
	Mentions		= 'TootDeck:Mentions',
	Bookmarks		= 'TootDeck:Bookmarks',
	Thread			= "TootDeck:Thread",
	Profile			= "TootDeck:Profile",
	Hashtag			= "TootDeck:Hashtag"
}

// prettier-ignore
export const TootDeckColumnIcon = {
	Federated		: "fa fa-globe",
	Local			: "fa fa-users",
	Home			: "fa fa-home",
	User			: "fa fa-user",
	Notifications	: "fa fa-bell",
	Search			: "fa fa-search",
	List			: "fa fa-list",
	Trending		: "fa fa-line-chart",
	Favourites		: "fa fa-star",
	Messages		: "fa fa-envelope",
	Mentions		: "fa fa-at",
	Bookmarks		: "fa fa-bookmark",
	Thread			: "fa fa-align-left",
	Profile			: "fa fa-bug",
	Hashtag			: "fa fa-hashtag"
}

// OK
// prettier-ignore
export const FederatedTimelineColumnType = new StandardColumnType('Federated',		TootDeckColumnIcon.Federated,		TootDeckColumnIdentifier.Federated);
// OK
// prettier-ignore
export const LocalTimelineColumnType 	= new StandardColumnType('Local',			TootDeckColumnIcon.Local,			TootDeckColumnIdentifier.Local);
// OK
// prettier-ignore
export const HomeColumnType 			= new StandardColumnType('Home',			TootDeckColumnIcon.Home,			TootDeckColumnIdentifier.Home);
// OK
// prettier-ignore
export const UserColumnType 			= new StandardColumnType('User',			TootDeckColumnIcon.User,			TootDeckColumnIdentifier.User);
// OK
// prettier-ignore
export const NotificationsColumnType	= new StandardColumnType('Notifications',	TootDeckColumnIcon.Notifications,	TootDeckColumnIdentifier.Notifications);
// TODO
// prettier-ignore
export const SearchColumnType 			= new StandardColumnType('Search',			TootDeckColumnIcon.Search,			TootDeckColumnIdentifier.Search);
// OK
// prettier-ignore
export const ListsColumnType 			= new ListColumnType('List',				TootDeckColumnIcon.List,			TootDeckColumnIdentifier.List);
// TODO
// prettier-ignore
export const TrendingColumnType 		= new StandardColumnType('Trending',		TootDeckColumnIcon.Trending,		TootDeckColumnIdentifier.Trending);
// OK
// prettier-ignore
export const FavouritesColumnType 		= new StandardColumnType('Favourites',		TootDeckColumnIcon.Favourites,		TootDeckColumnIdentifier.Favourites);
// TODO
// prettier-ignore
export const MessagesColumnType 		= new StandardColumnType('Messages',		TootDeckColumnIcon.Messages,		TootDeckColumnIdentifier.Messages);
// TODO
// prettier-ignore
export const MentionsColumnType 		= new StandardColumnType('Mentions',		TootDeckColumnIcon.Mentions,		TootDeckColumnIdentifier.Mentions);
// OK
// prettier-ignore
export const BookmarksColumnType 		= new StandardColumnType('Bookmarks',		TootDeckColumnIcon.Bookmarks,		TootDeckColumnIdentifier.Bookmarks);
// OK
// prettier-ignore
export const ThreadColumnType 			= new StandardColumnType('Thread',			TootDeckColumnIcon.Thread,		TootDeckColumnIdentifier.Thread);
// OK
// prettier-ignore
export const ProfileColumnType 			= new StandardColumnType('Profile',			TootDeckColumnIcon.Profile,		TootDeckColumnIdentifier.Profile);
// OK
// prettier-ignore
export const HashtagColumnType 			= new StandardColumnType('Hashtag',			TootDeckColumnIcon.Hashtag,		TootDeckColumnIdentifier.Hashtag);

export const TootDeckColumnType = [
	FederatedTimelineColumnType,
	LocalTimelineColumnType,
	HomeColumnType,
	UserColumnType,
	NotificationsColumnType,
	SearchColumnType,
	ListsColumnType,
	TrendingColumnType,
	FavouritesColumnType,
	MessagesColumnType,
	MentionsColumnType,
	BookmarksColumnType,
	ThreadColumnType,
	ProfileColumnType,
	HashtagColumnType,
];
