import WhichAccountLists from '../../../components/Window/AddColumn/Lists/WhichAccountLists.svelte';

import { StandardColumnType } from './StandardColumnType';

export class ListColumnType extends StandardColumnType {
	override get component() {
		return WhichAccountLists;
	}
}
