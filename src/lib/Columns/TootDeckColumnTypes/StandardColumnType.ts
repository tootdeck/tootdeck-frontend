import StandardColumnPicker from '../StandardColumnPicker.svelte';

import type { IColumnType } from '../types/interfaces';

export class StandardColumnType implements IColumnType {
	private readonly _name: string;
	private readonly _icon: string;
	private readonly _identifier: string;

	constructor(name: string, icon: string, identifier: string) {
		this._name = name;
		this._icon = icon;
		this._identifier = identifier;
	}

	get name() {
		return this._name;
	}

	get icon() {
		return this._icon;
	}

	get identifier() {
		return this._identifier;
	}

	get component(): any {
		return StandardColumnPicker;
	}

	get props() {
		return { text: this._name };
	}
}
