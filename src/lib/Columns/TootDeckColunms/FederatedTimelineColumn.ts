import { Account } from '../../../stores/Account/Accounts';

import { FederatedTimelineColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEvent } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import { WebsocketSubscriptionsManager } from '../../Websocket/WebsocketManager';
import type { IndexedArray } from '../../IndexedArray';

import type { StandardColumnParams } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';
import { AvailableSubscription } from '../../Websocket/types';

export class FederatedTimelineColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardColumnParams, id: number) {
		const account = Account.get(args.account);

		super(account, FederatedTimelineColumnType, id, args);
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Federated + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.FederatedTimeline:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);
		WebsocketSubscriptionsManager.subscribeColumn(this, AvailableSubscription.Public);
	}

	private event(content: IndexedArray<ParsedStatus>, statuses: PayloadEvent<ParsedStatus>): void {
		if (!statuses.data.length) {
			return;
		}

		statuses.data.forEach((status) => {
			const index = content.findIndex(status.id);
			if (index === -1) {
				if (status.reblog) {
					ContentManager.Status.addOwner(this, status.reblog.id);
				}
				ContentManager.Status.addOwner(this, status.id);

				this.playSound();
			}

			content.update(status, index);
		});
	}

	async nextContent(): Promise<void> {
		return this.findNextContent(
			this._account.mirrorAPI.Timeline.getPublic.bind(this._account.mirrorAPI)
		);
	}

	async immediatelyPreviousContent(): Promise<void> {
		return this.findImmediatelyPreviousContent(
			this._account.mirrorAPI.Timeline.getPublic.bind(this._account.mirrorAPI)
		);
	}

	async previousContent(): Promise<NextContent> {
		return this.findPreviousContent(
			this._account.mirrorAPI.Timeline.getPublic.bind(this._account.mirrorAPI)
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		WebsocketSubscriptionsManager.unsubscribeColumn(this, AvailableSubscription.Public);
		super.destroy();
	}
}
