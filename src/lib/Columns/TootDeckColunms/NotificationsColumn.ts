import { get } from 'svelte/store';

import { Account } from '../../../stores/Account/Accounts';
import { rdOptionsBehaviorNotificationPlayCustomSound } from '../../../stores/Options';

import { StandardNotificationsColumn } from '../StandardNotificationColumn';
import { NotificationsColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEvent } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import { WebsocketSubscriptionsManager } from '../../Websocket/WebsocketManager';
import type { IndexedArray } from '../../IndexedArray';
import { playCustomSound } from '../../ComponentFunctions/moyai';

import type { StandardNotificationColumnParams } from '../types/options';
import type { ParsedNotification } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';
import { AvailableSubscription } from '../../Websocket/types';
import { Entity } from '../../../types/mastodonEntities';

export class NotificationsColumn extends StandardNotificationsColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardNotificationColumnParams, id: number) {
		const account = Account.get(args.account);

		super(account, NotificationsColumnType, id, args);
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Notifications +
				':' +
				id +
				':' +
				account.mirrorAPI.handle_string,
			(source, payload: PayloadEvent<ParsedNotification>) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Notification:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);
		WebsocketSubscriptionsManager.subscribeColumn(this, AvailableSubscription.Notification);
	}

	private event(
		content: IndexedArray<ParsedNotification>,
		notifications: PayloadEvent<ParsedNotification>
	): void {
		if (!notifications.data.length) {
			return;
		}

		notifications.data.forEach((notification) => {
			if (!this.isSubscribedType(notification.type)) {
				return;
			}

			const index = content.findIndex(notification.id);
			if (index === -1) {
				const status = notification.status;
				if (status) {
					if (status.reblog) {
						ContentManager.Status.addOwner(this, status.reblog.id);
					}
					ContentManager.Status.addOwner(this, status.id);
				}
				ContentManager.Notification.addOwner(this, notification.id);

				if (notification.notify) {
					this.playSound().then(() => {
						if (
							notification.type === Entity.NotificationType.Mention &&
							get(rdOptionsBehaviorNotificationPlayCustomSound) &&
							notification.status &&
							notification.status.plain_content &&
							!notification.status.spoiler_text
						) {
							playCustomSound(
								notification.status.id,
								notification.status.plain_content
							);
						}
					});
				}
			}

			this.reorder(content, notification);
		});

		if (this.group) {
			this.groupNotifications(content);
		}
	}

	async nextContent(): Promise<void> {
		return this.findNextContent(
			this._account.mirrorAPI.Notifications.getAll.bind(this._account.mirrorAPI)
		);
	}

	async immediatelyPreviousContent(): Promise<void> {
		return this.findImmediatelyPreviousContent(
			this._account.mirrorAPI.Notifications.getAll.bind(this._account.mirrorAPI)
		);
	}

	async previousContent(): Promise<NextContent> {
		return this.findPreviousContent(
			this._account.mirrorAPI.Notifications.getAll.bind(this._account.mirrorAPI)
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		WebsocketSubscriptionsManager.unsubscribeColumn(this, AvailableSubscription.Notification);
		super.destroy();
	}
}
