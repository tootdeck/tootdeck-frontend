import { get } from 'svelte/store';

import { Account } from '../../../stores/Account/Accounts';
import type { LayoutColumn } from '../../../stores/Columns/Layout';

import { UserColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEvent } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import type { OptionalFull } from '../../APIs/MirrorAPI/types';
import type { IndexedArray } from '../../IndexedArray';

import type { StandardColumnProfileParms } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';

export class ProfileColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardColumnProfileParms, id: number) {
		const account = Account.get(args.account);

		super(account, UserColumnType, id, args);
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Profile + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Status:
					case TootDeckEvents.Profile:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);
	}

	override get props(): LayoutColumn<ProfileColumn, ParsedStatus> {
		return {
			icon: this.icon,
			title: this.name,
			identifier: this.identifier,
			options: this.params.options,
			using_links: this.using_links,
			account: this.account,
			content: this.content,
			performance_mode: this._performance_mode,
			instance: this,
		};
	}

	override clear() {
		this._initialized.set(false);
		super.clear();
	}

	/**
	 * @deprecated function unused
	 */
	override limitContent(content: IndexedArray<ParsedStatus>): boolean {
		return false;
	}

	private event(content: IndexedArray<ParsedStatus>, statuses: PayloadEvent<ParsedStatus>): void {
		if (!statuses.data.length) {
			return;
		}

		statuses.data.forEach((status) => {
			const params = get(this._params.options) as any as Omit<
				StandardColumnProfileParms,
				'options'
			>;
			if (status.account.id !== params.profile_account_id) {
				return;
			}

			const index = content.findIndex(status.id);
			if (index !== -1) {
				content.update(status, index);
				return;
			}

			if (status.reblog) {
				ContentManager.Status.addOwner(this, status.reblog.id);
			}
			ContentManager.Status.addOwner(this, status.id);

			this.reorder(content, status);
		});
	}

	async nextContent(): Promise<void> {
		const options = get(this._params.options) as any as Omit<
			StandardColumnProfileParms,
			'options'
		>;
		const profile_account_id = options.profile_account_id;
		const request_params = options.request_params;
		return this.findNextContent((params: OptionalFull) =>
			this._account.mirrorAPI.Accounts.getStatuses(profile_account_id, {
				...params,
				...request_params,
			})
		);
	}

	/**
	 * @deprecated function unused
	 */
	async immediatelyPreviousContent(): Promise<void> {}

	async previousContent(): Promise<NextContent> {
		const options = get(this._params.options) as any as Omit<
			StandardColumnProfileParms,
			'options'
		>;
		const profile_account_id = options.profile_account_id;
		const request_params = options.request_params;
		return this.findPreviousContent((params: OptionalFull) =>
			this._account.mirrorAPI.Accounts.getStatuses(profile_account_id, {
				...params,
				...request_params,
			})
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		super.destroy();
	}
}
