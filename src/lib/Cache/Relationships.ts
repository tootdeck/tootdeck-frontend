import { get } from 'svelte/store';

import type NotificationLoading from '../../components/NotificationsModal/NotificationLoading.svelte';

import {
	stAccounts,
	type TootDeckAccount,
	type EntityRelationship,
} from '../../stores/Account/Accounts';
import { notificationModal } from '../../stores/NotificationsModal';
import { stLayout } from '../../stores/Columns/Layout';
import { stRelationshipBuildDate } from '../../stores/init';

import { AccountsMananger } from '../Managers/AccountsManager';
import type { StandardColumn } from '../Columns/StandardColumn';
import type { ResponseLinks } from '../Utils/parseResponseLInk';
import { errorModal } from '../errorModal';
import { filterDuplicate } from '../Utils/filter/filterDuplicates';
import type { OptionalSince } from '../APIs/MirrorAPI/types';
import { addFullHandle } from '../ComponentFunctions/addFullHandle';

import type { Entity } from '../../types/mastodonEntities';

let progress: NotificationLoading | null;

function countEntities(accounts: Array<TootDeckAccount>) {
	let max: { [handle: string]: number } = {};

	for (const account of accounts) {
		max[addFullHandle(account, account.entity)] =
			account.entity.followers_count + account.entity.following_count;
	}

	return max;
}

function getProgressMax(entities_count: { [handle: string]: number }) {
	return Object.values(entities_count).reduce((a, b) => a + b);
}

async function getFollow(
	fetch_fn: (
		id: string,
		options?: OptionalSince
	) => Promise<ResponseLinks<Array<Entity.Account>> | null>,
	account_id: string,
	count: number,
	limit: number
) {
	const follow_entities: Array<Entity.Account> = [];

	let since_id: string | undefined;
	let max_id: string | undefined;

	for (let i = count; i >= 0; i -= limit) {
		const response = (await fetch_fn(account_id, {
			limit,
			since_id,
			max_id,
		})) as ResponseLinks<Array<Entity.Account>> | null;
		if (!response) {
			return null;
		}
		if (!response.links) {
			break;
		}

		follow_entities.push(...response.data);
		response.data.forEach(() => progress?.tick?.());

		if (response.links === 'NONE') {
			break;
		}
		since_id = response.links.next?.since_id;
		max_id = response.links.next?.max_id;
	}

	return follow_entities;
}

async function getRelationship(
	account: TootDeckAccount,
	accounts_ids: Array<string>,
	limit: number
) {
	const relationship_entities: Array<Entity.Relationship> = [];
	if (!accounts_ids.length) {
		return [];
	}

	while (accounts_ids.length) {
		const ids = accounts_ids.splice(0, limit);

		const response = await account.mirrorAPI.Accounts.getRelationships(ids);
		if (!response) {
			return null;
		}

		relationship_entities.push(...response);

		response.forEach(() => progress?.tick?.());
	}

	return relationship_entities;
}

export async function init() {
	// Relationships cache building is already in progress
	if (progress) {
		return;
	}

	const accounts = Array.from(get(stAccounts).values());

	const entities_count = countEntities(accounts);
	let progress_max = getProgressMax(entities_count);

	progress = await notificationModal.createLoading('Generating relationship cache', progress_max);

	let has_gone_looping: boolean = false;

	const limit = 40;
	for (const account of accounts) {
		if (account.relationship.getSize().map_size !== 0) {
			continue;
		}

		/**
		 * Fetch from instance
		 */
		const followers_count = account.entity.followers_count;
		const following_count = account.entity.following_count;
		if (!followers_count && !following_count) {
			continue;
		}

		// Fetch followers
		const follower_entities = await getFollow(
			account.mirrorAPI.Accounts.getFollowers.bind(account.mirrorAPI),
			account.entity.id,
			followers_count,
			limit
		);
		if (!follower_entities) {
			errorModal('Failed to fetch follows entities. [1]', undefined, true);
			break;
		}

		// Fetch following
		const following_entities = await getFollow(
			account.mirrorAPI.Accounts.getFollowing.bind(account.mirrorAPI),
			account.entity.id,
			following_count,
			limit
		);
		if (!following_entities) {
			errorModal('Failed to fetch follows entities. [2]', undefined, true);
			break;
		}

		// Remove duplicates
		let entities = [...follower_entities, ...following_entities];
		entities = filterDuplicate(entities, 'id');

		// Update max progress value
		progress_max += entities.length;
		progress?.updateMax?.(progress_max);

		// Cache accounts
		entities.forEach((a) => AccountsMananger.store(account, a));

		// Fetch relationships
		const accounts = entities.map((a) => ({ id: a.id, acct: a.acct }));

		const relationship_entities = await getRelationship(
			account,
			accounts.map((a) => a.id),
			limit
		);
		if (!relationship_entities) {
			errorModal('Failed to fetch relationship.', undefined, true);
			break;
		}

		// Store into map
		const relationships: Array<[string, EntityRelationship]> = [];
		relationship_entities.forEach((e) => {
			const entity = { ...e, acct: accounts.find((a) => a.id === e.id)!.acct };
			const acct = addFullHandle(account, entity);
			entity.acct = acct;

			relationships.push([entity.id, entity]);
			relationships.push([acct, entity]);
		});
		account.relationship.setAll(relationships);

		has_gone_looping = true;
	}

	progress?.finish?.();
	progress = null;

	if (has_gone_looping) {
		stRelationshipBuildDate.set(new Date().valueOf());
		get(stLayout).forEach((v) => (v as StandardColumn<any>)?.updateContent());
	}
}

/**
 * //TODO: Handle these events
 * - Profile [OK]
 * - BLock/Unblock [OK]
 * - Mute/Unmute [OK]
 * - Follow/Unfollow [OK]
 * - Remote Follow/Unfollow [OK]
 * - Notifications follow/follow request accepted [OK]
 */
