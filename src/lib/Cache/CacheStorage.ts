import PersistentStorage from 'persistent-storage';
import { parse, stringify } from 'flatted';

import { debugLogger } from '../debugLogger';

export interface DataFormat<T> {
	/**
	 * Function to format data when loading it from local storage
	 *
	 * @param data `Array<[string, T]>`
	 */
	load?: (data: Array<[string, T]>) => Array<[string, T]>;
	/**
	 * Function to format data for local storage
	 *
	 * @param data `Map<string, T>` tranformed to `Array<[string, T]>`
	 */
	store?: (data: Array<[string, T]>) => Array<[string, T]>;
}

interface CacheStorageOptions<T> {
	/**
	 * Threshold value before storing a snapshot
	 *
	 * When threshold is reached the data is dumped into the local storage
	 */
	threshold?: number;
	/**
	 * Time to wait before counting threshold value
	 */
	wait?: number;
	/**
	 * Functions to format data
	 */
	formatData?: DataFormat<T>;
	/**
	 * Number of stored items
	 */
	limit?: number;
	/**
	 * When a limit is set you can override the trim function
	 *
	 * @param data `Map` where the items are actually stored
	 * @param order `Array` of `Map` keys to keep the `Map` insertion order (the last inserted item is at then end)
	 */
	trim?: (data: Map<string, T>, order: Array<string>) => void;
}

export interface CacheSize {
	map_size: number;
	footprint: number;
}

export class CacheStorage<T> {
	private readonly key: string;
	private readonly storage: PersistentStorage;
	private readonly dump_key: string = '';
	private readonly format_data: DataFormat<T> = {};
	private data = new Map<string, T>();
	private has_changed: boolean = false;
	private update_count: number = 0;
	private threshold: number = 20;

	private limit: number = -1;
	private order: Array<string> = [];
	private trim = (data: Map<string, T>, order: Array<string>) => {
		const del = order.shift()!;
		data.delete(del);
	};

	constructor(key: string, options?: CacheStorageOptions<T>) {
		this.key = key;
		this.storage = new PersistentStorage({
			keyPrefix: key,
			useCompression: true,
			useCache: false,
			storageBackend: window.localStorage,
		});

		if (options?.threshold) {
			if (!options?.wait) {
				this.threshold = options.threshold;
			} else {
				this.threshold = Infinity;
				setTimeout(() => {
					this.threshold = options.threshold!;
					if (this.has_changed) {
						this.setCache();
					}
				}, options.wait);
			}
		}

		if (options?.formatData) {
			this.format_data = options.formatData;
		}

		if (options?.limit) {
			const type = typeof options.limit;
			if (type !== 'number') {
				throw new Error(`Invalid limit type: expected "number", received "${type}"`);
			}

			if (options.limit < 1) {
				throw new Error(
					`Invalid limit: must be a positive integer, received ${options.limit}`
				);
			}

			this.limit = options.limit;
		}

		if (options?.trim) {
			this.trim = options.trim;
		}

		setInterval(
			() => {
				if (this.has_changed) {
					this.debugLogger(this.update_count + ' changes in 5min. Saving...');
					this.setCache();
				} else {
					this.debugLogger('No change in 5min.');
				}
			},
			5 * 60 * 1000
		);

		this.loadCache();
	}

	private debugLogger(txt: string) {
		debugLogger('CacheStorage: ' + this.key + ': ' + txt);
	}

	/**
	 * Store
	 */
	private loadCache() {
		this.debugLogger('Loading..');

		const raw = this.storage.getItem(this.dump_key);
		if (!raw) {
			this.debugLogger('Nothing to load.');
			return;
		}

		const parsed = parse(raw) as Array<[string, T]>;
		if (this.format_data.load) {
			const override = this.format_data.load(parsed);
			// console.log(
			// 	'READ',
			// 	override
			// 		.map((o) => o[0])
			// 		.filter((o) => o.includes('@'))
			// 		.join(',')
			// );
			this.data = new Map<string, T>();
			override.forEach(([key, value]) => this.setter(key, value));
		} else {
			this.data = new Map<string, T>();
			parsed.forEach(([key, value]) => this.setter(key, value));
		}

		this.debugLogger(`Loaded ${this.data.size} items.`);
	}

	private setCache() {
		this.debugLogger('Updating..');

		const data = Array.from(this.data);

		if (this.format_data.store) {
			const override = this.format_data.store(data);
			// console.log(
			// 	'WRITE',
			// 	override
			// 		.map((o) => o[0])
			// 		.filter((o) => o.includes('@'))
			// 		.join(',')
			// );
			this.storage.setItem(this.dump_key, stringify(override));
		} else {
			this.storage.setItem(this.dump_key, stringify(data));
		}

		this.has_changed = false;

		this.debugLogger('Done.');
	}

	getSize(): CacheSize {
		const data = Array.from(this.data);

		let size: number;
		if (this.format_data.store) {
			const override = this.format_data.store(data);
			size = override.length;
		} else {
			size = data.length;
		}

		const footprint = window.localStorage.getItem(this.key)?.length ?? 0;

		return {
			map_size: size,
			footprint: footprint,
		};
	}

	clearCache() {
		this.data.clear();
		this.storage.clear();
	}

	incrementCount(value: number = 1) {
		this.update_count += value;
		this.has_changed = this.update_count !== 0;
	}

	/**
	 * Map
	 */
	has(key: string): boolean {
		return this.data.has(key);
	}

	get(key: string): T | undefined {
		return this.data.get(key);
	}

	getFirst(): [string, T] | undefined {
		const key = this.order[0];
		const value = this.data.get(key);

		return value ? [key, value] : undefined;
	}

	getLast(): [string, T] | undefined {
		const key = this.order[this.order.length - 1];
		const value = this.data.get(key);

		return value ? [key, value] : undefined;
	}

	getAll(): Map<string, T> {
		return this.data;
	}

	private setter(key: string, value: T) {
		this.data.set(key, value);
		this.order.push(key);

		if (this.limit !== -1) {
			if (this.order.length >= this.limit) {
				this.trim(this.data, this.order);
			}
		}
	}

	setAll(data: Array<[string, T]>, update: boolean = true) {
		data.forEach(([key, value]) => this.setter(key, value));
		this.incrementCount(data.length);

		if (update) {
			if (this.update_count >= this.threshold) {
				this.debugLogger('Threshold reached, saving..');
				this.setCache();
			}
		}
	}

	set(key: string, value: T, update: boolean = true) {
		this.setter(key, value);
		this.incrementCount();

		if (update) {
			if (this.update_count >= this.threshold) {
				this.debugLogger('Threshold reached, saving..');
				this.setCache();
			}
		}
	}

	delete(key: string, update: boolean = true): boolean {
		const result = this.data.delete(key);
		if (result) {
			this.incrementCount();
		}

		if (update) {
			if (this.update_count >= this.threshold) {
				this.debugLogger('Threshold reached, saving..');
				this.setCache();
			}
		}

		return result;
	}

	forEach(callbackfn: (value: T, key: string, map: Map<string, T>) => void, thisArg?: any): void {
		this.data.forEach(callbackfn, thisArg);
	}
}
