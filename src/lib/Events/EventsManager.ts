import { NotificationModalType, notificationModal } from '../../stores/NotificationsModal';

export type EventHandler = (source: string, payload: any) => void;

export interface SubscriptionHandle {
	readonly name: string;
}

class EventsManagerInner {
	static readonly instance = new EventsManagerInner();

	private readonly before_event_handlers = new Map<string, EventHandler>();
	private readonly event_handlers = new Map<string, EventHandler>();
	private readonly after_event_handlers = new Map<string, EventHandler>();

	subscribeBefore(subscription_name: string, event_handler: EventHandler): SubscriptionHandle {
		if (this.before_event_handlers.has(subscription_name)) {
			notificationModal.create(
				NotificationModalType.Warn,
				'Duplicate before event ' + subscription_name,
				true
			);
			return { name: subscription_name };
		}

		this.before_event_handlers.set(subscription_name, event_handler);

		return { name: subscription_name };
	}

	subscribeAfter(subscription_name: string, event_handler: EventHandler): SubscriptionHandle {
		if (this.after_event_handlers.has(subscription_name)) {
			notificationModal.create(
				NotificationModalType.Warn,
				'Duplicate after event ' + subscription_name,
				true
			);
			return { name: subscription_name };
		}

		this.after_event_handlers.set(subscription_name, event_handler);

		return { name: subscription_name };
	}

	subscribe(subscription_name: string, event_handler: EventHandler): SubscriptionHandle {
		if (this.event_handlers.has(subscription_name)) {
			notificationModal.create(
				NotificationModalType.Warn,
				'Duplicate event ' + subscription_name,
				true
			);
			return { name: subscription_name };
		}

		this.event_handlers.set(subscription_name, event_handler);

		return { name: subscription_name };
	}

	unsubscribe(handle: SubscriptionHandle): void {
		this.event_handlers.delete(handle.name);
	}

	sendEvent(source: string, payload: any): void {
		if (!payload) {
			return;
		}

		this.before_event_handlers.forEach((handler) => {
			handler(source, payload);
		});

		this.event_handlers.forEach((handler) => {
			handler(source, payload);
		});

		this.after_event_handlers.forEach((handler) => {
			handler(source, payload);
		});
	}
}

export const EventsManager = EventsManagerInner.instance;
