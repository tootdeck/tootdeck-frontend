import type { TootDeckAccount } from '../../stores/Account/Accounts';

import type { Links } from '../Utils/parseResponseLInk';

import type { ParsedStatus } from '../../types/contentParsed';

export interface PayloadEvent<T> {
	account: TootDeckAccount;
	data: Array<T>;
	links?: Links | null;
}

export interface PayloadEventList extends PayloadEvent<ParsedStatus> {
	list_id: string;
}
