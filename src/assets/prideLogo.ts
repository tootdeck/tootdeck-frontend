export const prideLogo: { [key: string]: string } = {
	ace: 'pride/tootdeck-ace.svg',
	agender: 'pride/tootdeck-agender.svg',
	aroace: 'pride/tootdeck-aroace.svg',
	aro: 'pride/tootdeck-aro.svg',
	bi: 'pride/tootdeck-bi.svg',
	femboy: 'pride/tootdeck-femboy.svg',
	gay: 'pride/tootdeck-gay.svg',
	lesbian: 'pride/tootdeck-lesbian.svg',
	queer: 'pride/tootdeck-queer.svg',
	nonbinary: 'pride/tootdeck-nonbinary.svg',
	pan: 'pride/tootdeck-pan.svg',
	transgender: 'pride/tootdeck-transgender.svg',
};
