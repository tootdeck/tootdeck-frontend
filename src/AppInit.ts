/**
 * Workaround to be able to subscribe to stores at the root of ts files
 */
export class AppInit {
	static Promise: Promise<void> = new Promise((r) => {
		setTimeout(() => r(undefined));
	});
}
